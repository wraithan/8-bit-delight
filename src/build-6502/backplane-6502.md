# Backplane 6502 Build

{{#include ./backplane-6502-summary.md}}

## Goals

Have a soldered together 6502 computer that is connected to the RA8875 display, as well as a serial connection. Input via PS/2 keyboard and SEGA controller. Oh and definitely has blinkenlights.

This is meant both to be my desktop 6502 that will live on my bench, but also the platform for me to prototype the peripherials and software for my Laptop 6502.

## Pin Map

Still working this out... there is a symbol in the `schematics/` directory of one potential layout.

Currently thinking 2 card edge connectors, 50 pins, 25 on each side for the first one, 30 pins, 15 on each side for the second. It's a lot of pins but it will allow me to expand over time without worrying that I'll have conflicts.
