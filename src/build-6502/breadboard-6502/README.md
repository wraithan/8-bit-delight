# Breadboard 6502

{{#include ./summary.md}}

## Memory Map

This memory map is inherited from the BE6052. It prioritizes ease of implementation on a breadboard with logic gates, while still being simple to conceptualize. One of my next iterations will include a GAL or PLD to be able to keep the number of ICs down (and thus have 1 place to about propegation delays) while being able to reprogram it as I adjust the memory map, this was also a prevailing technique on home computers that used the NMOS 6502. 

Important pins:

* A0-A3: No block is smaller than 16bits, so these are shared by all blocks.
* A4-A13: If A14 is high and A15 is low, IO Device select, only 1 pin should be high in that case. Very wasteful but easy to wire up.
* A14: If A15 is low, RAM / IO selector, low = RAM, high = IO
* A15: everything else / ROM selector, low = everything else, high = ROM

| Binary                                  | Hex             | Description |
|-----------------------------------------|-----------------|-------------|
| `0000000000000000` - `0000000011111111` | `0000` - `00ff` | RAM - Zero page |
| `0000000100000000` - `0000000111111111` | `0100` - `01ff` | RAM - Stack |
| `0000001000000000` - `0011111111111111` | `0200` - `3fff` | RAM - Free Usable RAM |
| `0100000000000000` - `0100000000001111` | `4000` - `400f` | IO 00 |
| `0100000000010000` - `0100000000011111` | `4010` - `401f` | IO 01 |
| `0100000000100000` - `0100000000101111` | `4020` - `402f` | IO 02 |
| `0100000001000000` - `0100000001001111` | `4040` - `404f` | IO 03 |
| `0100000010000000` - `0100000010001111` | `4080` - `408f` | IO 04 |
| `0100000100000000` - `0100000100001111` | `4100` - `410f` | IO 05 |
| `0100001000000000` - `0100001000001111` | `4200` - `420f` | IO 06 |
| `0100010000000000` - `0100010000001111` | `4400` - `440f` | IO 07 |
| `0100100000000000` - `0100100000001111` | `4800` - `480f` | IO 08 |
| `0101000000000000` - `0101000000001111` | `5000` - `500f` | IO 09 - ACIA for external UART |
| `0110000000000000` - `0110000000001111` | `6000` - `600f` | IO 10 - VIA for LCD |
| `1000000000000000` - `1111111111111111` | `8000` - `ffff` | ROM |

Visual version of the memory map, top is page-wise based on first 16bit block, second is bit-wise with a page (256 bits) per row. This really shows how ineffcient the current memory map is.

![Visual version of the memory map](./memory-map.png)