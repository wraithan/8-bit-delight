# The 6502 Delight Builds

Making 6502 computers with a delightful experience.

What does it mean for a 6502 computer to have a delightful experience? Well, that of course comes down to the eye of the beholder. For me it means caring about my ergonomics as I develop and eventually use these computers. For example, before I even completed building Ben Eater's kit, I'd already customized the build to have in situ ROM reprogramming and extra debug capabilties.

## Breadboard 6502

{{#include ./breadboard-6502/summary.md}}

## Backplane 6502

{{#include ./backplane-6502-summary.md}}

## Laptop 6502

{{#include ./laptop-6502-summary.md}}
