    .import __ACIA_START__

    .export init_acia
    .export acia_process_receive_loop
    .export acia_irq

ACIA_0_DATA = __ACIA_START__ + 0

; irq, data set read bar, data carrier detect bar, transmit data register empty, receiver data register full, overrun, framing error, parity error
ACIA_0_STATUS = __ACIA_START__ + 1
ACIA_0_COMMAND = __ACIA_START__ + 2
ACIA_0_CONTROL = __ACIA_START__ + 3

    .data
; chonker to dump stuff into
SYS_RAM_UART_RECEIVE_BUFFER: .res $ff
SYS_RAM_UART_RECEIVE_READ_PTR: .res 1
SYS_RAM_UART_RECEIVE_WRITE_PTR: .res 1
; MSB is echo enabled
SYS_RAM_UART_PROCESS_FLAGS: .res 1

    .code

init_acia:
	ldx #$ff
zero_buffer_loop:
	stz SYS_RAM_UART_RECEIVE_BUFFER,x ;
	dex
	bne zero_buffer_loop ; branch if zero flag not set
	stz SYS_RAM_UART_RECEIVE_BUFFER,x ; one more time so we get the 0th spot
	stz SYS_RAM_UART_RECEIVE_READ_PTR
	stz SYS_RAM_UART_RECEIVE_WRITE_PTR

    ; done zeroing buffers

    ; setup ACIA
	stz ACIA_0_STATUS

	lda #%00001001 ; no parity no echo no interrupt
	sta ACIA_0_COMMAND

	lda #%00010000 ; 1 stop bit 8 data bits 115200 baud
	sta ACIA_0_CONTROL

	rts

serial_send:
	pha
serial_send_wait:
	lda ACIA_0_STATUS
	and #%00010000
	beq serial_send_wait
	pla
	sta ACIA_0_DATA
	rts

acia_irq:
	pha
	phx

wait_acia_read:
	lda ACIA_0_STATUS
	bpl acia_irq_end ; branch positive, aka if MSB is low, faster than AND with value
	lda ACIA_0_DATA
	ldx SYS_RAM_UART_RECEIVE_WRITE_PTR
	sta SYS_RAM_UART_RECEIVE_BUFFER,x
	inc SYS_RAM_UART_RECEIVE_WRITE_PTR

acia_irq_end:
	plx
	pla
	rti

process_receive_buffer:
	pha
	phx
	sei ; SEt Interrupt disable

acia_process_receive_loop:
	ldx SYS_RAM_UART_RECEIVE_READ_PTR
	cpx SYS_RAM_UART_RECEIVE_WRITE_PTR
	beq process_receive_end

	lda SYS_RAM_UART_RECEIVE_BUFFER,x
	inc SYS_RAM_UART_RECEIVE_READ_PTR

	cmp #'e'
	bne done_with_loop

	tax
	lda SYS_RAM_UART_PROCESS_FLAGS
	eor #%10000000
	sta SYS_RAM_UART_PROCESS_FLAGS
	txa

done_with_loop:
	bit SYS_RAM_UART_PROCESS_FLAGS
	bpl acia_process_receive_loop
	jsr serial_send
	jmp acia_process_receive_loop

process_receive_end:
	cli ; CLear Interrupt disable
	plx
	pla
	rts
