	.import __VIA_START__

    .export VIA_0_PORTA
    .export VIA_0_PORTB
    .export VIA_0_DDRA
    .export VIA_0_DDRB
    .export VIA_0_T1C_L
    .export VIA_0_T1C_H
    .export VIA_0_T1L_L
    .export VIA_0_T1L_H
    .export VIA_0_T2C_L
    .export VIA_0_T2C_H
    .export VIA_0_SR
    .export VIA_0_ACR
    .export VIA_0_PCR
    .export VIA_0_IFR
    .export VIA_0_IER
    .export VIA_0_PORTA_NH
; In address order
; ORB/IRB are renamed to PORTB, likewise for PORTA
; IO to the ports
VIA_PORTB = $00
VIA_PORTA = $01

; Data direction, 0 = Input, 1 = Output
VIA_DDRB = $02
VIA_DDRA = $03

; Timers
VIA_T1C_L = $04
VIA_T1C_H = $05
VIA_T1L_L = $06
VIA_T1L_H = $07
VIA_T2C_L = $08
VIA_T2C_H = $09

; Shift register IO
VIA_SR = $0a

; Auxillary Control Register, controls shift register
VIA_ACR = $0b

; Peripheral Control Register, controls CA1, CA2, CB1, CB2
VIA_PCR = $0c

; Interrupt Flag Register, which interrupts have been triggered
VIA_IFR = $0d

; Interrupt Enable Register
VIA_IER = $0e

; PORTA without handshake setup in PCR
VIA_PORTA_NH = $0f

VIA_0_PORTA = __VIA_START__ + VIA_PORTA
VIA_0_PORTB = __VIA_START__ + VIA_PORTB
VIA_0_DDRA = __VIA_START__ + VIA_DDRA
VIA_0_DDRB = __VIA_START__ + VIA_DDRB
VIA_0_T1C_L = __VIA_START__ + VIA_T1C_L
VIA_0_T1C_H = __VIA_START__ + VIA_T1C_H
VIA_0_T1L_L = __VIA_START__ + VIA_T1L_L
VIA_0_T1L_H = __VIA_START__ + VIA_T1L_H
VIA_0_T2C_L = __VIA_START__ + VIA_T2C_L
VIA_0_T2C_H = __VIA_START__ + VIA_T2C_H
VIA_0_SR = __VIA_START__ + VIA_SR
VIA_0_ACR = __VIA_START__ + VIA_ACR
VIA_0_PCR = __VIA_START__ + VIA_PCR
VIA_0_IFR = __VIA_START__ + VIA_IFR
VIA_0_IER = __VIA_START__ + VIA_IER
VIA_0_PORTA_NH = __VIA_START__ + VIA_PORTA_NH