    .import VIA_0_PORTA
    .import VIA_0_PORTB
    ; .import VIA_0_DDRA
    .import VIA_0_DDRB
    .import VIA_0_SR
    .import VIA_0_PCR

    .export init_lcd
    .export print_char
    .export print_asciiz_at
    .export LCD_LINE_TABLE
    .export ZP_PRINT_STR

    .zeropage
 ; two bytes, location of string to print
ZP_PRINT_STR:  .res 2

    .data
; two bytes, red green blue contrast, $00 = rrrrgggg, $01 = bbbbcccc
SYS_RAM_LCD_SETTINGS: .res 2

    .rodata
boot_messages:
	.word boot_1, boot_2, boot_3, boot_4

boot_1: .asciiz "DELIGHT-OS 0.2"
boot_2: .asciiz "Welcome!"
boot_3: .asciiz "Input below:"
boot_4: .asciiz ""

    .code
LCD_CLEAR_DISPLAY = %00000001
LCD_RETURN_HOME = %00000010 ; LSB unused
LCD_ENTRY_MODE_SET = %00000100 ; cursor move right, shift on
LCD_DISPLAY_ON_OFF = %00001000 ; display on, cursor on, cursor blinking on
LCD_CURSOR_DISPLAY_SHIFT = %00010000 ; display shift, right shift, 2 LSB unused
LCD_FUNCTION_SET = %00100000 ; interface data len, number of lines, font, 2 LSB unused
LCD_SET_CGRAM = %01000000 ; 6 LSBs are the address
LCD_SET_DDRAM = %10000000 ; 7 LSBs are the address

LCD_DDRAM_FIRST_LINE = $00
LCD_DDRAM_SECOND_LINE = $40
LCD_DDRAM_THIRD_LINE = $14
LCD_DDRAM_FOURTH_LINE = $54

LCD_LINE_TABLE:
    .byte LCD_DDRAM_FIRST_LINE
    .byte LCD_DDRAM_SECOND_LINE
    .byte LCD_DDRAM_THIRD_LINE
    .byte LCD_DDRAM_FOURTH_LINE

LCD_BUSY_FLAG = %10000000

LCD_ENABLE = %10000000
LCD_RW = %01000000
LCD_RS = %00100000

init_lcd:
    ; setup LCD
	lda #(LCD_FUNCTION_SET | %11000) ; Set 8-bit mode; 2-line display; 5x8 font
	jsr lcd_instruction

	lda #(LCD_DISPLAY_ON_OFF | %111) ; Set Display on; cursor on; blink on;
	jsr lcd_instruction

	lda #(LCD_ENTRY_MODE_SET | %10) ; Cursor direction move right; no shift
	jsr lcd_instruction

	lda #LCD_CLEAR_DISPLAY
	jsr lcd_instruction

	cli ; CLear Interrupt disable

	jsr show_boot_message

    rts

show_boot_message:
	pha
	phx
	phy

	ldy #0 ; Pointer into boot_messages
	ldx #0 ; Line number

boot_message_loop:
	lda boot_messages,y
	sta ZP_PRINT_STR
	iny
	lda boot_messages,y
	sta ZP_PRINT_STR+1
	lda LCD_LINE_TABLE,x

	jsr print_asciiz_at

	iny
	inx
	cpx #4 ; There are 4 lines in the boot message
	bne boot_message_loop

	ply
	plx
	pla
	rts


lcd_instruction: ; Sends the instruction in register A to the LCD
  jsr lcd_wait
  sta VIA_0_PORTB
  lda #0           ; clear RS/RW/E
  sta VIA_0_PORTA
  lda #LCD_ENABLE  ; Send E to send instruction
  sta VIA_0_PORTA
  lda #0           ; clear RS/RW/E
  sta VIA_0_PORTA
  rts

lcd_wait: ; Waits for the busy flag to be unset
  pha
  lda #%00000000 ; Port B input mode
  sta VIA_0_DDRB

lcd_busy_loop:
  lda #LCD_RW        ; Switch to RW mode
  sta VIA_0_PORTA
  lda #( LCD_RW | LCD_ENABLE ) ; Send E to send instruction
  sta VIA_0_PORTA
  lda VIA_0_PORTB
  and #LCD_BUSY_FLAG
  bne lcd_busy_loop  ; Loop if busy bit was set

  lda #LCD_RW
  sta VIA_0_PORTA
  lda #%11111111   ; Port B output mode
  sta VIA_0_DDRB
  pla
  rts

print_char: ; Prints a single char in register A to the current cursor position
  jsr lcd_wait
  sta VIA_0_PORTB
  lda #LCD_RS         ; set RS, clear RW/E
  sta VIA_0_PORTA
  lda #( LCD_ENABLE | LCD_RS ) ; Send E to send instruction
  sta VIA_0_PORTA
  lda #LCD_RS         ; set RS, clear RW/E
  sta VIA_0_PORTA
  rts

print_asciiz_at: ; Prints string in ZP_PRINT_STR at DDRAM location provided in register A
  phy
  ora #LCD_SET_DDRAM
  jsr lcd_instruction

  ldy #0
print_first_loop:
  lda (ZP_PRINT_STR),y
  beq print_done
  jsr print_char
  iny
  jmp print_first_loop

print_done:
  ply
  rts

write_display_settings_to_sr:
  pha
  lda SYS_RAM_LCD_SETTINGS
  sta VIA_0_SR
  nop
  nop
  nop
  nop
  nop
  nop
  lda SYS_RAM_LCD_SETTINGS + 1
  sta VIA_0_SR
  nop
  nop
  nop
  nop
  nop
  nop
  lda #$fc ;; Turns CA2 off
  sta VIA_0_PCR
  lda #$ff ;; Turns CA2 on
  sta VIA_0_PCR
  pla
  rts
