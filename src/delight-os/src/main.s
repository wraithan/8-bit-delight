    .setcpu "65C02"

    .import VIA_0_DDRA
    .import VIA_0_DDRB

	.import init_acia
	.import acia_process_receive_loop
	.import acia_irq

	.import init_lcd
	.import print_char
	.import print_asciiz_at
	.import LCD_LINE_TABLE

	.zeropage
	.import ZP_PRINT_STR

	.data
SYS_RAM_DECODE_VALUE: .res 2
SYS_RAM_DECODE_MOD10: .res 2
SYS_RAM_DECODE_MESSAGE: .res 6

    .code
reset:
	sei ; SEt Interrupt disable
	ldx #$ff
	txs ; Initialize stack to top (grows down)

	jsr init_acia

    ; setup VIA
	lda #%11111111 ; Port B to output mode for data pins
	sta VIA_0_DDRB
	lda #%11100000 ; Port A to output mode for control pins
	sta VIA_0_DDRA

	jsr init_lcd
	jsr init_decode_buffer

	jsr decode_next
	jmp main

; buffer decoding test

init_decode_buffer:
	; zero whole message, unroll loop since loop is basically same amount of code
	stz SYS_RAM_DECODE_MESSAGE
	stz SYS_RAM_DECODE_MESSAGE + 1
	stz SYS_RAM_DECODE_MESSAGE + 2
	stz SYS_RAM_DECODE_MESSAGE + 3
	stz SYS_RAM_DECODE_MESSAGE + 4
	stz SYS_RAM_DECODE_MESSAGE + 5

	; initialize value before decoding
	lda example_value
	sta SYS_RAM_DECODE_VALUE
	lda example_value + 1
	sta SYS_RAM_DECODE_VALUE + 1
	rts

decode_next:
	; initialize remainder before decoding
	stz SYS_RAM_DECODE_MOD10
	stz SYS_RAM_DECODE_MOD10 + 1
	clc

	ldx #16
decode_loop:
	; rotate quotient and remainder
	rol SYS_RAM_DECODE_VALUE
	rol SYS_RAM_DECODE_VALUE + 1
	rol SYS_RAM_DECODE_MOD10
	rol SYS_RAM_DECODE_MOD10 + 1

	; a,y = dividend - divisor
	sec
	lda SYS_RAM_DECODE_MOD10
	sbc #10
	tay ; save low byte
	lda SYS_RAM_DECODE_MOD10 + 1
	sbc #0
	bcc  ignore_result ; branch carry clear if divident < divisor
	sty SYS_RAM_DECODE_MOD10
	sta SYS_RAM_DECODE_MOD10 + 1

ignore_result:
	dex
	bne decode_loop
	rol SYS_RAM_DECODE_VALUE ; shift in last bit of the quotient
	rol SYS_RAM_DECODE_VALUE + 1

	lda SYS_RAM_DECODE_MOD10
	clc
	adc #'0'
	; jsr print_char
	jsr push_char

	lda SYS_RAM_DECODE_VALUE
	ora SYS_RAM_DECODE_VALUE + 1
	bne decode_next


	lda #<SYS_RAM_DECODE_MESSAGE
	sta ZP_PRINT_STR
	lda #>SYS_RAM_DECODE_MESSAGE
	sta ZP_PRINT_STR+1
	lda LCD_LINE_TABLE + 3

	jsr print_asciiz_at
	rts

; Add the character in the A register to the beginning of the
; null-terminated string
push_char:
	pha
	ldy #0

	; rotate exising message to make room at start
char_loop:
	lda SYS_RAM_DECODE_MESSAGE,y ; load next char
	tax
	pla
	sta SYS_RAM_DECODE_MESSAGE,y ; push last character onto the front
	iny
	txa ; tried to skip this using phx but need to check for zero flag of this value anyway
	pha ; next char at start of stack
	bne char_loop

	pla
	sta SYS_RAM_DECODE_MESSAGE,y ;

	rts

main:
	jsr acia_process_receive_loop
	cli
	wai
	jmp main

irq_button:
	pha
	lda #'!'
	jsr print_char
	pla
	rti

irq:
	; jmp acia_irq ; rely on this to rti
	rti

nmi:
	rti

	.rodata
example_value: .word 5678
;example_value: .dword .time

	.segment "VECTORS"
	.word nmi   ; NMI Vector
	.word reset ; RES Vector
	.word irq   ; BRK/IRQ Vector