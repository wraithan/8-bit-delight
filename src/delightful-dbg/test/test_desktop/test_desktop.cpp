#include <unity.h>

void test_pass() {
    TEST_ASSERT_EQUAL(0x0a, 10);
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(test_pass);
    UNITY_END();
}