#define STR_PRODUCT L"delightful-dbg"
#define STR_SERIAL_NUMBER L"6502"

#include <Arduino.h>
#include "YACL.h"

#include "./StatefulPin.h"
#include "./6502.h"
#include "./eeprom.h"
#include "./print.h"
#include "./protocol.h"

// Teensy control
const uint8_t programmerModePin = PIN_E0;

StatefulPin programmerModeSwitch(programmerModePin);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // Serial.begin() not required on teensy.
  // Serial.begin(9600);
  // Wait for serial connect or 10s.
  while (!Serial && millis() < 10000) { }

  char printBuf[32];
  snprintf_P(printBuf, 32, PSTR("[%08lu] Starting up!"), millis());
  log(printBuf);

  programmerModeSwitch.setup(INPUT_PULLUP);

  readOnlyMode();

  // log("Rebooting 6502");
  // reset6502();

  digitalWrite(LED_BUILTIN, LOW);
}

const uint16_t dataBufferLen = 4096;
uint8_t dataBuffer[dataBufferLen];
uint16_t dataWritePointer = 0;
uint16_t dataReadPointer = 0;
elapsedMillis sinceLastSerialRead;
bool programmerModeSetup = false;
bool monitorMode = false;

bool runCommand(const char* kind, CBOR body) {
  size_t printBufSize = 96;
  char printBuf[printBufSize];
  snprintf_P(printBuf, printBufSize, PSTR("Running command: %s"), kind);
  log(printBuf);

  if (strcmp(kind, "Hello") == 0) {
    log("Howdy!");
    return true;
  } else if (strcmp(kind, "Reset6502") == 0) {
    log("Reseting 6502");
    reset6502();
    return true;
  } else if (strcmp(kind, "Monitor") == 0 && body.is_bool()) {
    log("Monitor mode message!");
    monitorMode = (bool)body;
    return true;
  } else if (strcmp(kind, "Flash") == 0 && body["payload"].is_bytestring()) {
    uint16_t offset = (uint16_t)body["offset"];
    CBOR payload = body["payload"];
    size_t flashLen = payload.get_bytestring_len();
    snprintf_P(printBuf, printBufSize, PSTR("Flash mode: Offset: %d, Payload size: %d"), offset, flashLen);
    log(printBuf);

    if (flashLen <= DYNAMIC_FLASH_MAX_SIZE) {
      payload.get_bytestring(dynamicFlash);
    } else {
      log("flash payload too big");
      return false;
    }

    readWriteMode();
    disableSoftwareDataProtection();
    writeDynamicFlashToEERPOM(offset, flashLen);
    bool success = validateDynamicFlashAtOffset(offset, flashLen);
    enableSoftwareDataProtection();
    readOnlyMode();

    return success;
  }
  log("failed to parse message was a type wrong?");
  return false;
}

bool handleCBOR(CBOR cbor_data) {
  if (cbor_data.is_array()) {
    char printBuf[32];
    uint16_t num = (uint16_t)cbor_data[1];
    bool needsAck = (bool)cbor_data[2];
    CBOR payload = cbor_data[3];

    if (payload.is_pair() && payload["kind"].is_string()) {
      const uint8_t maxCommandLen = 24;
      char msgKind[maxCommandLen];
      payload["kind"].get_string(msgKind);
      snprintf_P(printBuf, 32, PSTR("[%d] Got kind: %s (%s)"), num, msgKind, needsAck ? "ack" : "noack");
      log(printBuf);
      bool result = runCommand(msgKind, payload["body"]);
      if (needsAck && result) {
        ackMessage(num);
      } else if (needsAck) {
        log("needed ack but result was bad");
      }
      return result;
    } else {
      log("bad payload?");
    }
  }
  return false;
}

void loop() {
  static bool serialWasConnected = false;
  programmerModeSwitch.update();
  clock.update();
  rwValue.update();

  if (!serialWasConnected && Serial) {
    if (Serial.available()) {
      dataWritePointer = 0;
      serialWasConnected = true;
    }
  }

  if (!Serial) {
    dataWritePointer = 0;
    resetMessageCounter();
    monitorMode = false;
  }


  digitalWrite(LED_BUILTIN, clock.getHighLow());

  bool engageProgrammerMode = programmerModeSwitch.getHighLow() == LOW;
  static bool inObject = false;

  while (dataWritePointer < dataBufferLen  && Serial.available()) {
    // log("Buffering!");
    uint8_t nextByte = Serial.read();
    if (inObject || nextByte == 0x84) {
      dataBuffer[dataWritePointer++] = nextByte;
      inObject = true;
    }
    sinceLastSerialRead = 0;
  }

  if (dataWritePointer > 4) {
    // const uint8_t printBufLen = 128;
    uint16_t objectLen = (dataBuffer[2] << 8) | dataBuffer[3];

    // char printBuf[printBufLen];
    // snprintf_P(printBuf, printBufLen,
    //   PSTR("Parsing payload of len %d (%d) [0x0x 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, ..."),
    //   dataWritePointer, objectLen,
    //   dataBuffer[0], dataBuffer[1],
    //   dataBuffer[2], dataBuffer[3],
    //   dataBuffer[4], dataBuffer[5]
    // );
    // log(printBuf);

    if (dataWritePointer >= objectLen) {
      CBOR cbor_data = CBOR(dataBuffer, dataWritePointer);
      if (!handleCBOR(cbor_data)) {
        log("Hmmm invalid.");
      }
      memmove(dataBuffer, dataBuffer + objectLen, dataBufferLen - objectLen);
      dataWritePointer -= objectLen;
    }
  }

  if (monitorMode) {
    if (clock.isRising()) {
      sendMonitorCBOR();
    }
  }

  if (engageProgrammerMode) {
    if (programmerModeSetup) {
      readOnlyMode();

      programmerModeSetup = false;
      reset6502();
      return;
    }
  } else {
    if (!programmerModeSetup) {
      log("Programmer mode started!");
      sendMonitorCBOR();
      readWriteMode();

      // disableSoftwareDataProtection();
      // writeProgMemToEERPOM();
      // enableSoftwareDataProtection();
      // validateProgMemToEEPROM();

      // // Moved the - over 1 in the `hello-world-checked.bin` ROM
      // writeByteChecked(0x300e, ' ');
      // writeByteChecked(0x300f, '-');

      // // Add exclamation point!
      // writeByteChecked(0x301d, '!');
      // writeByteChecked(0x301e, 0x00);

      // Ensure chip and databus state
      readOnlyMode();
      programmerModeSetup = true;
      log("Done programming!");
    }

    delay(10);
  }
}

