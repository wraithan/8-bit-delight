#ifndef HEADER_EEPROM_H
#define HEADER_EEPROM_H

#include <Arduino.h>
#include "./6502.h"
#include "./print.h"

// https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

// EEPROM Control Signals
const uint8_t writeEnablePin = PIN_D2;
const uint8_t outputEnablePin = PIN_D3;
const uint8_t chipEnablePin = PIN_D5;
const uint8_t actualChipEnable = PIN_D7;

const uint16_t EEPROM_OFFSET = 0x8000;

const size_t DYNAMIC_FLASH_MAX_SIZE = 1024;
uint8_t dynamicFlash[DYNAMIC_FLASH_MAX_SIZE];

void setAddress(uint16_t);
void setEEPROMAddress(uint16_t address);

inline void enableWrites() {
  digitalWrite(writeEnablePin, LOW);
}

inline void disableWrites() {
  digitalWrite(writeEnablePin, HIGH);
}

inline void enableOutput() {
  digitalWrite(outputEnablePin, LOW);
}

inline void disableOutput() {
  digitalWrite(outputEnablePin, HIGH);
}

inline void enableChip() {
  digitalWrite(chipEnablePin, HIGH);
}

inline void disableChip() {
  digitalWrite(chipEnablePin, LOW);
}

void writeByteToEEPROM(uint16_t address, uint8_t data) {
  setEEPROMAddress(address);
  DATA_BUS_OUT = data;
  enableWrites();
  disableWrites();
}

void pollForWriteComplete(uint8_t expected, uint32_t timeout = 0) {
  uint8_t result = 0;
  uint8_t dataDir = DATA_BUS_DDR;
  DATA_BUS_DDR = 0;
  uint32_t startTime;
  if (timeout) {
    startTime = millis();
  }
  do {
    if (timeout && (millis() - startTime) > timeout) {
      log("failure to write");
      break;
    }
    enableOutput();
    result = DATA_BUS_IN;
    disableOutput();
  } while(result != expected);
  DATA_BUS_DDR = dataDir;
}

void pollForWriteToggleComplete() {
  uint8_t result = 0xff;
  uint8_t last = 0xff;
  uint8_t dataDir = DATA_BUS_DDR;
  DATA_BUS_DDR = 0;

  do {
    last = result;
    enableOutput();
    result = DATA_BUS_IN & 0b01000000;
    disableOutput();
  } while(result != last);
  DATA_BUS_DDR = dataDir;
}

void writeByteChecked(uint16_t address, uint8_t data) {
  DATA_BUS_DDR = 255;
  disableOutput();
  writeByteToEEPROM(address, data);
  enableOutput();
  DATA_BUS_DDR = 0;
  pollForWriteComplete(data);
}

void enableSoftwareDataProtection() {
  log("Enabling software data protection... ");
  disableOutput();
  DATA_BUS_DDR = 255;
  writeByteToEEPROM(0x5555, 0xaa);
  writeByteToEEPROM(0x2aaa, 0x55);
  writeByteToEEPROM(0x5555, 0xa0);
  DATA_BUS_DDR = 0;
  pollForWriteToggleComplete();
  enableOutput();
  log("Done");
}

void disableSoftwareDataProtection() {
  log("Disabling software data protection... ");
  disableOutput();
  DATA_BUS_DDR = 255;
  writeByteToEEPROM(0x5555, 0xaa);
  writeByteToEEPROM(0x2aaa, 0x55);
  writeByteToEEPROM(0x5555, 0x80);
  writeByteToEEPROM(0x5555, 0xaa);
  writeByteToEEPROM(0x2aaa, 0x55);
  writeByteToEEPROM(0x5555, 0x20);
  DATA_BUS_DDR = 0;
  pollForWriteToggleComplete();
  enableOutput();
  log("Done");
}

void writeDynamicFlashToEERPOM(uint16_t offset, size_t dynamicFlashLen) {
  disableOutput();
  DATA_BUS_DDR = 255;
  log("Starting first half");
  for (uint16_t i = 0; i < dynamicFlashLen; i++) {
    uint8_t data = dynamicFlash[i];
    writeByteToEEPROM(offset + i, data);
    // Page write mode can write 64 bytes at a time, also need to check if it is the last byte
    if (i % 64 == 63 || i == (dynamicFlashLen - 1)) {
      pollForWriteComplete(data, 10);
      log(".");
    }
  }
  log("Done");
  DATA_BUS_DDR = 0;
  enableOutput();
}

inline void setEEPROMAddress(uint16_t address) {
  // EEPROM memory has an offset;
  setAddress(address + EEPROM_OFFSET);
}

inline void setAddress(uint16_t address) {
  ADDRESS_LSB_OUT = address & 0xff;
  ADDRESS_MSB_OUT = (address >> 8) & 0xff;
}

void readWriteMode() {
  pinMode(rdyPin, OUTPUT);
  pinMode(busEnablePin, OUTPUT);

  // Stop the 6502
  digitalWrite(rdyPin, LOW);
  digitalWrite(busEnablePin, LOW);

  // Set to what the timing diagram shows at the start.
  enableOutput();
  disableWrites();
  disableChip();

  pinMode(outputEnablePin, OUTPUT);
  pinMode(chipEnablePin, OUTPUT);
  pinMode(writeEnablePin, OUTPUT);

  setEEPROMAddress(0);

  // Address lines are outputs
  DATA_BUS_DDR = 255;
  ADDRESS_LSB_DDR = 255;
  ADDRESS_MSB_DDR = 255;

  enableChip();
}

void readOnlyMode() {
  // Setup 6502 Control signals
  clock.setup();
  rwValue.setup();

  pinMode(rdyPin, INPUT);
  pinMode(busEnablePin, INPUT);
  pinMode(resetPin, INPUT);

  // Setup EEPROM Control signals
  pinMode(writeEnablePin, INPUT);
  pinMode(outputEnablePin, INPUT);
  pinMode(chipEnablePin, INPUT);
  pinMode(actualChipEnable, INPUT);

  // Setup Data bus
  DATA_BUS_DDR = 0;
  DATA_BUS_OUT = 0;

  // Setup Address bus
  ADDRESS_LSB_DDR = 0;
  ADDRESS_LSB_OUT = 0;
  ADDRESS_MSB_DDR = 0;
  ADDRESS_MSB_OUT = 0;
}

bool validateDynamicFlashAtOffset(uint16_t offset, size_t dynamicFlashLen) {
  log("Validating");
  char printBuf[96];
  DATA_BUS_DDR = 0;
  enableOutput();
  int failures = 0;
  int address;

  for (uint16_t i = 0; i < dynamicFlashLen; i++) {
    address = offset + i;
    setEEPROMAddress(offset + i);
    uint8_t expected = dynamicFlash[i];
    uint8_t actual = DATA_BUS_IN;
    if (expected != actual) {
      failures++;
      sendMonitorCBOR();
      snprintf_P(printBuf, 96, PSTR("Validation failed at address 0x%04x, expected: 0x%02x actual 0x%02x"), 0x8000 + address, expected, actual);
      log(printBuf);
      if (failures > 10) {
        break;
      }
    }
  }
  log("Done");
  return failures == 0;
}

// void printBusDebug() {
//   uint8_t addressBusLsb = ADDRESS_LSB_IN;
//   uint8_t addressBusMsb = ADDRESS_MSB_IN;
//   uint8_t dataBus = DATA_BUS_IN;
//   char rw = rwValue.getHighLow() ? 'r' : 'W';
//   // char oe = digitalRead(outputEnablePin) ? 'O' : 'o';
//   // char we = digitalRead(writeEnablePin) ? 'W' : 'w';
//   // char ace = digitalRead(actualChipEnable) ? 'C' : 'c';
//   // char ce = digitalRead(chipEnablePin) ? 'C' : 'c';
//   Serial.printf(
//     "[%08lu] " BYTE_TO_BINARY_PATTERN BYTE_TO_BINARY_PATTERN "  " BYTE_TO_BINARY_PATTERN "  %02x%02x  %c %02x \n",
//     millis(),
//     BYTE_TO_BINARY(addressBusMsb), BYTE_TO_BINARY(addressBusLsb), BYTE_TO_BINARY(dataBus),
//     addressBusMsb, addressBusLsb, rw, dataBus
//     // Programmer mode stuff
//     // (%c%c%c[%c])
//     // oe, we, ace, ce
//     );
// }

#endif // HEADER_EEPROM_H
