#ifndef HEADER_PROTOCOL_H
#define HEADER_PROTOCOL_H

#include <Arduino.h>
#include "YACL.h"

uint16_t messageCounter = 0;

void resetMessageCounter() {
  messageCounter = 0;
}

void sendCBOR(const char* kind, CBOR body) {
  CBORArray envelope = CBORArray();
  // First is the message length
  // Use max uint16_t to force it to be a uint16 for protocol reasons
  envelope.append(0xffff);

  // Sequence number
  envelope.append(messageCounter++);

  // Needs acknowledgement
  envelope.append(false);

  CBORPair payload = CBORPair();
  payload.append("kind", kind);
  payload.append("body", body);

  envelope.append(payload);

  uint16_t length = envelope.length();

  uint8_t* buffer = envelope.get_buffer();
  buffer[2] = (length >> 8) & 0xff;
  buffer[3] = length & 0xff;

  Serial.write(buffer, length);
}

void ackMessage(uint16_t num) {
  sendCBOR("Ack", CBOR(num));
}

#endif // HEADER_PROTOCOL_H
