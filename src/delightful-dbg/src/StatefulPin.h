#ifndef STATEFULPIN_H
#define STATEFULPIN_H

#include <Arduino.h>

enum PinState {
  Unknown,
  Falling,
  Low,
  Rising,
  High,
};

class StatefulPin {
  public:
    StatefulPin(uint8_t _pin) : pin(_pin) {}

    void setup(uint8_t mode = INPUT) {
      pinMode(pin, mode);
    }

    PinState update() {
      uint8_t current = digitalReadFast(pin);
      if (current == HIGH) {
        if (state == Low || state == Unknown) {
          state = Rising;
        } else if (state == Rising) {
          state = High;
        }
      } else {
        if (state == High || state == Unknown) {
          state = Falling;
        } else if (state == Falling) {
          state = Low;
        }
      }
      return state;
    }

    // Calls update if state is unknown
    uint8_t getHighLow() {
      if (state == Unknown) {
        update();
      }
      if (state == Low || state == Falling) {
        return LOW;
      }
      return HIGH;
    }

    bool isRising() {
      return state == Rising;
    }

    bool isFalling() {
      return state == Falling;
    }

  private:
    PinState state = PinState::Unknown;
    uint8_t pin;
};

#endif // STATEFULPIN_H