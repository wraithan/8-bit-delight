#ifndef HEADER_6502_H
#define HEADER_6502_H

#include <Arduino.h>
#include "YACL.h"

#include "./StatefulPin.h"
#include "./protocol.h"

// PORT C is Address LSB
#define ADDRESS_LSB_DDR DDRC
#define ADDRESS_LSB_OUT PORTC
#define ADDRESS_LSB_IN  PINC

// PORT F is Address MSB
#define ADDRESS_MSB_DDR DDRF
#define ADDRESS_MSB_OUT PORTF
#define ADDRESS_MSB_IN  PINF

// PORT B is Data
#define DATA_BUS_DDR DDRB
#define DATA_BUS_OUT PORTB
#define DATA_BUS_IN  PINB

// 6502 Control Signals
const uint8_t clockPin = PIN_E7;
const uint8_t rwPin = PIN_E6;
const uint8_t rdyPin = PIN_D0;
const uint8_t busEnablePin = PIN_D1;
const uint8_t resetPin = PIN_D4;


StatefulPin clock(clockPin);
StatefulPin rwValue(rwPin);


void reset6502() {
  pinMode(resetPin, OUTPUT);
  digitalWrite(resetPin, LOW);
  delayMicroseconds(1);
  digitalWrite(resetPin, HIGH);
  pinMode(resetPin, INPUT);
}


void sendMonitorCBOR() {
  uint8_t addressBusLsb = ADDRESS_LSB_IN;
  uint8_t addressBusMsb = ADDRESS_MSB_IN;
  uint16_t address = addressBusLsb | (addressBusMsb << 8);
  uint8_t dataBus = DATA_BUS_IN;
  bool rw = rwValue.getHighLow() == HIGH;
  CBORArray body = CBORArray();
  body.append(address);
  body.append(dataBus);
  body.append(rw);
  sendCBOR("BusMonitor", body);
}

#endif // HEADER_6502_H