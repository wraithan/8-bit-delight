#ifndef HEADER_PRINT_H
#define HEADER_PRINT_H

#include <Arduino.h>
#include "YACL.h"

#include "./protocol.h"

void log(const char* data) {
  CBOR body = CBOR(data);
  sendCBOR("Debug", body);
}

#endif // HEADER_PRINT_H
