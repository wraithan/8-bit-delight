use std::{
    fs::File,
    io::{self, Write},
    path::{Path, PathBuf},
};

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "asm-delight",
    about = "Custom compiler for 6502 and 8-bit delight ROMs"
)]
struct Opt {
    #[structopt(parse(from_os_str))]
    output: PathBuf,
}

fn main() {
    let opt = Opt::from_args();
    println!("Hello, world!\n{:?}", opt);

    let rom = create_rom();
    println!("Created rom: ({})", rom.len());
    save_rom(&rom, &opt.output).expect("Couldn't write rom");
}

fn create_rom() -> Vec<u8> {
    #[rustfmt::skip]
    let program = [
        0xa9, 0b1111_1111, // lda #$ff
        0x8d, 0x02, 0x60,  // sta $6002

        0xa9, 0x55,        // lda #$55
        0x8d, 0x00, 0x60,  // sta $6000

        0xa9, 0xaa,        // lda #$aa
        0x8d, 0x00, 0x60,  // sta $6000

        0x4c, 0x05, 0x80,  // jmp $8005
    ];

    let mut rom = vec![0xEA; 2_usize.pow(15)];
    rom.splice(0..program.len(), program.iter().cloned());

    rom[0x7ffc] = 0x00;
    rom[0x7ffd] = 0x80;
    rom
}

fn save_rom(rom: &[u8], location: &Path) -> io::Result<()> {
    let mut output_file = File::create(location)?;
    output_file.write_all(rom)?;
    Ok(())
}
