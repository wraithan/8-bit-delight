# Preface

Howdy, I'm Wraithan McCarroll, I greatly enjoy tinkering with things. This is my documentation for my 8-bit computer based on [Ben Eater's Kit](https://eater.net/8bit) and [related videos](https://www.youtube.com/playlist?list=PLowKtXNTBypGqImE405J2565dvjafglHU). I'm starting this documentation part of the way through the build, mostly because I took a break from working on this computer after having a few modules built, then had a hard time understanding what I'd already built.

In doing some more research for this project, I came across [rolf-electronics build documentation](https://github.com/rolf-electronics/The-8-bit-SAP-3) which made me realize I should do the same, especially as I've already dabbled in deviating from the original build. 

When doing builds of sorts I tend to have rules. They're arbitrary and mostly serve to keep the project interesting to me. They're not super strict but here they are:

1. The first build of the CPU should be *largely* the base that Ben Eater designed.
2. Anything that lives on the CPU is 8-bit or less.
3. Modules can't be added to the CPU until they've been tested on their own.

![Build as of 2021-03-15](./build-2021-03-15.jpg)