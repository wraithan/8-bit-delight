PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

E  = %10000000
RW = %01000000
RS = %00100000

  .org $8000

reset:
  ldx #$ff
  txs

  lda #%11111111 ; Port B to output mode for data pins
  sta DDRB
  lda #%11100000 ; Port A to output mode for control pins
  sta DDRA

  lda #%00111000 ; Set 8-bit mode; 2-line display; 5x8 font
  jsr lcd_instruction

  lda #%00001111 ; Set Display on; cursor on; blink on;
  jsr lcd_instruction

  lda #%00000110 ; Cursor direction move right; no shift
  jsr lcd_instruction

  lda #%00000001 ; Clear display
  jsr lcd_instruction

  lda #"H"
  jsr print_char

  lda #"e"
  jsr print_char

  lda #"l"
  jsr print_char

  lda #"l"
  jsr print_char

  lda #"o"
  jsr print_char

  lda #","
  jsr print_char

  lda #" "
  jsr print_char

  lda #"w"
  jsr print_char

  lda #"o"
  jsr print_char

  lda #"r"
  jsr print_char

  lda #"l"
  jsr print_char

  lda #"d"
  jsr print_char

  lda #"!"
  jsr print_char

  jmp loop

  .org $9000
loop:
  nop
  jmp loop

  .org $a000
lcd_instruction:
  sta PORTB
  lda #0         ; clear RS/RW/E
  sta PORTA
  lda #E         ; Send E to send instruction
  sta PORTA
  lda #0         ; clear RS/RW/E
  sta PORTA
  rts

print_char:
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  rts

  .org $fffc
  .word reset
  .word $0000