PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

E  = %10000000
RW = %01000000
RS = %00100000

LCD_CLEAR_DISPLAY = %00000001
LCD_RETURN_HOME = %00000010 ; LSB unused
LCD_ENTRY_MODE_SET = %00000100 ; cursor move right, shift on
LCD_DISPLAY_ON_OFF = %00001000 ; display on, cursor on, cursor blinking on
LCD_CURSOR_DISPLAY_SHIFT = %00010000 ; display shift, right shift, 2 LSB unused
LCD_FUNCTION_SET = %00100000 ; interface data len, number of lines, font, 2 LSB unused
LCD_SET_CGRAM = %01000000 ; 6 LSBs are the address
LCD_SET_DDRAM = %10000000 ; 7 LSBs are the address

LCD_BUSY_FLAG = %10000000

  .org $8000

reset:
  ldx #$ff
  txs

  lda #%11111111 ; Port B to output mode for data pins
  sta DDRB
  lda #%11100000 ; Port A to output mode for control pins
  sta DDRA

  lda #(LCD_FUNCTION_SET | %11000) ; Set 8-bit mode; 2-line display; 5x8 font
  jsr lcd_instruction

  lda #(LCD_DISPLAY_ON_OFF | %111) ; Set Display on; cursor on; blink on;
  jsr lcd_instruction

  lda #(LCD_ENTRY_MODE_SET | %10) ; Cursor direction move right; no shift
  jsr lcd_instruction

  lda #LCD_CLEAR_DISPLAY
  jsr lcd_instruction


print:
  ldx #0
print_first_loop:
  lda first_line,x
  beq print_second
  jsr print_char
  inx
  jmp print_first_loop

print_second:
  lda #(LCD_SET_DDRAM | $40 ) ; Move to second line which is 0x40
  jsr lcd_instruction

  ldx #0
print_second_loop:
  lda second_line,x
  beq print_done
  jsr print_char
  inx
  jmp print_second_loop

print_done:
  jmp halt_loop

  .org $9000
halt_loop:
  nop
  jmp halt_loop

  .org $a000
lcd_wait:
  pha            ; Push A onto stack
  lda #%00000000 ; Port B input mode
  sta DDRB

lcd_busy:
  lda #RW       ; Switch to RW mode
  sta PORTA
  lda #(RW | E) ; Send E to send instruction
  sta PORTA
  lda PORTB
  and #LCD_BUSY_FLAG
  bne lcd_busy  ; Loop if busy bit was set

  lda #RW
  sta PORTA
  lda #%11111111 ; Port B output mode
  sta DDRB
  pla            ; Pop A back off the stack
  rts

lcd_instruction:
  jsr lcd_wait
  sta PORTB
  lda #0         ; clear RS/RW/E
  sta PORTA
  lda #E         ; Send E to send instruction
  sta PORTA
  lda #0         ; clear RS/RW/E
  sta PORTA
  rts

print_char:
  jsr lcd_wait
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  rts


  .org $b000
first_line: .asciiz "Hello, world!"
second_line: .asciiz "- 65c02 Delight"

  .org $fffc
  .word reset
  .word $0000
