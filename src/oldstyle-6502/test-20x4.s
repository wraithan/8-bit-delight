PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

SR = $600a
ACR = $600b
PCR = $600c

E  = %10000000
RW = %01000000
RS = %00100000

DISPLAY_COLORS = %0110 ; 4 LSB of PORTA, 2 MSBs blue, 2 LBSs constrast, upper then lower

LCD_CLEAR_DISPLAY = %00000001
LCD_RETURN_HOME = %00000010 ; LSB unused
LCD_ENTRY_MODE_SET = %00000100 ; cursor move right, shift on
LCD_DISPLAY_ON_OFF = %00001000 ; display on, cursor on, cursor blinking on
LCD_CURSOR_DISPLAY_SHIFT = %00010000 ; display shift, right shift, 2 LSB unused
LCD_FUNCTION_SET = %00100000 ; interface data len, number of lines, font, 2 LSB unused
LCD_SET_CGRAM = %01000000 ; 6 LSBs are the address
LCD_SET_DDRAM = %10000000 ; 7 LSBs are the address

LCD_DDRAM_FIRST_LINE = $00
LCD_DDRAM_SECOND_LINE = $40
LCD_DDRAM_THIRD_LINE = $14
LCD_DDRAM_FOURTH_LINE = $54

LCD_BUSY_FLAG = %10000000


; System RAM Map
SYS_RAM_LCD_SETTINGS_A = $0200 ; two bytes,
SYS_RAM_LCD_SETTINGS_B = $0201 ; red green blue contrast, $0200 = rrrrgggg, $0201 = bbbbcccc


  .org $8000

reset:
  ldx #$ff
  txs

  ; jsr sr_dance

  lda #%11111111 ; Port B to output mode for data pins
  sta DDRB
  lda #%11101111 ; Port A to output mode for control pins
  sta DDRA


  lda #(LCD_FUNCTION_SET | %11000) ; Set 8-bit mode; 2-line display; 5x8 font
  jsr lcd_instruction

  lda #(LCD_DISPLAY_ON_OFF | %111) ; Set Display on; cursor on; blink on;
  jsr lcd_instruction

  lda #(LCD_ENTRY_MODE_SET | %10) ; Cursor direction move right; no shift
  jsr lcd_instruction

  lda #LCD_CLEAR_DISPLAY
  jsr lcd_instruction


print:
  ldx #0
print_first_loop:
  lda first_line,x
  beq print_second
  jsr print_char
  inx
  jmp print_first_loop

print_second:
  lda #(LCD_SET_DDRAM | LCD_DDRAM_THIRD_LINE ) ; Move to second line which is 0x40
  jsr lcd_instruction

  ldx #0

print_second_loop:
  lda second_line,x
  beq print_done
  jsr print_char
  inx
  jmp print_second_loop

print_done:
  jmp halt_loop

  .org $9000
halt_loop:
  jsr sr_dance
  jmp halt_loop

  .org $a000
lcd_wait:
  pha            ; Push A onto stack
  lda #%00000000 ; Port B input mode
  sta DDRB

lcd_busy:
  lda #( DISPLAY_COLORS | RW )       ; Switch to RW mode
  sta PORTA
  lda #( DISPLAY_COLORS | RW | E ) ; Send E to send instruction
  sta PORTA
  lda PORTB
  and #LCD_BUSY_FLAG
  bne lcd_busy  ; Loop if busy bit was set

  lda #( DISPLAY_COLORS | RW )
  sta PORTA
  lda #%11111111 ; Port B output mode
  sta DDRB
  pla            ; Pop A back off the stack
  rts

lcd_instruction:
  jsr lcd_wait
  sta PORTB
  lda #DISPLAY_COLORS         ; clear RS/RW/E
  sta PORTA
  lda #(DISPLAY_COLORS | E)         ; Send E to send instruction
  sta PORTA
  lda #DISPLAY_COLORS         ; clear RS/RW/E
  sta PORTA
  rts

print_char:
  jsr lcd_wait
  sta PORTB
  lda #( DISPLAY_COLORS | RS )         ; set RS, clear RW/E
  sta PORTA
  lda #( DISPLAY_COLORS | E | RS ) ; Send E to send instruction
  sta PORTA
  lda #( DISPLAY_COLORS | RS )         ; set RS, clear RW/E
  sta PORTA
  rts

sr_dance:
  pha
  phx
  phy
  lda #$fc ;; Turns CA2 off
  sta PCR

  lda #%00011000
  sta ACR

  ldx #0

sr_loop:
  lda light_cycle,x
  sta SYS_RAM_LCD_SETTINGS_A
  sta SYS_RAM_LCD_SETTINGS_B
  jsr write_display_settings_to_sr

  inx
  cpx light_cycle_len
  bne sr_loop

  ply
  plx
  pla
  rts

; Needs to be improved to take in an array and only pulse at the end
; Should use the pulse feature instead of toggling myself
write_sr:
  pha
  sta SR
  nop ; Need to confirm that this timing is actually right
  nop ; 3 NOP is the least that worked reliably
  nop
  lda #$fc ;; Turns CA2 off
  sta PCR
  lda #$ff ;; Turns CA2 on
  sta PCR
  pla
  rts

write_display_settings_to_sr:
  pha
  lda SYS_RAM_LCD_SETTINGS_A
  sta SR
  nop
  nop
  nop
  nop
  nop
  nop
  lda SYS_RAM_LCD_SETTINGS_B
  sta SR
  nop
  nop
  nop
  nop
  nop
  nop
  lda #$fc ;; Turns CA2 off
  sta PCR
  lda #$ff ;; Turns CA2 on
  sta PCR
  pla
  rts

silly_sleep:
  phx
  phy
  ldx #00
  ldy #00
silly_sleep_loop:
  iny


silly_sleep_loop_inner:
  inx
  cpx #$ff
  bne silly_sleep_loop_inner

  cpy #$10
  bne silly_sleep_loop

  plx
  ply
  rts

  .org $b000
first_line: .asciiz "Hello world!"
second_line: .asciiz "    -65c02 Delight"
light_cycle_len: .byte 8
light_cycle:
  .byte %11111111
  .byte %01110111
  .byte %00110011
  .byte %00010001
  .byte %00000000
  .byte %00010001
  .byte %00110011
  .byte %01110111

  .org $fffa
  .word reset ; NMI Vector
  .word reset ; RES Vector
  .word reset ; BRK/IRQ Vector
