PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

E  = %10000000
RW = %01000000
RS = %00100000

  .org $8000

reset:
  lda #%11111111 ; Port B to output mode for data pins
  sta DDRB
  lda #%11100000 ; Port A to output mode for control pins
  sta DDRA

  lda #%00111000 ; Set 8-bit mode; 2-line display; 5x8 font
  sta PORTB
  lda #0         ; clear RS/RW/E
  sta PORTA
  lda #E         ; Send E to send instruction
  sta PORTA
  lda #0        ; clear RS/RW/E
  sta PORTA

  lda #%00001111 ; Set Display on; cursor on; blink on;
  sta PORTB
  lda #0         ; clear RS/RW/E
  sta PORTA
  lda #E         ; Send E to send instruction
  sta PORTA
  lda #0         ; clear RS/RW/E
  sta PORTA

  lda #%00000110 ; Cursor direction move right; no shift
  sta PORTB
  lda #0         ; clear RS/RW/E
  sta PORTA
  lda #E          ; Send E to send instruction
  sta PORTA
  lda #0         ; clear RS/RW/E
  sta PORTA

  lda #%00000001 ; Clear display
  sta PORTB
  lda #0         ; clear RS/RW/E
  sta PORTA
  lda #E         ; Send E to send instruction
  sta PORTA
  lda #0         ; clear RS/RW/E
  sta PORTA

  lda #"H"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"e"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"l"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"l"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"o"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #","        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #" "        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"w"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"o"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"r"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"l"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"d"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

  lda #"!"        ; Write letter
  sta PORTB
  lda #RS         ; set RS, clear RW/E
  sta PORTA
  lda #( E | RS ) ; Send E to send instruction
  sta PORTA
  lda #RS         ; set RS, clear RW/E
  sta PORTA

loop:
  nop
  jmp loop

  .org $fffc
  .word reset
  .word $0000