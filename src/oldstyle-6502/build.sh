#!/usr/bin/env bash

set -e

PROGRAM_NAME="test-20x4"

# -wdc02 enables 65C02 instructions
# -Fbin simple binary output
# -dotdir dots before directives
../../local-tools/vasm6502_oldstyle -wdc02 -Fbin -dotdir -L build/$PROGRAM_NAME.list -o build/$PROGRAM_NAME.bin $PROGRAM_NAME.s

pushd ../delightful-cli
    cargo run --release -- flash ../oldstyle-6502/build/$PROGRAM_NAME.bin
    cargo run --release -- --monitor reset6502
popd
# minipro -p AT28C256 -w build/$PROGRAM_NAME.bin