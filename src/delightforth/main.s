    .setcpu "65C02"


    .macro  PString Arg
        .byte   .strlen(Arg), Arg
    .endmacro

    .segment "SYS_RAM"

result: .res 4
input_counter: .res 1


    .rodata

; Dictionary
; Alphabetized linked list of words
word_alpha:
    .addr word_hi
    PString "alpha"
    .byte $01

word_hi:
    .addr word_howdy
    PString "hi"
    .byte $02

word_howdy:
    .addr $0000 ; nullptr means end of list
    PString "howdy"
    .byte $03

input:
    PString "alpha"
    PString "beta"
    PString "hi"
    PString "howdy"
    .byte $00 ; null terminated list

    .code

reset:
    ; Zero Results
    ldx #4
    @result_loop:
    dex
    stz result,x
    bne @result_loop
    ; Zero input counter
    stz input_counter
    rts

irq:
nmi:
    rti


    .segment "VECTORS"
    .addr irq
    .addr reset
    .addr nmi
