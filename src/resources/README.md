# Resources

All of these resources I've gone through and meaningful changes happened to my CPU and I take inspiration from.

## Ben Eater

The man, myth, legend that started the process for me. I watched his video [The world's worst video card?](https://www.youtube.com/watch?v=l7rce6IQDWs) and was hooked by the idea of building up from logic ICs. I'd watched a few of the 8-bit breadboard computer series before, but the VGA card really sparked my desire to also build some of this stuff.

* [8-bit breadboard computer playlist](https://www.youtube.com/playlist?list=PLowKtXNTBypGqImE405J2565dvjafglHU) has the full SAP-1 build using TTL logic via the 7400 LS series logic. My build is almost entirely built off this guide so far.
* [6502 based computer playlist](https://www.youtube.com/playlist?list=PLowKtXNTBypFbtuVMUVXNR0z1mu7dp7eH) I built this over the weekend of March 27th, 2021, then proceeded to start modding it with in situ EEPROM programming. You can find code related to this in the 8-bit-delight repo. I'll add writing about it soon.

## [r/beneater](https://www.reddit.com/r/beneater/)

This is the subreddit for those work toward building the various projects that Ben Eater has put on his channel, then extending them. Lots a great resources available here and seems pretty welcoming.

* u/lordmonoxide [What I Have Learned](https://www.reddit.com/r/beneater/comments/dskbug/what_i_have_learned_a_master_list_of_what_to_do/)
* u/CalliGuy [Helpful Tips and Recommendations](https://www.reddit.com/r/beneater/comments/ii113p/helpful_tips_and_recommendations_for_ben_eaters/)

## Other builds
* rolf-electronics [The-8-bit-SAP-3](https://github.com/rolf-electronics/The-8-bit-SAP-3)
* Wilson Mines Co. has a whole [6502 Primer](http://wilsonminesco.com/6502primer/) section
    * [The Circuit Potpourri](https://wilsonminesco.com/6502primer/potpourri.html) Base of this and the Ben Eater builds are functionally identical. Goes through various ways to extend it, where I understood using the 6522's shift register capabilities for some display logic I needed.

## Tools
* Volker Barthelmann [vasm](http://sun.hasenbraten.de/vasm/release/vasm.html) Assembler I've been using for 6502 stuff. Recommended by Ben Eater in his video series.

## CBOR

[CBOR](https://en.wikipedia.org/wiki/CBOR) is the binary serialization used by the [Delightful Debug Protocol](../delightful-debug-protocol.md) and this could be useful if doing your own dev with CBOR or are just curious.

* [YACL](https://github.com/imt-atlantique/YACL) is a C++ Arduino platform CBOR (de)serializer. Used by [Delightful Debugger](../delightful-dbg/index.md).
* [serde_cbor](https://github.com/pyfisch/cbor) is a Rust crate for using CBOR with [serde](https://github.com/serde-rs/serde) it has a longer history than others, and a variety of active contributors but is undergoing a significant rewrite. Until that rewrite completes I decided to use ciborium.
* [ciborium](https://github.com/enarx/ciborium) is another Rust crate for using CBOR with serde, this is a newer crate that didn't want to wait for the serde_cbor rewrite to complete along with some differing takes on CBOR Tag support. [Delightful CLI](../delightful-cli/index.md) currently uses this as it's CBOR (de)serializer.
* [CBOR Playground](http://cbor.me/) is an online tool that takes "Diagnostic notation" and hex binary notation of CBOR data and translates between. Has been invaluable when debugging my own code and understanding exactly what the libraries were outputting.
* [CBOR RFC 8949](https://www.rfc-editor.org/rfc/rfc8949) the spec for CBOR rendered as HTML helpful for understanding the exact data model, as well as considerations to make when serializing or creating protocols based on the data format.

## Future

Resources that look useful down the line, whether that's more computers I want to build, upgrades I think look neat but haven't dove into the details, or programming related assembly and related tools.
* [Error detection playlist](https://www.youtube.com/playlist?list=PLowKtXNTBypFWff2QjXCWuSfJDWcvE0Vm)
* u/MironV [Step-by-Step Guide to Upgrading the RAM with Pictures](https://www.reddit.com/r/beneater/comments/h8y28k/stepbystep_guide_to_upgrading_the_ram_with/)
* Ben Eater's [Networking tutorial](https://www.youtube.com/playlist?list=PLowKtXNTBypH19whXTVoG3oKSuOcw_XeW) playlist was fun dive into a lot of details of how networking actually works. I plan to revisit this since it's been a few years since I watched it.