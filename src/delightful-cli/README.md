# Delightful CLI

This is the Command Line Interface that talks with the [Delightful Debugger](../delightful-dbg/index.md) I've written to run on a [Teensy++ 2.0](https://www.pjrc.com/store/teensypp.html)