use std::path::PathBuf;

use structopt::StructOpt;

use crate::{client::DeviceConnection, protocol::DelightfulMessage};

#[derive(Debug, StructOpt)]
pub struct Opt {
    /// Whether to engage monitoring
    #[structopt(short, long)]
    pub monitor: bool,

    #[structopt(subcommand)]
    pub command: Command,
}

#[derive(Debug, StructOpt)]
pub enum Command {
    Hello,
    Reset6502,
    Flash(FlashData),
}

#[derive(Debug, StructOpt)]
pub struct FlashData {
    #[structopt(parse(from_os_str))]
    filename: PathBuf,

    #[structopt(short, long, default_value = "1024")]
    chunk_size: usize,
}

impl Command {
    pub fn run(&self, connection: &mut DeviceConnection, monitor: bool) {
        let mut should_listen = monitor;
        if monitor {
            connection.send_message(DelightfulMessage::Monitor(true), false);
        }

        match self {
            Command::Hello => {
                let num = connection.send_message(DelightfulMessage::Hello, true);
                connection.listen_for_ack(num);
                should_listen = true;
            }
            Command::Reset6502 => {
                let num = connection.send_message(DelightfulMessage::Reset6502, true);
                connection.listen_for_ack(num);
                should_listen = true;
            }
            Command::Flash(FlashData {
                filename,
                chunk_size,
            }) => {
                println!("Reading {:?}", filename);
                let binary = std::fs::read(&filename).expect("Couldn't read file to flash");
                for (index, chunk) in binary.chunks(*chunk_size).enumerate() {
                    // println!("Start of chunk: {:#04x?}", &chunk[..10]);
                    let num = connection.send_message(
                        DelightfulMessage::Flash {
                            offset: (index * chunk_size) as u16,
                            payload: Vec::from(chunk),
                        },
                        true,
                    );
                    connection.listen_for_ack(num);
                }
                println!("Flash complete resetting 6502!");
                let num = connection.send_message(DelightfulMessage::Reset6502, true);
                connection.listen_for_ack(num);
                println!("Reset acked");
            }
        };
        if should_listen {
            connection.listen();
        }
    }
}
