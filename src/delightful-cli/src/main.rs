mod client;
mod config;
mod protocol;

use serialport::available_ports;
use structopt::StructOpt;

use crate::client::DeviceConnection;
use crate::config::Opt;

fn main() {
    println!("Delightful!");
    let opt = Opt::from_args();
    println!("{:?}", opt);
    let ports = available_ports().expect("couldn't scan ports?");
    for port in ports {
        if let serialport::SerialPortType::UsbPort(info) = &port.port_type {
            if let Some(serial_num) = &info.serial_number {
                if serial_num == "12345" {
                    println!("Found the Teensy!");
                    let mut connection = DeviceConnection::connect(&port.port_name);
                    opt.command.run(&mut connection, opt.monitor);
                    return;
                }
            }
        };
    }
}
