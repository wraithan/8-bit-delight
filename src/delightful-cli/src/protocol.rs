use std::{
    convert::TryInto,
    io::{self, BufRead},
};

use byteorder::{BigEndian, ByteOrder};
use ciborium::{de::from_reader as cbor_from_reader, ser::into_writer as cbor_into_writer};
use serde::{Deserialize, Serialize};
use strum_macros::AsStaticStr;

#[derive(Debug, Deserialize, Serialize, AsStaticStr)]
#[serde(tag = "kind", content = "body")]
pub enum DelightfulMessage {
    Hello,
    Monitor(bool),
    Reset6502,
    Flash {
        offset: u16,
        #[serde(with = "serde_bytes")]
        payload: Vec<u8>,
    },
    Debug(String),
    /// Tuple for compression
    /// address, data, rw pin (read true write false),
    BusMonitor(u16, u8, bool),
    Ack(u16),
}

impl DelightfulMessage {
    /// Adds the message to the provided buffer encoded for the wire
    pub fn to_wire_format(&self, mut output: &mut Vec<u8>, sequence: u16, needs_ack: bool) {
        let vec_starting_len = output.len();
        let placeholder_len = 0xffff_u32;
        let wrapped_message = (placeholder_len, sequence, needs_ack, self);
        cbor_into_writer(&wrapped_message, &mut output)
            .expect("Failed to serialize message to CBOR");
        let length: u16 = (output.len() - vec_starting_len).try_into().unwrap();
        BigEndian::write_u16(
            &mut output[vec_starting_len + 2..vec_starting_len + 4],
            length,
        );
    }
}

pub struct InboundMessage {
    pub sequence: u16,
    pub needs_ack: bool,
    pub message: DelightfulMessage,
}

pub struct InboundMessages<B> {
    serial_buf: Vec<u8>,
    serial_buf_write_ptr: usize,
    buf: B,
}

impl<B: BufRead> From<B> for InboundMessages<B> {
    fn from(buf: B) -> Self {
        Self {
            serial_buf: vec![0; 2048],
            serial_buf_write_ptr: 0,
            buf,
        }
    }
}

impl<B: BufRead> Iterator for InboundMessages<B> {
    type Item = InboundMessage;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self
                .buf
                .read(&mut self.serial_buf[self.serial_buf_write_ptr..])
            {
                Ok(t) => {
                    self.serial_buf_write_ptr += t;
                    // 02 and 03 are a u16 that is the length,
                    // length 0 is invalid so there should also be at least 1 content byte.
                }
                Err(ref e) if e.kind() == io::ErrorKind::TimedOut => (),
                Err(e) => {
                    eprintln!("{:?}", e);
                    return None;
                }
            }
            if self.serial_buf_write_ptr >= 4 {
                let length = BigEndian::read_u16(&self.serial_buf[2..=3]) as usize;
                // println!("Got length: {}", length);
                assert!(
                    length < 2048,
                    "Message size exceeds possible size {} >= {}",
                    length,
                    2048
                );
                if self.serial_buf_write_ptr < length {
                    continue;
                }
                let raw_object: Vec<u8> = self.serial_buf.drain(..length).collect();
                self.serial_buf_write_ptr -= length;
                self.serial_buf.resize(2048, 0);

                if let Ok((_len, num, needs_ack, device_msg)) =
                    cbor_from_reader::<(u16, u16, bool, DelightfulMessage), _>(&raw_object[..])
                {
                    // println!("len: {}, num: {}, need_ack: {}", len, num, needs_ack);
                    return Some(InboundMessage {
                        sequence: num,
                        needs_ack,
                        message: device_msg,
                    });
                    // message_print_count += 1;
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::DelightfulMessage;

    /// Checks specific parts first, then the whole buffer to make it clear what part failed.
    fn test_buffer_is_as_expected_simple(expected: &[u8], actual: &[u8]) {
        assert_eq!(actual[2], expected[2], "checking MSB of message length");
        assert_eq!(actual[3], expected[3], "checking LSB of message length");

        assert_eq!(
            actual.len(),
            expected.len(),
            "checking that whole message is the same length"
        );

        for (index, (expected_byte, actual_byte)) in expected.iter().zip(actual).enumerate() {
            assert_eq!(
                actual_byte, expected_byte,
                "Checking bytes at index: {} (0x{:02x} != 0x{:02x})",
                index, actual_byte, expected_byte
            );
        }
    }

    #[test]
    fn basic_hello_to_binary() {
        let message = DelightfulMessage::Hello;
        let mut buf = vec![];
        message.to_wire_format(&mut buf, 65, true);

        #[rustfmt::skip]
        let expected = [
            0x84, 0x19, 0x00, 0x13, 0x18, 0x41, 0xf5, 0xa1,
            0x64, 0x6b, 0x69, 0x6e, 0x64, 0x65, 0x48, 0x65,
            0x6c, 0x6c, 0x6f
        ];

        test_buffer_is_as_expected_simple(&expected, &buf[..]);
    }

    #[test]
    fn basic_monitor_to_binary() {
        let message = DelightfulMessage::Monitor(false);
        let mut buf = vec![];
        message.to_wire_format(&mut buf, 4739, true);

        #[rustfmt::skip]
        let expected = [
            0x84, 0x19, 0x00, 0x1c, 0x19, 0x12, 0x83, 0xf5,
            0xa2, 0x64, 0x6b, 0x69, 0x6e, 0x64, 0x67, 0x4d,
            0x6f, 0x6e, 0x69, 0x74, 0x6f, 0x72, 0x64, 0x62,
            0x6f, 0x64, 0x79, 0xf4
        ];

        test_buffer_is_as_expected_simple(&expected, &buf[..]);
    }
}
