use std::{
    io::{BufReader, Write},
    sync::mpsc::{sync_channel, Receiver, SyncSender},
    thread,
    time::Duration,
};

use strum::AsStaticRef;

use crate::protocol::{DelightfulMessage, InboundMessage, InboundMessages};

struct OutboundMessage {
    sequence: u16,
    needs_ack: bool,
    message: DelightfulMessage,
}

pub struct DeviceConnection {
    message_count: u16,
    to_device: SyncSender<OutboundMessage>,
    from_device: Receiver<InboundMessage>,
}

impl DeviceConnection {
    pub fn connect(port_name: &str) -> Self {
        let mut read_port = serialport::TTYPort::open(
            &serialport::new(port_name, 12_000_000_u32).timeout(Duration::from_millis(100)),
        )
        .expect("failed to open port");

        read_port
            .set_exclusive(false)
            .expect("failed to make serial port connection non-exclusive");

        let mut write_port = read_port
            .try_clone_native()
            .expect("Failed to make write port");

        let (write_tx, write_rx) = sync_channel(10);

        thread::spawn(move || {
            let mut cbor_message = vec![];
            for OutboundMessage {
                sequence,
                needs_ack,
                message,
            } in write_rx
            {
                message.to_wire_format(&mut cbor_message, sequence, needs_ack);
                println!(
                    "Sending: {:?} ({:x?}...{}) ",
                    message.as_static(),
                    &cbor_message[..7],
                    cbor_message.len()
                );
                write_port
                    .write_all(&cbor_message)
                    .expect("failed to write to serial port");
                cbor_message.clear();
            }
        });

        let (read_tx, read_rx) = sync_channel(100);

        thread::spawn(move || {
            for message in InboundMessages::from(BufReader::new(read_port)) {
                read_tx
                    .send(message)
                    .expect("failed to send InboundMessage from read thread");
            }
        });

        Self {
            message_count: 0,
            to_device: write_tx,
            from_device: read_rx,
        }
    }

    pub fn listen(&self) {
        for InboundMessage { message, .. } in &self.from_device {
            match message {
                DelightfulMessage::Debug(msg) => println!("Debug Message: {}", msg),
                DelightfulMessage::BusMonitor(address, data, rw) => {
                    let rw_flag = if rw { 'r' } else { 'W' };
                    println!(
                        "Bus Monitor: {:016b} {:08b} 0x{:04x} 0x{:02x} {}",
                        address, data, address, data, rw_flag
                    );
                }
                _ => {
                    println!("Unexpected message from device: {:?}", message);
                }
            }
        }
    }

    pub fn listen_for_ack(&self, num: u16) {
        for InboundMessage { message, .. } in &self.from_device {
            println!("Got: {:?} while waiting for {}", message, num);
            if let DelightfulMessage::Ack(msg_num) = message {
                if msg_num == num {
                    return;
                }
            }
        }
    }

    pub fn send_message(&mut self, message: DelightfulMessage, needs_ack: bool) -> u16 {
        let sequence = self.message_count;
        self.message_count += 1;
        self.to_device
            .send(OutboundMessage {
                sequence,
                needs_ack,
                message,
            })
            .expect("failed to send client message to write thread");
        sequence
    }
}
