use std::{fs, time::Instant};

use delightful_emu::{Computer, Settings as ComputerSettings};
use iced::{executor, widget::{self, column, row}, window, Application, Command, Settings, Theme};

fn main() -> iced::Result {
    Emulator::run(Settings::default())
}

struct Emulator {
    computer: Computer,
    free_run: bool,
}

#[derive(Debug)]
enum EmulatorMessage {
    Frame(Instant),
    FreeRun(bool),
}

impl Application for Emulator {
    type Executor = executor::Default;
    type Flags = ();
    type Message = EmulatorMessage;
    type Theme = Theme;

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        println!("new!");

        let settings_text =
            fs::read_to_string("../fixtures/settings.yaml").expect("Couldn't read settings file");
        let settings: ComputerSettings =
            serde_yaml::from_str(&settings_text).expect("Couldn't parse settings file");
        println!("Settings: {:#?}!", settings);
        let computer: Computer = (&settings).into();
        (
            Self {
                computer,
                free_run: false,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        "Delightful Emu - Iced Variant".to_string()
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            EmulatorMessage::Frame(_) => {
                if self.free_run {
                    self.computer.single_step();
                }        
            },
            EmulatorMessage::FreeRun(value) => self.free_run = value,
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<Self::Message> {
        column![
            row![
                widget::toggler(
                    "Free Run".to_string(),
                    self.free_run,
                    |f| EmulatorMessage::FreeRun(f),
                )
            ]
        ].into()
    }

    fn theme(&self) -> Self::Theme {
        Theme::Dark
    }

    fn subscription(&self) -> iced::Subscription<Self::Message> {
        window::frames().map(EmulatorMessage::Frame)
    }
}
