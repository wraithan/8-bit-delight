# Delightful Emu

This is an emulator to help enable my various 8-bit projects. To start with the goal is to be a 65C02 emulator but maybe that'll change as I explore the Z80, 6809, or 68000. Also facilities for emulating support chips like the 6522 VIA or 6551 ACIA. I'm debating who down to the wire I want to be.

I have two main usecases in mind:
 * Be a modular core that lets me build software versions of computers I build
 * Give me a way to write tests for assembly I write. 

## Virtual Computer

There is a huge amount of depth available in this, display means emulating the controller chip for the display, plus actually showing something. ACIA would ideally include a way for me to connect via minicom or other serial communication for live iteraction. Additionally I'll want interfaces for debugging the computer itself such as being able to interact and monitor memory, change clock rate from single step up to MHz speeds.

## Testing

For testing there is a variety of integration and unit level tests I'll want to be able to do. At a full computer level I'll want to be able to initialize RAM/ROM/devices to given states, tell it to execute, then after some criteria stop and validate that RAM/ROM/devices are in an expected state. 

## Things to Build
 * Instruction Set
    - [ ] 6502 - Original NMOS, most portable
    - [ ] 65C02 - Modern WDC CMOS, what I'm actually using
    - [ ] 8051 - aka MCS 51, what the keyboard controller will use via AT89S52
 * Peripherials
    - [ ] 6522 - VIA Versitile Interface Adaptor, provides GPIO General Purpose Input/Output
    - [ ] 6551 - ACIA Asynchronous Communication Interface Adaptor, provides serial connection
    - [ ] Character Display
        - [ ] HD44780 - Character display adaptor, connects to 20x4 Character LCD
        - [ ] Display - 20x4 Character LCD has RGB Backlight
    - [ ] Pixel Display
        - [ ] RA8875 - Display adaptor IC connects to 800x480 display
        - [ ] Display - 800x480 display, connected to RA8875
        - [ ] Touch Screen - Also connected to RA8875
    - Misc Input / Output
        - [ ] Button Matrix - Simple 1x4 or 3x4, or complex full keyboard emulation
        - [ ] Potentiometer - Knob
        - [ ] LED - Single or array
        - [ ] Switch - Single or array
    - IO Abstractions - Abstraction both for debugging and building other peripherials
        - [ ] I2C - Plenty of sensors available
        - [ ] SPI - RA8875 and SD card both could use this