use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
pub struct Opcode {
    pub id: u8,
    pub name: OpcodeName,
    pub mode: OpcodeMode,
}

impl Opcode {
    fn build_add_to_map(id: u8, name: OpcodeName, mode: OpcodeMode, map: &mut HashMap<u8, Opcode>) {
        let result = map.insert(id, Opcode { id, name, mode });
        if let Some(opcode) = result {
            unreachable!("Opcode {} is double booked", opcode.id);
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum OpcodeMode {
    Accumulator,
    Immediate,
    ZeroPage,
    ZeroPageX,
    ZeroPageY,
    Absolute,
    AbsoluteX,
    AbsoluteY,
    Indirect,
    IndirectX,
    IndirectY,
    Implied,
    Relative,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug, Clone, Copy)]
pub enum OpcodeName {
    ADC,
    AND,
    ASL,
    BIT,

    // Branching
    BPL,
    BMI,
    BVC,
    BVS,
    BCC,
    BCS,
    BNE,
    BEQ,

    BRK,

    CMP,
    CPX,
    CPY,

    DEC,
    EOR,

    // Flags
    CLC,
    SEC,
    CLI,
    SEI,
    CLV,
    CLD,
    SED,

    INC,

    JMP,
    JSR,

    LDA,
    LDX,
    LDY,

    LSR,

    NOP,

    ORA,

    // Registers
    TAX,
    TXA,
    DEX,
    INX,
    TAY,
    TYA,
    DEY,
    INY,

    ROL,
    ROR,

    RTI,
    RTS,

    SBC,

    STA,
    STX,
    STY,

    // Stack
    TXS,
    TSX,
    PHA,
    PLA,
    PHP,
    PLP,
}

pub fn build_opcode_map() -> HashMap<u8, Opcode> {
    use OpcodeMode::*;
    use OpcodeName::*;
    let mut map = HashMap::new();
    // ADC
    Opcode::build_add_to_map(0x69, ADC, Immediate, &mut map);
    Opcode::build_add_to_map(0x65, ADC, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x75, ADC, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x6d, ADC, Absolute, &mut map);
    Opcode::build_add_to_map(0x7d, ADC, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0x79, ADC, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0x61, ADC, IndirectX, &mut map);
    Opcode::build_add_to_map(0x71, ADC, IndirectY, &mut map);

    // AND
    Opcode::build_add_to_map(0x29, AND, Immediate, &mut map);
    Opcode::build_add_to_map(0x25, AND, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x35, AND, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x2d, AND, Absolute, &mut map);
    Opcode::build_add_to_map(0x3d, AND, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0x39, AND, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0x21, AND, IndirectX, &mut map);
    Opcode::build_add_to_map(0x31, AND, IndirectY, &mut map);

    // ASL
    Opcode::build_add_to_map(0x0a, ASL, Accumulator, &mut map);
    Opcode::build_add_to_map(0x06, ASL, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x16, ASL, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x0e, ASL, Absolute, &mut map);
    Opcode::build_add_to_map(0x1e, ASL, AbsoluteX, &mut map);

    // BIT
    Opcode::build_add_to_map(0x24, BIT, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x2c, BIT, Absolute, &mut map);

    // Branching
    Opcode::build_add_to_map(0x10, BPL, Relative, &mut map);
    Opcode::build_add_to_map(0x30, BMI, Relative, &mut map);
    Opcode::build_add_to_map(0x50, BVC, Relative, &mut map);
    Opcode::build_add_to_map(0x70, BVS, Relative, &mut map);
    Opcode::build_add_to_map(0x90, BCC, Relative, &mut map);
    Opcode::build_add_to_map(0xb0, BCS, Relative, &mut map);
    Opcode::build_add_to_map(0xd0, BNE, Relative, &mut map);
    Opcode::build_add_to_map(0xf0, BEQ, Relative, &mut map);

    // BRK
    Opcode::build_add_to_map(0x00, BRK, Implied, &mut map);

    // CMP
    Opcode::build_add_to_map(0xc9, CMP, Immediate, &mut map);
    Opcode::build_add_to_map(0xc5, CMP, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xd5, CMP, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0xcd, CMP, Absolute, &mut map);
    Opcode::build_add_to_map(0xdd, CMP, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0xd9, CMP, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0xc1, CMP, IndirectX, &mut map);
    Opcode::build_add_to_map(0xd1, CMP, IndirectY, &mut map);

    // CPX
    Opcode::build_add_to_map(0xe0, CPX, Immediate, &mut map);
    Opcode::build_add_to_map(0xe4, CPX, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xec, CPX, Absolute, &mut map);

    // CPY
    Opcode::build_add_to_map(0xc0, CPY, Immediate, &mut map);
    Opcode::build_add_to_map(0xc4, CPY, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xcc, CPY, Absolute, &mut map);

    // DEC
    Opcode::build_add_to_map(0xc6, DEC, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xd6, DEC, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0xce, DEC, Absolute, &mut map);
    Opcode::build_add_to_map(0xde, DEC, AbsoluteX, &mut map);

    // EOR
    Opcode::build_add_to_map(0x49, EOR, Immediate, &mut map);
    Opcode::build_add_to_map(0x45, EOR, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x55, EOR, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x4d, EOR, Absolute, &mut map);
    Opcode::build_add_to_map(0x5d, EOR, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0x59, EOR, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0x41, EOR, IndirectX, &mut map);
    Opcode::build_add_to_map(0x51, EOR, IndirectY, &mut map);

    // Flags
    Opcode::build_add_to_map(0x18, CLC, Implied, &mut map);
    Opcode::build_add_to_map(0x38, SEC, Implied, &mut map);
    Opcode::build_add_to_map(0x58, CLI, Implied, &mut map);
    Opcode::build_add_to_map(0x78, SEI, Implied, &mut map);
    Opcode::build_add_to_map(0xb8, CLV, Implied, &mut map);
    Opcode::build_add_to_map(0xd8, CLD, Implied, &mut map);
    Opcode::build_add_to_map(0xf8, SED, Implied, &mut map);

    // INC
    Opcode::build_add_to_map(0xe6, INC, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xf6, INC, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0xee, INC, Absolute, &mut map);
    Opcode::build_add_to_map(0xfe, INC, AbsoluteX, &mut map);

    // JMP
    Opcode::build_add_to_map(0x4c, JMP, Absolute, &mut map);
    Opcode::build_add_to_map(0x6c, JMP, Indirect, &mut map);

    // JSR
    Opcode::build_add_to_map(0x20, JSR, Absolute, &mut map);

    // LDA
    Opcode::build_add_to_map(0xa9, LDA, Immediate, &mut map);
    Opcode::build_add_to_map(0xa5, LDA, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xb5, LDA, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0xad, LDA, Absolute, &mut map);
    Opcode::build_add_to_map(0xbd, LDA, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0xb9, LDA, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0xa1, LDA, IndirectX, &mut map);
    Opcode::build_add_to_map(0xb1, LDA, IndirectY, &mut map);

    // LDX
    Opcode::build_add_to_map(0xa2, LDX, Immediate, &mut map);
    Opcode::build_add_to_map(0xa6, LDX, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xb6, LDX, ZeroPageY, &mut map);
    Opcode::build_add_to_map(0xae, LDX, Absolute, &mut map);
    Opcode::build_add_to_map(0xbe, LDX, AbsoluteY, &mut map);

    // LDY
    Opcode::build_add_to_map(0xa0, LDY, Immediate, &mut map);
    Opcode::build_add_to_map(0xa4, LDY, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xb4, LDY, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0xac, LDY, Absolute, &mut map);
    Opcode::build_add_to_map(0xbc, LDY, AbsoluteX, &mut map);

    // LSR
    Opcode::build_add_to_map(0x4a, LSR, Accumulator, &mut map);
    Opcode::build_add_to_map(0x46, LSR, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x56, LSR, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x4e, LSR, Absolute, &mut map);
    Opcode::build_add_to_map(0x5e, LSR, AbsoluteX, &mut map);

    // NOP
    Opcode::build_add_to_map(0xea, NOP, Implied, &mut map);

    // ORA
    Opcode::build_add_to_map(0x09, ORA, Immediate, &mut map);
    Opcode::build_add_to_map(0x05, ORA, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x15, ORA, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x0d, ORA, Absolute, &mut map);
    Opcode::build_add_to_map(0x1d, ORA, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0x19, ORA, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0x01, ORA, IndirectX, &mut map);
    Opcode::build_add_to_map(0x11, ORA, IndirectY, &mut map);

    // Registers
    Opcode::build_add_to_map(0xaa, TAX, Implied, &mut map);
    Opcode::build_add_to_map(0x8a, TXA, Implied, &mut map);
    Opcode::build_add_to_map(0xca, DEX, Implied, &mut map);
    Opcode::build_add_to_map(0xe8, INX, Implied, &mut map);
    Opcode::build_add_to_map(0xa8, TAY, Implied, &mut map);
    Opcode::build_add_to_map(0x98, TYA, Implied, &mut map);
    Opcode::build_add_to_map(0x88, DEY, Implied, &mut map);
    Opcode::build_add_to_map(0xc8, INY, Implied, &mut map);

    // ROL
    Opcode::build_add_to_map(0x2a, ROL, Accumulator, &mut map);
    Opcode::build_add_to_map(0x26, ROL, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x36, ROL, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x2e, ROL, Absolute, &mut map);
    Opcode::build_add_to_map(0x3e, ROL, AbsoluteX, &mut map);

    // ROR
    Opcode::build_add_to_map(0x6a, ROR, Accumulator, &mut map);
    Opcode::build_add_to_map(0x66, ROR, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x76, ROR, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x6e, ROR, Absolute, &mut map);
    Opcode::build_add_to_map(0x7e, ROR, AbsoluteX, &mut map);

    // RTI
    Opcode::build_add_to_map(0x40, RTI, Implied, &mut map);

    // RTS
    Opcode::build_add_to_map(0x60, RTS, Implied, &mut map);

    // SBC
    Opcode::build_add_to_map(0xe9, SBC, Immediate, &mut map);
    Opcode::build_add_to_map(0xe5, SBC, ZeroPage, &mut map);
    Opcode::build_add_to_map(0xf5, SBC, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0xed, SBC, Absolute, &mut map);
    Opcode::build_add_to_map(0xfd, SBC, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0xf9, SBC, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0xe1, SBC, IndirectX, &mut map);
    Opcode::build_add_to_map(0xf1, SBC, IndirectY, &mut map);

    // STA
    Opcode::build_add_to_map(0x85, STA, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x95, STA, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x8d, STA, Absolute, &mut map);
    Opcode::build_add_to_map(0x9d, STA, AbsoluteX, &mut map);
    Opcode::build_add_to_map(0x99, STA, AbsoluteY, &mut map);
    Opcode::build_add_to_map(0x81, STA, IndirectX, &mut map);
    Opcode::build_add_to_map(0x91, STA, IndirectY, &mut map);

    // STX
    Opcode::build_add_to_map(0x86, STX, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x96, STX, ZeroPageY, &mut map);
    Opcode::build_add_to_map(0x8e, STX, Absolute, &mut map);

    // STY
    Opcode::build_add_to_map(0x84, STY, ZeroPage, &mut map);
    Opcode::build_add_to_map(0x94, STY, ZeroPageX, &mut map);
    Opcode::build_add_to_map(0x8c, STY, Absolute, &mut map);

    // Stack
    Opcode::build_add_to_map(0x9a, TXS, Implied, &mut map);
    Opcode::build_add_to_map(0xba, TSX, Implied, &mut map);
    Opcode::build_add_to_map(0x48, PHA, Implied, &mut map);
    Opcode::build_add_to_map(0x68, PLA, Implied, &mut map);
    Opcode::build_add_to_map(0x08, PHP, Implied, &mut map);
    Opcode::build_add_to_map(0x28, PLP, Implied, &mut map);

    map
}
