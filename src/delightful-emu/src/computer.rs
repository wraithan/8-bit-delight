use std::collections::{BTreeMap, HashMap};

use crate::opcodes::{build_opcode_map, Opcode, OpcodeMode, OpcodeName};

pub(crate) type MemoryTree = BTreeMap<u16, Device>;

pub struct Computer {
    // Registers
    accumulator: u8,      // A
    register_x: u8,       // X
    register_y: u8,       // Y
    program_counter: u16, // PC
    stack_pointer: u8,    // S
    status: StatusFlags,  // P

    mode: ComputerMode,
    memory: MemoryTree,
    opcodes: HashMap<u8, Opcode>,
}

#[derive(Debug, PartialEq, Eq)]
enum ComputerMode {
    Booting(BootMode),
    Running,
}

// TODO: make a `impl From<u8> for StatusFlags`
struct StatusFlags {
    carry: bool,             // C
    zero: bool,              // Z
    interrupt_disable: bool, // I
    decimal: bool,           // D
    r#break: bool,           // B
    unused: bool,            // always true,
    overflow: bool,          // V
    negative: bool,          // N
}

impl Default for StatusFlags {
    fn default() -> Self {
        Self {
            carry: false,
            zero: false,
            interrupt_disable: false,
            decimal: false,
            r#break: false,
            unused: true,
            overflow: false,
            negative: false,
        }
    }
}

impl StatusFlags {
    fn update_nz_from_u8(&mut self, value: u8) {
        self.negative = (value >> 7) > 0;
        self.zero = value == 0;
    }
}

impl From<&StatusFlags> for u8 {
    fn from(flags: &StatusFlags) -> Self {
        let StatusFlags {
            carry,
            zero,
            interrupt_disable,
            decimal,
            r#break,
            unused,
            overflow,
            negative,
        } = flags;

        u8::from(*carry)
            | u8::from(*zero) << 1
            | u8::from(*interrupt_disable) << 2
            | u8::from(*decimal) << 3
            | u8::from(*r#break) << 4
            | u8::from(*unused) << 5
            | u8::from(*overflow) << 6
            | u8::from(*negative) << 7
    }
}

pub(crate) struct Device {
    pub name: String,
    pub start: u16,
    pub end: u16,
    pub memory: MemoryBlock,
}

pub(crate) enum MemoryBlock {
    Rom(Vec<u8>),
    Ram(Vec<u8>),
}


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum BootMode {
    Res(ResetSteps),
    BrkIrqNmi,
}

impl Default for BootMode {
    fn default() -> Self {
        Self::Res(ResetSteps::Init)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum ResetSteps {
    Init,
    Noop1,
    Noop2,
    StorePCA,
    StorePCB,
    StoreP,
    ReadVectorLow,
    ReadVectorHigh,
}

struct ReadAddress {
    result: Option<u16>,
    new_offset: u16,
}

impl Computer {
    pub fn print_addr(&self, addr: u16) {
        println!(
            "{:#06x}: {:02x}",
            addr,
            self.lookup_address(addr).expect("segmentation fault")
        );
    }

    fn lookup_address(&self, addr: u16) -> Option<u8> {
        for device in self.memory.values() {
            if device.start <= addr && device.end >= addr {
                let offset = addr - device.start;
                return Some(device.read(offset));
            }
        }
        None
    }

    fn write_to_address(&mut self, addr: u16, value: u8) -> Option<bool> {
        for device in self.memory.values_mut() {
            if device.start <= addr && device.end >= addr {
                let offset = addr - device.start;
                return Some(device.write(offset, value));
            }
        }
        None
    }

    pub(crate) fn from_memory(memory: MemoryTree) -> Self {
        Self {
            accumulator: 0xaa,
            register_x: 0,
            register_y: 0,
            program_counter: 0x00ff,
            stack_pointer: 0,
            status: Default::default(),

            mode: ComputerMode::Booting(BootMode::default()),
            memory,
            opcodes: build_opcode_map(),
        }
    }

    /// Runs a single instruction and returns whether that instruction was BRK
    pub fn single_step(&mut self) {
        match self.mode {
            ComputerMode::Booting(boot_mode) => self.single_step_boot(boot_mode),
            ComputerMode::Running => self.single_step_run(),
        }
    }

    fn single_step_boot(&mut self, boot_mode: BootMode) {
        match boot_mode {
            BootMode::Res(ResetSteps::Init) => {
                self.stack_pointer = 0;
                self.program_counter = 0x0200;
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::Noop1));
            }
            BootMode::Res(ResetSteps::Noop1) => {
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::Noop2));
            }
            BootMode::Res(ResetSteps::Noop2) => {
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::StorePCA));
            }
            BootMode::Res(ResetSteps::StorePCA) => {
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::StorePCB));
            }
            BootMode::Res(ResetSteps::StorePCB) => {
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::StoreP));
            }
            BootMode::Res(ResetSteps::StoreP) => {
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::ReadVectorLow));
                self.program_counter =
                    self.lookup_address(0xfffc)
                        .expect("couldn't read reset vector low") as u16;
            }
            BootMode::Res(ResetSteps::ReadVectorLow) => {
                self.mode = ComputerMode::Booting(BootMode::Res(ResetSteps::ReadVectorHigh));
                self.program_counter |= (self
                    .lookup_address(0xfffd)
                    .expect("couldn't read reset vector high")
                    as u16)
                    << 8;
            }
            BootMode::Res(ResetSteps::ReadVectorHigh) => {
                self.mode = ComputerMode::Running;
            }
            BootMode::BrkIrqNmi => todo!(),
        }
    }

    fn get_next_address(&mut self, mode: OpcodeMode) -> Result<Option<u16>, String> {
        let result = self.read_address_with_mode(self.program_counter, mode)?;
        self.program_counter = result.new_offset;
        Ok(result.result)
    }

    fn get_next_program_value(&mut self, mode: OpcodeMode) -> Option<u8> {
        let addr = self
            .get_next_address(mode)
            .expect("Failed to read next address");
        self.lookup_address(addr.expect("expected another address"))
    }

    fn single_step_run(&mut self) {
        // println!("PC: ${:04x} SP: ${:02x} A: ${:02x} X: ${:02x} Y: ${:02x}", self.program_counter, self.stack_pointer, self.accumulator, self.register_x, self.register_y);
        let instruction = self
            .get_next_program_value(OpcodeMode::Immediate)
            .expect("failed to read address");
        // println!("Instruction: ${instruction:02x}");
        let opcode = self.opcodes.get(&instruction).expect("Unknown opcode");
        match opcode {
            Opcode {
                name: OpcodeName::BRK,
                ..
            } => {
                // BRK
                self.program_counter += 1;
                // TODO write program counter to the stack?
                self.status.r#break = true;
                self.mode = ComputerMode::Booting(BootMode::BrkIrqNmi);
            }
            Opcode {
                name: OpcodeName::LDA,
                mode,
                ..
            } => {
                let value = self
                    .get_next_program_value(*mode)
                    .expect("failed to get ldx argument");
                self.accumulator = value;
                self.status.update_nz_from_u8(value);
            }
            Opcode {
                name: OpcodeName::LDX,
                mode,
                ..
            } => {
                let value = self
                    .get_next_program_value(*mode)
                    .expect("failed to get ldx argument");
                self.register_x = value;
                self.status.update_nz_from_u8(value);
            }
            Opcode {
                name: OpcodeName::LDY,
                mode,
                ..
            } => {
                let value = self
                    .get_next_program_value(*mode)
                    .expect("failed to get ldy argument");
                self.register_y = value;
                self.status.update_nz_from_u8(value);
            }
            Opcode {
                name: OpcodeName::NOP,
                ..
            } => {
                // NOP
            }
            Opcode {
                name: OpcodeName::TXS,
                ..
            } => {
                self.stack_pointer = self.register_x;
            }
            Opcode {
                name: OpcodeName::TSX,
                ..
            } => {
                self.register_x = self.stack_pointer;
            }
            Opcode {
                name: OpcodeName::STA,
                mode,
                ..
            } => {
                let addr = self
                    .get_next_address(*mode)
                    .expect("failed to get sta argument")
                    .expect("bad read mode for sta, no argument");
                self.write_to_address(addr, self.accumulator)
                    .expect("failed to write");
            }
            Opcode {
                name: OpcodeName::JMP,
                mode,
                ..
            } => {
                let addr = self
                    .get_next_address(*mode)
                    .expect("failed to get jmp argument")
                    .expect("bad read mode for jmp, no argument");
                self.program_counter = addr;
            }
            _ => unimplemented!("Instruction {instruction:#04x}!"),
        }
        // println!(
        //     "PC: ${:04x} SP: ${:02x} A: ${:02x} X: ${:02x} Y: ${:02x}",
        //     self.program_counter,
        //     self.stack_pointer,
        //     self.accumulator,
        //     self.register_x,
        //     self.register_y
        // );
    }

    fn read_address_with_mode(&self, offset: u16, mode: OpcodeMode) -> Result<ReadAddress, String> {
        let mut new_offset = offset;
        // TODO Addressing modes are more complex, account for paging as needed here
        let result = match mode {
            OpcodeMode::Accumulator => todo!("Accumulator lookup mode"),
            OpcodeMode::Immediate => {
                new_offset = new_offset.wrapping_add(1);
                Some(new_offset)
            }
            OpcodeMode::ZeroPage => {
                new_offset = new_offset.wrapping_add(1);
                let addr = self
                    .lookup_address(new_offset)
                    .map(u16::from)
                    .ok_or_else(|| format!("Couldn't read zp mode address {new_offset}"))?;
                Some(addr)
            }
            OpcodeMode::ZeroPageX => {
                new_offset = new_offset.wrapping_add(1);
                let addr = self.lookup_address(new_offset)
                    .map(|v| v.wrapping_add(self.register_x) as u16)
                    .ok_or_else(|| format!("Couldn't read zpx mode address {new_offset}"))?;
                Some(addr)
            }
            OpcodeMode::ZeroPageY => {
                new_offset = new_offset.wrapping_add(1);
                let addr = self.lookup_address(new_offset)
                    .map(|v| v.wrapping_add(self.register_y) as u16)
                    .ok_or_else(|| format!("Couldn't read zpy mode address {new_offset}"))?;
                Some(addr)
            }
            OpcodeMode::Absolute => {
                new_offset = new_offset.wrapping_add(1);
                let low = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read low side absolute address {new_offset}")
                })?;
                new_offset = new_offset.wrapping_add(1);
                let high = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read high side absolute address {new_offset}")
                })?;
                Some(u16::from_le_bytes([low, high]))
            }
            OpcodeMode::AbsoluteX => {
                new_offset = new_offset.wrapping_add(1);
                let low = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read low side absolute address {new_offset}")
                })?;
                new_offset = new_offset.wrapping_add(1);
                let high = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read high side absolute address {new_offset}")
                })?;
                Some(u16::from_le_bytes([low, high]).wrapping_add(self.register_x as u16))
            }
            OpcodeMode::AbsoluteY => {
                new_offset = new_offset.wrapping_add(1);
                let low = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read low side absolute address {new_offset}")
                })?;
                new_offset = new_offset.wrapping_add(1);
                let high = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read high side absolute address {new_offset}")
                })?;
                Some(u16::from_le_bytes([low, high]).wrapping_add(self.register_y as u16))
            }
            OpcodeMode::Indirect => {
                new_offset = new_offset.wrapping_add(1);
                let low = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read low side indirect address {new_offset}")
                })?;
                new_offset = new_offset.wrapping_add(1);
                let high = self.lookup_address(new_offset).ok_or_else(|| {
                    format!("Couldn't read high side indirect address {new_offset}")
                })?;
                let low_addr = u16::from_le_bytes([low, high]);
                let high_addr = u16::from_le_bytes([low.wrapping_add(1), high]);
                let result_low = self.lookup_address(low_addr).ok_or_else(|| {
                    format!("Couldn't read low side indirect dest address {new_offset}")
                })?;
                let result_high = self.lookup_address(high_addr).ok_or_else(|| {
                    format!("Couldn't read low side indirect dest address {new_offset}")
                })?;
                // println!("Indirect: {low:02x} {high:02x} {low_addr:04x} {high_addr:04x} {result_low:02x} {result_high:02x}");
                Some(u16::from_le_bytes([result_low, result_high]))
            }
            OpcodeMode::IndirectX => todo!("IndirectX lookup mode"),
            OpcodeMode::IndirectY => todo!("IndirectY lookup mode"),
            OpcodeMode::Implied => None,
            OpcodeMode::Relative => todo!("Relative lookup mode"),
        };
        Ok(ReadAddress { result, new_offset })
    }

    fn read_opcode_and_args(&self, starting_offset: u16) -> Result<(Opcode, Option<u16>), String> {
        let mut offset = starting_offset;
        let instruction = self
            .lookup_address(offset)
            .ok_or_else(|| format!("Couldn't read instruction at offset {offset}"))?;
        offset += 1;
        let opcode = self.opcodes.get(&instruction).expect("Unknown opcode");
        let addr_result = self.read_address_with_mode(offset, opcode.mode)?;
        Ok((*opcode, addr_result.result))
    }

    pub fn run_until_brk(&mut self) {
        while !self.status.r#break {
            self.single_step();
        }
    }
}

impl Device {
    fn read(&self, offset: u16) -> u8 {
        match &self.memory {
            MemoryBlock::Ram(mem) | MemoryBlock::Rom(mem) => *mem
                .get(offset as usize)
                .expect("expected memory to be longer"),
            // _ => unimplemented!("device reading not implemented"),
        }
    }

    fn write(&mut self, offset: u16, value: u8) -> bool {
        // println!("Writing {value:02x} to offset {offset:04x}");
        if let MemoryBlock::Ram(mem) = &mut self.memory {
            let byte = mem
                .get_mut(offset as usize)
                .expect("expected memory to be longer");
            *byte = value;
            // _ => unimplemented!("device writing not implemented"),
            true
        } else {
            // println!(
            //     "Tried to write to {} ${offset:04x}: ${value:02x}",
            //     self.name
            // );
            false
        }
    }
}

#[cfg(test)]
mod test_util_impls {
    use super::*;

    #[test]
    fn status_flags_to_u8() {
        let mut flags = StatusFlags::default();
        let mut as_u8 = u8::from(&flags);
        assert_eq!(as_u8, 0b0010_0000);

        flags.r#break = true;
        as_u8 = u8::from(&flags);

        assert_eq!(as_u8, 0b0011_0000);

        flags.carry = true;
        as_u8 = u8::from(&flags);

        assert_eq!(as_u8, 0b0011_0001);

        flags.zero = true;
        as_u8 = u8::from(&flags);

        assert_eq!(as_u8, 0b0011_0011);
    }
}

#[cfg(test)]
mod simple_test_programs {
    use super::*;

    fn test_rom_only(program: Vec<u8>, zp_and_stack: Option<Vec<u8>>) -> Computer {
        let zp_stack_mem = zp_and_stack.unwrap_or_else(|| vec![0; 200]);

        let memory = MemoryTree::from_iter([
            (
                0x0000,
                Device {
                    name: "ZP/Stack".to_string(),
                    start: 0x0000,
                    end: 0x01ff,
                    memory: MemoryBlock::Rom(zp_stack_mem),
                },
            ),
            (
                0x0200,
                Device {
                    name: "ROM".to_string(),
                    start: 0x0200,
                    end: 0xffff,
                    memory: MemoryBlock::Rom(program),
                },
            ),
        ]);

        test_from_memory(memory)
    }

    fn test_ram_only(program: Vec<u8>, zp_and_stack: Option<Vec<u8>>) -> Computer {
        let zp_stack_mem = zp_and_stack.unwrap_or_else(|| vec![0; 200]);

        let memory = MemoryTree::from_iter([
            (
                0x0000,
                Device {
                    name: "ZP/Stack".to_string(),
                    start: 0x0000,
                    end: 0x01ff,
                    memory: MemoryBlock::Ram(zp_stack_mem),
                },
            ),
            (
                0x0200,
                Device {
                    name: "RAM".to_string(),
                    start: 0x0200,
                    end: 0xffff,
                    memory: MemoryBlock::Ram(program),
                },
            ),
        ]);

        test_from_memory(memory)
    }

    fn test_from_memory(memory: MemoryTree) -> Computer {
        Computer {
            accumulator: 0,
            register_x: 0,
            register_y: 0,
            program_counter: 0x01ff,
            stack_pointer: 0,
            status: Default::default(),

            mode: ComputerMode::Running,
            memory,
            opcodes: build_opcode_map(),
        }
    }

    fn load_immediate<R>(opcode: u8, get_register: R)
    where
        R: Fn(&Computer) -> u8,
    {
        // Negative
        {
            let expected = 0xc0;
            let program = vec![opcode, expected, 0x00];
            let mut computer = test_rom_only(program, None);
            computer.run_until_brk();
            assert_eq!(get_register(&computer), expected);
            assert!(computer.status.negative);
            assert!(!computer.status.zero);
        }
        // Zero
        {
            let expected = 0x00;
            let program = vec![opcode, expected, 0x00];
            let mut computer = test_rom_only(program, None);
            computer.run_until_brk();
            assert_eq!(get_register(&computer), expected);
            assert!(!computer.status.negative);
            assert!(computer.status.zero);
        }
        // Positive
        {
            let expected = 0x40;
            let program = vec![opcode, expected, 0x00];
            let mut computer = test_rom_only(program, None);
            computer.run_until_brk();
            assert_eq!(get_register(&computer), expected);
            assert!(!computer.status.negative);
            assert!(!computer.status.zero);
        }
    }

    fn load_zp<R>(opcode: u8, get_register: R)
    where
        R: Fn(&Computer) -> u8,
    {
        let negative_zp_expected = 0xc0;
        let zero_zp_expected = 0x00;
        let positive_zp_expected = 0x40;
        let zp_and_stack = Some(vec![
            negative_zp_expected,
            zero_zp_expected,
            positive_zp_expected,
        ]);
        // Negative
        {
            let program = vec![opcode, 0x00, 0x00];
            let mut computer = test_rom_only(program, zp_and_stack.clone());
            computer.run_until_brk();
            assert_eq!(get_register(&computer), negative_zp_expected);
            assert!(computer.status.negative);
            assert!(!computer.status.zero);
        }
        // Zero
        {
            let program = vec![opcode, 0x01, 0x00];
            let mut computer = test_rom_only(program, zp_and_stack.clone());
            computer.run_until_brk();
            assert_eq!(get_register(&computer), zero_zp_expected);
            assert!(!computer.status.negative);
            assert!(computer.status.zero);
        }
        // Positive
        {
            let program = vec![opcode, 0x02, 0x00];
            let mut computer = test_rom_only(program, zp_and_stack);
            computer.run_until_brk();
            assert_eq!(get_register(&computer), positive_zp_expected);
            assert!(!computer.status.negative);
            assert!(!computer.status.zero);
        }
    }

    fn load_zp_offset<R>(preload_opcode: u8, opcode: u8, get_register: R)
    where
        R: Fn(&Computer) -> u8,
    {
        let offset = 3;
        let negative_zp_expected = 0xc0;
        let zero_zp_expected = 0x00;
        let positive_zp_expected = 0x40;
        let zp_and_stack = Some(vec![
            0,
            0,
            0,
            negative_zp_expected,
            zero_zp_expected,
            positive_zp_expected,
        ]);
        // Negative
        {
            let program = vec![preload_opcode, offset, opcode, 0x00, 0x00];
            let mut computer = test_rom_only(program, zp_and_stack.clone());
            computer.run_until_brk();
            assert_eq!(get_register(&computer), negative_zp_expected);
            assert!(computer.status.negative);
            assert!(!computer.status.zero);
        }
        // Zero
        {
            let program = vec![preload_opcode, offset, opcode, 0x01, 0x00];
            let mut computer = test_rom_only(program, zp_and_stack.clone());
            computer.run_until_brk();
            assert_eq!(get_register(&computer), zero_zp_expected);
            assert!(!computer.status.negative);
            assert!(computer.status.zero);
        }
        // Positive
        {
            let program = vec![preload_opcode, offset, opcode, 0x02, 0x00];
            let mut computer = test_rom_only(program, zp_and_stack);
            computer.run_until_brk();
            assert_eq!(get_register(&computer), positive_zp_expected);
            assert!(!computer.status.negative);
            assert!(!computer.status.zero);
        }
    }

    fn load_absolute<R>(opcode: u8, get_register: R)
    where
        R: Fn(&Computer) -> u8,
    {
        let negative_zp_expected = 0xc0;
        let zero_zp_expected = 0x00;
        let positive_zp_expected = 0x40;
        const OFFSET: usize = 0x0110;
        let mut zp_and_stack: Vec<u8> = vec![0; OFFSET];
        zp_and_stack.extend_from_slice(&[
            negative_zp_expected,
            zero_zp_expected,
            positive_zp_expected,
        ]);
        let [offset_low, offset_high] = (OFFSET as u16).to_le_bytes();

        // Negative
        {
            let program = vec![opcode, offset_low, offset_high, 0x00];
            let mut computer = test_rom_only(program, Some(zp_and_stack.clone()));
            computer.run_until_brk();
            assert_eq!(get_register(&computer), negative_zp_expected);
            assert!(computer.status.negative);
            assert!(!computer.status.zero);
        }
        // Zero
        {
            let program = vec![opcode, offset_low + 1, offset_high, 0x00];
            let mut computer = test_rom_only(program, Some(zp_and_stack.clone()));
            computer.run_until_brk();
            assert_eq!(get_register(&computer), zero_zp_expected);
            assert!(!computer.status.negative);
            assert!(computer.status.zero);
        }
        // Positive
        {
            let program = vec![opcode, offset_low + 2, offset_high, 0x00];
            let mut computer = test_rom_only(program, Some(zp_and_stack));
            computer.run_until_brk();
            assert_eq!(get_register(&computer), positive_zp_expected);
            assert!(!computer.status.negative);
            assert!(!computer.status.zero);
        }
    }

    fn load_absolute_offset<R>(preload_opcode: u8, opcode: u8, get_register: R)
    where
        R: Fn(&Computer) -> u8,
    {
        let negative_zp_expected = 0xc0;
        let zero_zp_expected = 0x00;
        let positive_zp_expected = 0x40;
        const OFFSET: usize = 0x0110;
        const REGISTER_OFFSET: u8 = 0x06;
        let mut zp_and_stack: Vec<u8> = vec![0; OFFSET + REGISTER_OFFSET as usize];
        zp_and_stack.extend_from_slice(&[
            negative_zp_expected,
            zero_zp_expected,
            positive_zp_expected,
        ]);
        let [offset_low, offset_high] = (OFFSET as u16).to_le_bytes();

        // Negative
        {
            let program = vec![
                preload_opcode,
                REGISTER_OFFSET,
                opcode,
                offset_low,
                offset_high,
                0x00,
            ];
            let mut computer = test_rom_only(program, Some(zp_and_stack.clone()));
            computer.run_until_brk();
            assert_eq!(get_register(&computer), negative_zp_expected);
            assert!(computer.status.negative);
            assert!(!computer.status.zero);
        }
        // Zero
        {
            let program = vec![
                preload_opcode,
                REGISTER_OFFSET,
                opcode,
                offset_low + 1,
                offset_high,
                0x00,
            ];
            let mut computer = test_rom_only(program, Some(zp_and_stack.clone()));
            computer.run_until_brk();
            assert_eq!(get_register(&computer), zero_zp_expected);
            assert!(!computer.status.negative);
            assert!(computer.status.zero);
        }
        // Positive
        {
            let program = vec![
                preload_opcode,
                REGISTER_OFFSET,
                opcode,
                offset_low + 2,
                offset_high,
                0x00,
            ];
            let mut computer = test_rom_only(program, Some(zp_and_stack));
            computer.run_until_brk();
            assert_eq!(get_register(&computer), positive_zp_expected);
            assert!(!computer.status.negative);
            assert!(!computer.status.zero);
        }
    }

    #[test]
    fn lda() {
        let get_register = |c: &Computer| c.accumulator;

        // LDA #$42
        load_immediate(0xa9, get_register);
        // LDA $42
        load_zp(0xa5, get_register);
        // LDX #$03
        // LDA $42,X
        load_zp_offset(0xa2, 0xb5, get_register);
        // LDA $0110
        load_absolute(0xad, get_register);
        // LDX #$06
        // LDA $0110,X
        load_absolute_offset(0xa2, 0xbd, get_register);
        // LDY #$06
        // LDA $0110,Y
        load_absolute_offset(0xa0, 0xb9, get_register);
    }

    #[test]
    fn ldx() {
        let get_register = |c: &Computer| c.register_x;

        // LDX #$42
        load_immediate(0xa2, get_register);
        // LDX $42
        load_zp(0xa6, get_register);
        // LDY #$03
        // LDX $42,Y
        load_zp_offset(0xa0, 0xb6, get_register);
        // LDX $0110
        load_absolute(0xae, get_register);
        // LDY #$06
        // LDX $0110,Y
        load_absolute_offset(0xa0, 0xbe, get_register);
    }

    #[test]
    fn ldy() {
        let get_register = |c: &Computer| c.register_y;

        // LDY #$42
        load_immediate(0xa0, get_register);
        // LDY $42
        load_zp(0xa4, get_register);
        // LDX #$03
        // LDY $42,X
        load_zp_offset(0xa2, 0xb4, get_register);
        // LDY $0110
        load_absolute(0xac, get_register);
        // LDX #$06
        // LDY $0110,X
        load_absolute_offset(0xa2, 0xbc, get_register);
    }

    #[test]
    fn store_and_jump() {
        // LDA #$37
        // STA $30
        // LDA #$13
        // STA $31
        // JMP ($0030)
        // PC = 1337
        let mut program = vec![
            0xa9, 0x37, 0x85, 0x30, 0xa9, 0x13, 0x85, 0x31, 0x6c, 0x30, 0x00,
        ];
        program.resize(0x2000, 0);
        let mut computer = test_ram_only(program, None);
        computer.run_until_brk();
        // brk is size 2 , so $1339 is where we land
        assert_eq!(computer.program_counter, 0x1339);
    }
}
