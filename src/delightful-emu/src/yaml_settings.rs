use std::{
    fs::File,
    io::Read,
    path::PathBuf,
};

use serde::Deserialize;

use crate::computer::{Computer, MemoryTree, Device, MemoryBlock};

#[derive(Debug, Deserialize)]
pub struct Settings {
    initial_layout: Vec<Layout>,
}

#[derive(Debug, Deserialize)]
struct Layout {
    name: String,
    start: u16,
    end: u16,
    read: bool,
    write: bool,
    fill: Fill,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
enum Fill {
    Constant(u8),
    File(PathBuf),
}

impl Layout {
    fn to_memory_vec(&self) -> Vec<u8> {
        let size = (self.end - self.start) as usize;
        let mut memory = Vec::with_capacity(size);
        match &self.fill {
            Fill::Constant(val) => memory.resize(size, *val),
            Fill::File(path) => {
                let mut file = File::open(path).expect("Couldn't open memory file");
                file.read_to_end(&mut memory)
                    .expect("Failed to read memory file into buffer");
            }
        };
        memory
    }
}

impl From<&Settings> for Computer {
    fn from(settings: &Settings) -> Self {
        let mut memory = MemoryTree::new();
        for layout in &settings.initial_layout {
            memory.insert(
                layout.start,
                Device {
                    name: layout.name.clone(),
                    start: layout.start,
                    end: layout.end,
                    memory: match (layout.read, layout.write) {
                        (true, false) => MemoryBlock::Rom(layout.to_memory_vec()),
                        (true, true) => MemoryBlock::Ram(layout.to_memory_vec()),
                        _ => unimplemented!("unknown device/memory object"),
                    },
                },
            );
        }
        Computer::from_memory(memory)
    }
}