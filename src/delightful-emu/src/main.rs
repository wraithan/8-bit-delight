use std::fs;

use delightful_emu::{Computer, Settings};

fn main() {
    let settings_text =
        fs::read_to_string("fixtures/settings.yaml").expect("Couldn't read settings file");
    let settings: Settings =
        serde_yaml::from_str(&settings_text).expect("Couldn't parse settings file");
    println!("Settings: {:#?}!", settings);
    let mut computer: Computer = (&settings).into();
    // For functional tests
    // computer.mode = ComputerMode::Running;
    // computer.program_counter = 0x0400;
    // computer.print_addr(0x0010);
    // computer.print_addr(0x8010);
    computer.print_addr(0xfffc);
    computer.print_addr(0xfffd);
    computer.run_until_brk();
}
