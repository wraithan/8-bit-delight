use std::{fs::read_to_string, str::FromStr};

use kdl::{KdlDocument, KdlError};

struct Computer {
    name: String,
    memory_tree: Vec<MemoryBlock>,
    device_tree: Vec<Device>,
}

struct MemoryBlock {
    name: String,
    start: u16,
    size: u16,
    write: bool,
    memory_tree: Option<Vec<MemoryBlock>>,
    fill: Option<Vec<Filling>>,
}

enum Filling {
    Constant(u8),
    File(String),
}

struct Device {
    name: String,
    device_tree: Option<Vec<Device>>,
    details: DeviceDetails,
}

enum DeviceDetails {
    MemoryMap {
        block: String,
    },
    CharacterLcd {
        size: LcdSize,
        backlight: Option<LcdBacklight>,
    },
}

enum LcdSize {
    W16H2, // Standard smaller 16x2
    W20H4, // Standard larger 20x4
}

enum LcdBacklight {
    Mono,
    Rgb,
}

impl FromStr for Computer {
    type Err = KdlError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let document: KdlDocument = input.parse()?;
        let computer = document.get("computer").expect("no computer?");
        let computer_name = computer
            .get(0)
            .expect("No computer name?")
            .value()
            .as_string()
            .expect("name isn't a string?");
        println!("computer_name: {:?}", computer_name);
        let memory_tree = computer.children().expect("no memory-tree?");
        
        println!("memory_tree: {}", memory_tree);
        unimplemented!()
    }
}

pub fn parse_kdl_version() {
    let filename = "fixtures/settings.kdl";
    let settings_data = read_to_string(filename).expect("couldn't read?");
    settings_data.parse::<Computer>().expect("couldn't parse?");
    println!("Success!");
}
