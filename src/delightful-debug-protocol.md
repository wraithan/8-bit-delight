# Delightful Debug Protocol

This is the protocol used over USB Serial between the [Delightful CLI](./delightful-cli/index.md) and the [Delightful Debugger](./delightful-dbg/index.md). Typically home grown USB Serial wire formats are newline delimited text streams. But, since I didn't want to have to [Base64](https://en.wikipedia.org/wiki/Base64) encode binary payloads, especially from the debugger to the client, I went with a custom binary protocol using [CBOR](https://en.wikipedia.org/wiki/CBOR) as the payload data format.

Currently a packet is laid out as such:

* First 2 bytes are a u16 that describes the length of the payload of the message
* Payload of the message serialized as CBOR

Assuming we know what byte to start on, this lets us read the first two bytes, get the length to read from the buffer, then keep reading until that many bytes come out. When messages come in too fast (and so more than one in a chunk) or are broken up across multiple chunks, being able to tell how much of a stream to read lets us handle it.

Unfortunately this assumes:
* data will always be aligned exactly correctly, with no spurious data on the line.
    * This is already disproven because for some reason, sometimes after boot the first byte the teensy gets is a 0, then it starts reading the data I sent, causing it to misread the length of the first message and not be able to find the proper alignment.
* the contents of the message never has transmission errors
    * Need to understand how much assurance about the data I get from USB Serial
    * If there is a protocol error, even if due to developer bug, restart of the serial connection is required.

If there are transmission errors, retransmit is one way to handle it. Also trying to find the start to another message because all messages that exist currently start their CBOR with `0xa1` (no body) or `0xa2` (has body), and the first key of that body should be `"kind"`, but that assumes a few more things (like never nesting messages or using `"kind"` in other ways in the data model). In an acknowledged world, where the a side wants to be sure one operation has finished before trying to send the next command, messages need to be able to be distinguishable. One way to do that is to keep a message counter that should increase for every message sent. When an acknowledgement is required, just that sequence number is needed. Also then when a number is skipped, whether a transmission error was detected or not, those missing messages can be requested.

For retransmit to work, this means holding onto the sent messages for some period of time, and resending them as requested. If messages were out of order somehow, we might get a request for a message that is already in a buffer to be sent and we'll send it again. If clients hold onto the last message number they processed they can ignore ones from before that as already processed, within some range in order to handle rollover.

## CBOR Array base

A potential future for the protocol is not putting that length outside of the message, but instead requiring messages to start with `0x84` which `0x80` is "array" and the `0x04` of it means 4 elements. First element would be an unsigned number that is the length of the message. Realistically that'll be `0x18` aka `u8` or `0x19` aka `u16`, maybe in the most extreme case `0x1a` aka `u32`, but I don't plan to sweep more than 50% of the 16bit address space in a given send, given the Teensy only has 8192 bytes of RAM I'd have to be streaming the response. It isn't hard to manually parse this much of a CBOR message to decide how much to buffer before trying to decode the rest of the message. Also for generation I have to include a placeholder, I could just make that placeholder `0xffff` which means it'll always be `[0x84 0x19 ...]` to start the message.

Next element of the array would be the message counter, limiting that to a `u16` means it'd start with `0x18` aka `u8` and `0x19` aka `u16` then reroll over when it hit the cap. Again this could be manually parsed, then we could more eagerly attempt to request a retransmit, but I'm not sure that's really that useful.

The last element of the array would be the CBOR Pair aka Order Map object that holds `"kind"` and `"body"` mentioned above.

Another feature I need is whether this message needs acknowledgement, I'll reserve a boolean for this.

```json
[
    0x001c, // length
    0x1283, // message number
    true,   // needs acknowledgement
    {
        "kind": "Monitor",
        "body": false // optional/any type
    }
]
```

Turns into the following CBOR

```
84                      # array(4)
   19 001c              # unsigned(28)
   19 1283              # unsigned(4739)
   F5                   # primitive(21) aka true
   A2                   # map(2)
      64                # text(4)
         6B696E64       # "kind"
      67                # text(7)
         4D6F6E69746F72 # "Monitor"
      64                # text(4)
         626F6479       # "body"
      F4                # primitive(20) aka false
```
