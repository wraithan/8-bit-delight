#include <Arduino.h>

// https://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

void readBusByPin();

const uint8_t debugButtonPin = PIN_B7;
const uint8_t stepButtonPin = PIN_D0;

const uint8_t testPassLedPin = PIN_E6;
const uint8_t testFailLedPin = PIN_E6;

// Uses port F for bus interaction
// Least significant bit first so DDRF/PORTF/PINF line up as expected
// Port F has the benefit of also including an ADC if I want to test levels of the lines.
const uint8_t busPins[8] = { PIN_F0, PIN_F1, PIN_F2, PIN_F3, PIN_F4, PIN_F5, PIN_F6, PIN_F7 };

// Control Signals
const uint8_t clockPin = PIN_B4;
const uint8_t aluRegAOut = PIN_C4;
const uint8_t aluRegAIn = PIN_C5;
const uint8_t aluRegBOut = PIN_C6;
const uint8_t aluRegBIn = PIN_C7;
const uint8_t aluOut = PIN_C3;
const uint8_t clearRegistersPin = PIN_C2;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  // Wait for serial connect or 10s.
  while (!Serial && millis() < 1000) { }

  pinMode(testPassLedPin, OUTPUT);
  pinMode(testFailLedPin, OUTPUT);
  pinMode(debugButtonPin, INPUT_PULLUP);
  pinMode(stepButtonPin, INPUT_PULLUP);

  pinMode(clockPin, INPUT);

  // Set to inputs so they're high impedance and the buttons still work.
  pinMode(aluRegAOut, INPUT);
  pinMode(aluRegAIn, INPUT);
  pinMode(aluRegBOut, INPUT);
  pinMode(aluRegBIn, INPUT);
  pinMode(aluOut, INPUT);
  pinMode(clearRegistersPin, INPUT);

  // Setup bus interaction pins
  DDRF = 0;
  PORTF = 0;
  digitalWrite(LED_BUILTIN, LOW);
}

enum ClockState {
  Unknown,
  Rising,
  High,
  Falling,
  Low,
};

enum TestStep {
  Init,
  Storage,
  Pass,
  Fail,
  End,
};

ClockState clockState = ClockState::Unknown;
uint8_t busData = 0;
elapsedMillis testDuration = 0;
TestStep step = TestStep::Init;

bool testRegister(uint8_t regIn, uint8_t regOut, uint8_t num);

void loop() {
  static bool testRunning = false;
  // static bool clockPulsedSinceWrite = false;
  bool rawClock = digitalRead(clockPin);
  digitalWrite(LED_BUILTIN, rawClock);

  if (rawClock) {
    if (clockState == ClockState::Unknown || clockState == ClockState::Falling || clockState == ClockState::Low) {
      clockState = ClockState::Rising;
    } else if (clockState == ClockState::Rising) {
      clockState = ClockState::High;
    }
  } else {
    if (clockState == ClockState::Unknown || clockState == ClockState::Rising || clockState == ClockState::High) {
      clockState = ClockState::Falling;
    } else if (clockState == ClockState::Falling) {
      clockState = ClockState::Low;
    }
  }

  if (clockState == ClockState::Rising) {
    busData = PINF;
    // clockPulsedSinceWrite = true;
    Serial.printf("Bus: %3d [" BYTE_TO_BINARY_PATTERN "]\n", busData, BYTE_TO_BINARY(busData));
  }

  if (!testRunning && testDuration >= 1000 && digitalRead(debugButtonPin) == LOW ) {
    testRunning = true;
    testDuration = 0;
    step = TestStep::Init;
  }

  if (testRunning) {
    switch (step) {
      case TestStep::Init:
        pinMode(clearRegistersPin, OUTPUT);
        digitalWrite(clearRegistersPin, HIGH);
        delay(1);
        pinMode(clearRegistersPin, INPUT);
        step = TestStep::Storage;
        break;
      case TestStep::Storage: {

        pinMode(aluRegAIn, OUTPUT);
        pinMode(aluRegAOut, OUTPUT);
        pinMode(aluRegBIn, OUTPUT);
        pinMode(aluRegBOut, OUTPUT);
        pinMode(clockPin, OUTPUT);

        digitalWrite(aluRegAIn, HIGH);
        digitalWrite(aluRegAOut, HIGH);
        digitalWrite(aluRegBIn, HIGH);
        digitalWrite(aluRegBOut, HIGH);
        digitalWrite(clockPin, LOW);

        uint8_t i = 0;
        uint8_t ii = 0;
        do {
          ii = 255 - i;
          if(!testRegister(aluRegAIn, aluRegAOut, ii)) {
            Serial.printf("Test::Storage: %d A Failed\n", ii);
            Serial.printf("  expected: " BYTE_TO_BINARY_PATTERN " got " BYTE_TO_BINARY_PATTERN "\n", BYTE_TO_BINARY(ii), BYTE_TO_BINARY(busData));
            break;
          }
          if(!testRegister(aluRegBIn, aluRegBOut, i)) {
            Serial.printf("Test::Storage: %d B Failed\n", i);
            Serial.printf("  expected: " BYTE_TO_BINARY_PATTERN " got " BYTE_TO_BINARY_PATTERN "\n", BYTE_TO_BINARY(i), BYTE_TO_BINARY(busData));
            break;
          }
          // Serial.printf("Test::Storage: %d A, %d B\n", i, ii);
        } while (i++ != 255);
        if (step == TestStep::Storage) {
          Serial.printf("[%07lu] Test::Storage: A & B Passed\n", millis());
          // step = TestStep::Pass;
        }

        pinMode(aluRegAIn, INPUT);
        pinMode(aluRegAOut, INPUT);
        pinMode(aluRegBIn, INPUT);
        pinMode(aluRegBOut, INPUT);
        pinMode(clockPin, INPUT);
        break;
      }
      case TestStep::Fail:
        digitalWrite(testFailLedPin, HIGH);
        step = TestStep::End;
        break;
      case TestStep::Pass:
        digitalWrite(testPassLedPin, HIGH);
        pinMode(clearRegistersPin, OUTPUT);
        digitalWrite(clearRegistersPin, HIGH);
        delay(1);
        pinMode(clearRegistersPin, INPUT);
        // step = TestStep::Storage;
      case TestStep::End:
      default:
        testRunning = false;
    }
  }

}

const uint32_t clockDelayMs = 30;

bool testRegister(uint8_t regIn, uint8_t regOut, uint8_t num) {
  // pinMode(regIn, OUTPUT);
  // pinMode(regOut, OUTPUT);
  // pinMode(clockPin, OUTPUT);
  digitalWrite(regIn, HIGH);
  digitalWrite(regOut, HIGH);
  digitalWrite(clockPin, LOW);

  DDRF = 255;
  PORTF = 0;
  // Serial.println("Cleared Bus");
  // while(digitalRead(stepButtonPin) != LOW) {};
  // delay(500);
  // while(digitalRead(stepButtonPin) != HIGH) {};
  // delay(250);

  // DDRF = 255;
  PORTF = num;
  // Serial.printf("Set bus to num %d\n", num);
  // while(digitalRead(stepButtonPin) != LOW) {};
  // delay(500);
  // while(digitalRead(stepButtonPin) != HIGH) {};
  // delay(250);

  digitalWrite(regIn, LOW);
  delay(clockDelayMs);
  digitalWrite(clockPin, HIGH);
  // Serial.println("Register read from bus");
  // while(digitalRead(stepButtonPin) != LOW) {};
  // delay(500);
  // while(digitalRead(stepButtonPin) != HIGH) {};
  // delay(250);

  delay(clockDelayMs);
  digitalWrite(clockPin, LOW);
  digitalWrite(regIn, HIGH);
  // Serial.println("Register stopped reading from bus");
  delay(clockDelayMs);
  // while(digitalRead(stepButtonPin) != LOW) {};
  // delay(500);
  // while(digitalRead(stepButtonPin) != HIGH) {};
  // delay(250);

  // Serial.println("Cleared bus again");
  DDRF = 0;
  PORTF = 0;
  // while(digitalRead(stepButtonPin) != LOW) {};
  // delay(500);
  // while(digitalRead(stepButtonPin) != HIGH) {};
  // delay(250);

  digitalWrite(regOut, LOW);
  delay(clockDelayMs);
  digitalWrite(clockPin, HIGH);
  // Serial.println("Register output to bus");
  // while(digitalRead(stepButtonPin) != LOW) {};
  // delay(500);
  // while(digitalRead(stepButtonPin) != HIGH) {};
  // delay(250);

  delay(clockDelayMs);
  busData = PINF;
  digitalWrite(clockPin, LOW);
  digitalWrite(regIn, HIGH);
  digitalWrite(regOut, HIGH);
  // pinMode(regIn, INPUT);
  // pinMode(regOut, INPUT);
  // Serial.println("Bus read by teensy");
  // pinMode(clockPin, INPUT);
  if (busData != num) {
    step = TestStep::Fail;
    DDRF = 255;
    PORTF = num;
    return false;
  }
  return true;
}

void readBusByPin() {
  Serial.printf("Bus: [ %d, %d, %d, %d, %d, %d, %d, %d ]\n",
    digitalRead(busPins[0]),
    digitalRead(busPins[1]),
    digitalRead(busPins[2]),
    digitalRead(busPins[3]),
    digitalRead(busPins[4]),
    digitalRead(busPins[5]),
    digitalRead(busPins[6]),
    digitalRead(busPins[7])
  );
}