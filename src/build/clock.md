# Clock

## ICs

From left to right:
* [LM555](../datasheets/lm555.pdf) timer as an oscillator
* [LM555](../datasheets/lm555.pdf) timer as a monostable debouncer
* [LM555](../datasheets/lm555.pdf) timer as a bistable debouncer
* [SN74LS04N](../datasheets/sn74ls04n.pdf) hex inverter used both for signal inversion and signal buffering
* [SN74LS08N](../datasheets/sn74ls08n.pdf) quad dual input AND used for clock source selection and halt
* [SN74LS32N](../datasheets/sn74ls32n.pdf) quad dual input OR used for clock source selection

## Notes

This clock is both neat and could be stripped down a bit. I'll definitely keep the 555 used as an oscillator because I really enjoy that style, but I might switch to a simpler RC low pass filter for the debouncing. Similarly it isn't hard to rework the logic ICs down to just two of them, Ben Eater did this in the video with NAND gates.

I've altered the build from the base schematic by taking the inverted clock signal and passing it through another inverter to functionally buffer the clock signal. I did this based on the u/lordmonoxide [What I Have Learned](https://www.reddit.com/r/beneater/comments/dskbug/what_i_have_learned_a_master_list_of_what_to_do/) post, but reading that section on the clock section, they mention swapping the hex inverter with a schmidt trigger based inverter, which I don't believe I have any spares of in LS right now, just CMOS CD40106 which wont play well with the TTL of the LS chips. I'm considering swapping to CMOS logic eventually, but will likely be going to the 74HC series logic, not the 4000 series logic.

The clock module is a prime candidate for turning into a soldered protoboard version, both because then I can reduce noise and keep the clock more stable, as well there not being many features I can think of adding to the clock, just simplifications.