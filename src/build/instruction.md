# Instruction Register

## ICs

From left to right:
* [SN74LS173AN](../datasheets/sn74ls173an.pdf) 4-bit D register is for instruction to be decoded in the MSB
* [SN74LS173AN](../datasheets/sn74ls173an.pdf) 4-bit D register is for the address in the LSB
* [SN74LS245N](../datasheets/sn74ls245n.pdf) 8-bit bus transceiver that can read 8 bits off the bus, or put the 4 LSB on the bus.

## Notes

For the initial build where I'll be sticking with the 16 instructions and bytes of RAM limitation, this likely wont get any real changes until I expand the CPU.