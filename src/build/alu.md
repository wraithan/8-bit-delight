# ALU / Registers

### Register A and B ICs

From left to right:
* [SN74LS245N](../datasheets/sn74ls245n.pdf) 8-bit bus transceiver that can read 8 bits off the bus or put them on.
* [SN74LS173AN](../datasheets/sn74ls173an.pdf) 4-bit D register is for the MSB
* [SN74LS173AN](../datasheets/sn74ls173an.pdf) 4-bit D register is for the LSB

### ALU ICs

* [SN74LS245N](../datasheets/sn74ls245n.pdf) 8-bit bus transceiver that can write 8 bits to the bus, but cannot read.
* [SN74LS283N](../datasheets/sn74ls283n.pdf) 4-bit adder with carry used for MSB
* [SN74LS283N](../datasheets/sn74ls283n.pdf) 4-bit adder with carry used for LSB
* [SN74LS86N](../datasheets/sn74ls86n.pdf) Quad dual input XOR gate used for subtraction for MSB
* [SN74LS86N](../datasheets/sn74ls86n.pdf) Quad dual input XOR gate used for subtraction for LSB

## Notes

Currently this aspect of my build is a mess using a lot of stranded wire jumpers instead of cut to size solid core. Since I've been running the CPU again I've managed to find/fix all the lose wires in this section but it needs to be upgraded to cut to length. This section has the most control signals out of any part of my CPU so far, IO for both registers, output for the ALU register, and subtract mode. Before I go about cleaning up these modules, I want to have some basic tests written for the QA module that will ensure that when I replace all the wires it works at least as good.

Notably on this design of the ALU, the MSB carry bit is ignored. I know some processors have access to detect if an overflow happened during a math operation. Not sure how I'd use it or where I'd route it, but it intrigues me.