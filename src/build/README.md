# The 8-Bit Delight Build

Current build state (excuse the picture was taken quickly):

![Build as of 2021-03-15](../build-2021-03-15.jpg)

Left Side:
1. [Clock](./clock.md)
    1. Potentiometer chooses speed, left faster, right slower
    2. Momentary button steps clock if manual stepping is enabled
    3. Switch button switches between auto and manual stepping
2. Inputs
    1. Yellow Button, instruction register output to bus
    2. Red Button, instruction register input from bus
    3. Red button, clear memory, hooked up to instruction register, and ALU register A and B
    4. Switch bank for setting bus lines. On is high, off is disconnected and relies on bus pull-down resistor
3. [Instruction Register](./instruction.md)
4. [Teensy++ 2.0 Debugger Module](./qa.md)

Right Side:
1. Open
2. [ALU](./alu.md) Register A
    1. Yellow button output to bus
    2. Red button input from bus
3. [ALU](./alu.md) Register Output
    1. Has a long yellow wire for output to bus, low is output, high is disconnected, Blue button is switches to subtract while pressed.
4. [ALU](./alu.md) Register B
    1. Yellow button output to bus
    2. Red button input from bus
