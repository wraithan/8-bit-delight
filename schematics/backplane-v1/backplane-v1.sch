EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2000 900  0    50   ~ 0
BP-B1
Text Label 2000 1000 0    50   ~ 0
BP-B2
Text Label 2000 1100 0    50   ~ 0
BP-B3
Text Label 2000 1200 0    50   ~ 0
BP-B4
Text Label 2000 1300 0    50   ~ 0
BP-B5
Text Label 2000 1400 0    50   ~ 0
BP-B6
Text Label 2000 1500 0    50   ~ 0
BP-B7
Text Label 2000 1600 0    50   ~ 0
BP-B8
Text Label 2000 1700 0    50   ~ 0
BP-B9
Text Label 2000 1800 0    50   ~ 0
BP-B10
Text Label 2000 1900 0    50   ~ 0
BP-B11
Text Label 2000 2000 0    50   ~ 0
BP-B12
Text Label 2000 2100 0    50   ~ 0
BP-B13
Text Label 2000 2200 0    50   ~ 0
BP-B14
Text Label 2000 2300 0    50   ~ 0
BP-B15
Text Label 2000 2400 0    50   ~ 0
BP-B16
Text Label 2000 2500 0    50   ~ 0
BP-B17
Text Label 2000 2600 0    50   ~ 0
BP-B18
Text Label 2000 2700 0    50   ~ 0
BP-B19
Text Label 2000 2800 0    50   ~ 0
BP-B20
Text Label 2000 2900 0    50   ~ 0
BP-B21
Text Label 2000 3000 0    50   ~ 0
BP-B22
Text Label 2000 3100 0    50   ~ 0
BP-B23
Text Label 2000 3200 0    50   ~ 0
BP-B24
Text Label 2000 3300 0    50   ~ 0
BP-B25
Text Label 2000 3400 0    50   ~ 0
BP-B26
Text Label 2000 3500 0    50   ~ 0
BP-B27
Text Label 2000 3600 0    50   ~ 0
BP-B28
Text Label 2000 3700 0    50   ~ 0
BP-B29
Text Label 2000 3800 0    50   ~ 0
BP-B30
Text Label 2000 3900 0    50   ~ 0
BP-B31
Text Label 2000 4000 0    50   ~ 0
BP-B32
Text Label 2000 4100 0    50   ~ 0
BP-B33
Text Label 2000 4200 0    50   ~ 0
BP-B34
Text Label 2000 4300 0    50   ~ 0
BP-B35
Text Label 2000 4400 0    50   ~ 0
BP-B36
Text Label 2000 4500 0    50   ~ 0
BP-B37
Text Label 2000 4600 0    50   ~ 0
BP-B38
Text Label 2000 4700 0    50   ~ 0
BP-B39
Text Label 2000 4800 0    50   ~ 0
BP-B40
Text Label 2000 4900 0    50   ~ 0
BP-B41
Text Label 2000 5000 0    50   ~ 0
BP-B42
Text Label 2000 5100 0    50   ~ 0
BP-B43
Wire Wire Line
	2000 900  2350 900 
Wire Wire Line
	2000 1000 2350 1000
Wire Wire Line
	2000 1100 2350 1100
Wire Wire Line
	2000 1200 2350 1200
Wire Wire Line
	2000 1300 2350 1300
Wire Wire Line
	2000 1400 2350 1400
Wire Wire Line
	2000 1500 2350 1500
Wire Wire Line
	2000 1600 2350 1600
Wire Wire Line
	2000 1700 2350 1700
Wire Wire Line
	2000 1800 2350 1800
Wire Wire Line
	2000 1900 2350 1900
Wire Wire Line
	2000 2000 2350 2000
Wire Wire Line
	2000 2100 2350 2100
Wire Wire Line
	2000 2200 2350 2200
Wire Wire Line
	2000 2300 2350 2300
Wire Wire Line
	2000 2400 2350 2400
Wire Wire Line
	2000 2500 2350 2500
Wire Wire Line
	2000 2600 2350 2600
Wire Wire Line
	2000 2700 2350 2700
Wire Wire Line
	2000 2800 2350 2800
Wire Wire Line
	2000 2900 2350 2900
Wire Wire Line
	2000 3000 2350 3000
Wire Wire Line
	2000 3100 2350 3100
Wire Wire Line
	2000 3200 2350 3200
Wire Wire Line
	2000 3300 2350 3300
Wire Wire Line
	2000 3400 2350 3400
Wire Wire Line
	2000 3500 2350 3500
Wire Wire Line
	2000 3600 2350 3600
Wire Wire Line
	2000 3700 2350 3700
Wire Wire Line
	2000 3800 2350 3800
Wire Wire Line
	2000 3900 2350 3900
Wire Wire Line
	2000 4000 2350 4000
Wire Wire Line
	2000 4100 2350 4100
Wire Wire Line
	2000 4200 2350 4200
Wire Wire Line
	2000 4300 2350 4300
Wire Wire Line
	2000 4400 2350 4400
Wire Wire Line
	2000 4500 2350 4500
Wire Wire Line
	2000 4600 2350 4600
Wire Wire Line
	2000 4700 2350 4700
Wire Wire Line
	2000 4800 2350 4800
Wire Wire Line
	2000 4900 2350 4900
Wire Wire Line
	2000 5000 2350 5000
Wire Wire Line
	2000 5100 2350 5100
Entry Wire Line
	2350 900  2450 1000
Entry Wire Line
	2350 1000 2450 1100
Entry Wire Line
	2350 1100 2450 1200
Entry Wire Line
	2350 1200 2450 1300
Entry Wire Line
	2350 1300 2450 1400
Entry Wire Line
	2350 1400 2450 1500
Entry Wire Line
	2350 1500 2450 1600
Entry Wire Line
	2350 1600 2450 1700
Entry Wire Line
	2350 1700 2450 1800
Entry Wire Line
	2350 1800 2450 1900
Entry Wire Line
	2350 1900 2450 2000
Entry Wire Line
	2350 2000 2450 2100
Entry Wire Line
	2350 2100 2450 2200
Entry Wire Line
	2350 2200 2450 2300
Entry Wire Line
	2350 2300 2450 2400
Entry Wire Line
	2350 2400 2450 2500
Entry Wire Line
	2350 2500 2450 2600
Entry Wire Line
	2350 2600 2450 2700
Entry Wire Line
	2350 2700 2450 2800
Entry Wire Line
	2350 2800 2450 2900
Entry Wire Line
	2350 2900 2450 3000
Entry Wire Line
	2350 3000 2450 3100
Entry Wire Line
	2350 3100 2450 3200
Entry Wire Line
	2350 3200 2450 3300
Entry Wire Line
	2350 3300 2450 3400
Entry Wire Line
	2350 3400 2450 3500
Entry Wire Line
	2350 3500 2450 3600
Entry Wire Line
	2350 3600 2450 3700
Entry Wire Line
	2350 3700 2450 3800
Entry Wire Line
	2350 3800 2450 3900
Entry Wire Line
	2350 3900 2450 4000
Entry Wire Line
	2350 4000 2450 4100
Entry Wire Line
	2350 4100 2450 4200
Entry Wire Line
	2350 4200 2450 4300
Entry Wire Line
	2350 4300 2450 4400
Entry Wire Line
	2350 4400 2450 4500
Entry Wire Line
	2350 4500 2450 4600
Entry Wire Line
	2350 4600 2450 4700
Entry Wire Line
	2350 4700 2450 4800
Entry Wire Line
	2350 4800 2450 4900
Entry Wire Line
	2350 4900 2450 5000
Entry Wire Line
	2350 5000 2450 5100
Entry Wire Line
	2350 5100 2450 5200
Text Label 1500 900  2    50   ~ 0
BP-A1
Text Label 1500 1000 2    50   ~ 0
BP-A2
Text Label 1500 1100 2    50   ~ 0
BP-A3
Text Label 1500 1200 2    50   ~ 0
BP-A4
Text Label 1500 1300 2    50   ~ 0
BP-A5
Text Label 1500 1400 2    50   ~ 0
BP-A6
Text Label 1500 1500 2    50   ~ 0
BP-A7
Text Label 1500 1600 2    50   ~ 0
BP-A8
Text Label 1500 1700 2    50   ~ 0
BP-A9
Text Label 1500 1800 2    50   ~ 0
BP-A10
Text Label 1500 1900 2    50   ~ 0
BP-A11
Text Label 1500 2000 2    50   ~ 0
BP-A12
Text Label 1500 2100 2    50   ~ 0
BP-A13
Text Label 1500 2200 2    50   ~ 0
BP-A14
Text Label 1500 2300 2    50   ~ 0
BP-A15
Text Label 1500 2400 2    50   ~ 0
BP-A16
Text Label 1500 2500 2    50   ~ 0
BP-A17
Text Label 1500 2600 2    50   ~ 0
BP-A18
Text Label 1500 2700 2    50   ~ 0
BP-A19
Text Label 1500 2800 2    50   ~ 0
BP-A20
Text Label 1500 2900 2    50   ~ 0
BP-A21
Text Label 1500 3000 2    50   ~ 0
BP-A22
Text Label 1500 3100 2    50   ~ 0
BP-A23
Text Label 1500 3200 2    50   ~ 0
BP-A24
Text Label 1500 3300 2    50   ~ 0
BP-A25
Text Label 1500 3400 2    50   ~ 0
BP-A26
Text Label 1500 3500 2    50   ~ 0
BP-A27
Text Label 1500 3600 2    50   ~ 0
BP-A28
Text Label 1500 3700 2    50   ~ 0
BP-A29
Text Label 1500 3800 2    50   ~ 0
BP-A30
Text Label 1500 3900 2    50   ~ 0
BP-A31
Text Label 1500 4000 2    50   ~ 0
BP-A32
Text Label 1500 4100 2    50   ~ 0
BP-A33
Text Label 1500 4200 2    50   ~ 0
BP-A34
Text Label 1500 4300 2    50   ~ 0
BP-A35
Text Label 1500 4400 2    50   ~ 0
BP-A36
Text Label 1500 4500 2    50   ~ 0
BP-A37
Text Label 1500 4600 2    50   ~ 0
BP-A38
Text Label 1500 4700 2    50   ~ 0
BP-A39
Text Label 1500 4800 2    50   ~ 0
BP-A40
Text Label 1500 4900 2    50   ~ 0
BP-A41
Text Label 1500 5000 2    50   ~ 0
BP-A42
Text Label 1500 5100 2    50   ~ 0
BP-A43
Wire Wire Line
	1500 900  1150 900 
Wire Wire Line
	1500 1000 1150 1000
Wire Wire Line
	1500 4900 1150 4900
Wire Wire Line
	1500 5000 1150 5000
Wire Wire Line
	1500 5100 1150 5100
Wire Wire Line
	1500 4400 1150 4400
Wire Wire Line
	1500 4300 1150 4300
Wire Wire Line
	1500 4200 1150 4200
Wire Wire Line
	1500 4100 1150 4100
Wire Wire Line
	1500 4000 1150 4000
Wire Wire Line
	1500 3900 1150 3900
Wire Wire Line
	1500 3800 1150 3800
Wire Wire Line
	1500 3700 1150 3700
Wire Wire Line
	1500 3600 1150 3600
Wire Wire Line
	1500 3500 1150 3500
Wire Wire Line
	1500 3400 1150 3400
Wire Wire Line
	1500 3300 1150 3300
Wire Wire Line
	1500 3200 1150 3200
Wire Wire Line
	1500 3100 1150 3100
Wire Wire Line
	1500 3000 1150 3000
Wire Wire Line
	1500 2900 1150 2900
Wire Wire Line
	1500 2800 1150 2800
Wire Wire Line
	1500 2700 1150 2700
Wire Wire Line
	1500 2600 1150 2600
Wire Wire Line
	1500 2500 1150 2500
Wire Wire Line
	1500 2400 1150 2400
Wire Wire Line
	1500 2300 1150 2300
Wire Wire Line
	1500 2200 1150 2200
Wire Wire Line
	1500 2100 1150 2100
Wire Wire Line
	1500 2000 1150 2000
Wire Wire Line
	1500 1900 1150 1900
Wire Wire Line
	1500 1800 1150 1800
Wire Wire Line
	1500 1700 1150 1700
Wire Wire Line
	1500 1600 1150 1600
Wire Wire Line
	1500 1500 1150 1500
Wire Wire Line
	1500 1400 1150 1400
Wire Wire Line
	1500 1300 1150 1300
Wire Wire Line
	1500 1200 1150 1200
Wire Wire Line
	1500 1100 1150 1100
Wire Wire Line
	1500 4800 1150 4800
Wire Wire Line
	1500 4700 1150 4700
Wire Wire Line
	1500 4600 1150 4600
Wire Wire Line
	1500 4500 1150 4500
$Comp
L DelightSymbols:Conn_02x43_Row_Letter_First J1
U 1 1 625DE3BB
P 1750 3000
F 0 "J1" H 1750 5317 50  0000 C CNN
F 1 "Slot 0" H 1750 5226 50  0000 C CNN
F 2 "DelightFootprints:PinHeader_2x43_P2.54mm_Vertical" H 1700 3200 50  0001 C CNN
F 3 "~" H 1700 3200 50  0001 C CNN
	1    1750 3000
	1    0    0    -1  
$EndComp
Entry Wire Line
	2350 900  2450 1000
Entry Wire Line
	1050 5200 1150 5100
Entry Wire Line
	1050 5100 1150 5000
Entry Wire Line
	1050 5000 1150 4900
Entry Wire Line
	1050 4900 1150 4800
Entry Wire Line
	1050 4800 1150 4700
Entry Wire Line
	1050 4700 1150 4600
Entry Wire Line
	1050 4600 1150 4500
Entry Wire Line
	1050 4500 1150 4400
Entry Wire Line
	1050 4400 1150 4300
Entry Wire Line
	1050 4300 1150 4200
Entry Wire Line
	1050 4200 1150 4100
Entry Wire Line
	1050 4100 1150 4000
Entry Wire Line
	1050 4000 1150 3900
Entry Wire Line
	1050 3900 1150 3800
Entry Wire Line
	1050 3800 1150 3700
Entry Wire Line
	1050 3700 1150 3600
Entry Wire Line
	1050 3600 1150 3500
Entry Wire Line
	1050 3500 1150 3400
Entry Wire Line
	1050 3400 1150 3300
Entry Wire Line
	1050 3300 1150 3200
Entry Wire Line
	1050 3200 1150 3100
Entry Wire Line
	1050 3100 1150 3000
Entry Wire Line
	1050 3000 1150 2900
Entry Wire Line
	1050 2900 1150 2800
Entry Wire Line
	1050 2800 1150 2700
Entry Wire Line
	1050 2700 1150 2600
Entry Wire Line
	1050 2600 1150 2500
Entry Wire Line
	1050 2500 1150 2400
Entry Wire Line
	1050 2400 1150 2300
Entry Wire Line
	1050 2300 1150 2200
Entry Wire Line
	1050 2200 1150 2100
Entry Wire Line
	1050 2100 1150 2000
Entry Wire Line
	1050 2000 1150 1900
Entry Wire Line
	1050 1900 1150 1800
Entry Wire Line
	1050 1800 1150 1700
Entry Wire Line
	1050 1700 1150 1600
Entry Wire Line
	1050 1600 1150 1500
Entry Wire Line
	1050 1500 1150 1400
Entry Wire Line
	1050 1400 1150 1300
Entry Wire Line
	1050 1300 1150 1200
Entry Wire Line
	1050 1200 1150 1100
Entry Wire Line
	1050 1100 1150 1000
Entry Wire Line
	1050 1000 1150 900 
Entry Wire Line
	1050 5200 1150 5100
Wire Bus Line
	1050 5400 2450 5400
Text Label 3500 900  0    50   ~ 0
BP-B1
Text Label 3500 1000 0    50   ~ 0
BP-B2
Text Label 3500 1100 0    50   ~ 0
BP-B3
Text Label 3500 1200 0    50   ~ 0
BP-B4
Text Label 3500 1300 0    50   ~ 0
BP-B5
Text Label 3500 1400 0    50   ~ 0
BP-B6
Text Label 3500 1500 0    50   ~ 0
BP-B7
Text Label 3500 1600 0    50   ~ 0
BP-B8
Text Label 3500 1700 0    50   ~ 0
BP-B9
Text Label 3500 1800 0    50   ~ 0
BP-B10
Text Label 3500 1900 0    50   ~ 0
BP-B11
Text Label 3500 2000 0    50   ~ 0
BP-B12
Text Label 3500 2100 0    50   ~ 0
BP-B13
Text Label 3500 2200 0    50   ~ 0
BP-B14
Text Label 3500 2300 0    50   ~ 0
BP-B15
Text Label 3500 2400 0    50   ~ 0
BP-B16
Text Label 3500 2500 0    50   ~ 0
BP-B17
Text Label 3500 2600 0    50   ~ 0
BP-B18
Text Label 3500 2700 0    50   ~ 0
BP-B19
Text Label 3500 2800 0    50   ~ 0
BP-B20
Text Label 3500 2900 0    50   ~ 0
BP-B21
Text Label 3500 3000 0    50   ~ 0
BP-B22
Text Label 3500 3100 0    50   ~ 0
BP-B23
Text Label 3500 3200 0    50   ~ 0
BP-B24
Text Label 3500 3300 0    50   ~ 0
BP-B25
Text Label 3500 3400 0    50   ~ 0
BP-B26
Text Label 3500 3500 0    50   ~ 0
BP-B27
Text Label 3500 3600 0    50   ~ 0
BP-B28
Text Label 3500 3700 0    50   ~ 0
BP-B29
Text Label 3500 3800 0    50   ~ 0
BP-B30
Text Label 3500 3900 0    50   ~ 0
BP-B31
Text Label 3500 4000 0    50   ~ 0
BP-B32
Text Label 3500 4100 0    50   ~ 0
BP-B33
Text Label 3500 4200 0    50   ~ 0
BP-B34
Text Label 3500 4300 0    50   ~ 0
BP-B35
Text Label 3500 4400 0    50   ~ 0
BP-B36
Text Label 3500 4500 0    50   ~ 0
BP-B37
Text Label 3500 4600 0    50   ~ 0
BP-B38
Text Label 3500 4700 0    50   ~ 0
BP-B39
Text Label 3500 4800 0    50   ~ 0
BP-B40
Text Label 3500 4900 0    50   ~ 0
BP-B41
Text Label 3500 5000 0    50   ~ 0
BP-B42
Text Label 3500 5100 0    50   ~ 0
BP-B43
Wire Wire Line
	3500 900  3850 900 
Wire Wire Line
	3500 1000 3850 1000
Wire Wire Line
	3500 1100 3850 1100
Wire Wire Line
	3500 1200 3850 1200
Wire Wire Line
	3500 1300 3850 1300
Wire Wire Line
	3500 1400 3850 1400
Wire Wire Line
	3500 1500 3850 1500
Wire Wire Line
	3500 1600 3850 1600
Wire Wire Line
	3500 1700 3850 1700
Wire Wire Line
	3500 1800 3850 1800
Wire Wire Line
	3500 1900 3850 1900
Wire Wire Line
	3500 2000 3850 2000
Wire Wire Line
	3500 2100 3850 2100
Wire Wire Line
	3500 2200 3850 2200
Wire Wire Line
	3500 2300 3850 2300
Wire Wire Line
	3500 2400 3850 2400
Wire Wire Line
	3500 2500 3850 2500
Wire Wire Line
	3500 2600 3850 2600
Wire Wire Line
	3500 2700 3850 2700
Wire Wire Line
	3500 2800 3850 2800
Wire Wire Line
	3500 2900 3850 2900
Wire Wire Line
	3500 3000 3850 3000
Wire Wire Line
	3500 3100 3850 3100
Wire Wire Line
	3500 3200 3850 3200
Wire Wire Line
	3500 3300 3850 3300
Wire Wire Line
	3500 3400 3850 3400
Wire Wire Line
	3500 3500 3850 3500
Wire Wire Line
	3500 3600 3850 3600
Wire Wire Line
	3500 3700 3850 3700
Wire Wire Line
	3500 3800 3850 3800
Wire Wire Line
	3500 3900 3850 3900
Wire Wire Line
	3500 4000 3850 4000
Wire Wire Line
	3500 4100 3850 4100
Wire Wire Line
	3500 4200 3850 4200
Wire Wire Line
	3500 4300 3850 4300
Wire Wire Line
	3500 4400 3850 4400
Wire Wire Line
	3500 4500 3850 4500
Wire Wire Line
	3500 4600 3850 4600
Wire Wire Line
	3500 4700 3850 4700
Wire Wire Line
	3500 4800 3850 4800
Wire Wire Line
	3500 4900 3850 4900
Wire Wire Line
	3500 5000 3850 5000
Wire Wire Line
	3500 5100 3850 5100
Entry Wire Line
	3850 900  3950 1000
Entry Wire Line
	3850 1000 3950 1100
Entry Wire Line
	3850 1100 3950 1200
Entry Wire Line
	3850 1200 3950 1300
Entry Wire Line
	3850 1300 3950 1400
Entry Wire Line
	3850 1400 3950 1500
Entry Wire Line
	3850 1500 3950 1600
Entry Wire Line
	3850 1600 3950 1700
Entry Wire Line
	3850 1700 3950 1800
Entry Wire Line
	3850 1800 3950 1900
Entry Wire Line
	3850 1900 3950 2000
Entry Wire Line
	3850 2000 3950 2100
Entry Wire Line
	3850 2100 3950 2200
Entry Wire Line
	3850 2200 3950 2300
Entry Wire Line
	3850 2300 3950 2400
Entry Wire Line
	3850 2400 3950 2500
Entry Wire Line
	3850 2500 3950 2600
Entry Wire Line
	3850 2600 3950 2700
Entry Wire Line
	3850 2700 3950 2800
Entry Wire Line
	3850 2800 3950 2900
Entry Wire Line
	3850 2900 3950 3000
Entry Wire Line
	3850 3000 3950 3100
Entry Wire Line
	3850 3100 3950 3200
Entry Wire Line
	3850 3200 3950 3300
Entry Wire Line
	3850 3300 3950 3400
Entry Wire Line
	3850 3400 3950 3500
Entry Wire Line
	3850 3500 3950 3600
Entry Wire Line
	3850 3600 3950 3700
Entry Wire Line
	3850 3700 3950 3800
Entry Wire Line
	3850 3800 3950 3900
Entry Wire Line
	3850 3900 3950 4000
Entry Wire Line
	3850 4000 3950 4100
Entry Wire Line
	3850 4100 3950 4200
Entry Wire Line
	3850 4200 3950 4300
Entry Wire Line
	3850 4300 3950 4400
Entry Wire Line
	3850 4400 3950 4500
Entry Wire Line
	3850 4500 3950 4600
Entry Wire Line
	3850 4600 3950 4700
Entry Wire Line
	3850 4700 3950 4800
Entry Wire Line
	3850 4800 3950 4900
Entry Wire Line
	3850 4900 3950 5000
Entry Wire Line
	3850 5000 3950 5100
Entry Wire Line
	3850 5100 3950 5200
Text Label 3000 900  2    50   ~ 0
BP-A1
Text Label 3000 1000 2    50   ~ 0
BP-A2
Text Label 3000 1100 2    50   ~ 0
BP-A3
Text Label 3000 1200 2    50   ~ 0
BP-A4
Text Label 3000 1300 2    50   ~ 0
BP-A5
Text Label 3000 1400 2    50   ~ 0
BP-A6
Text Label 3000 1500 2    50   ~ 0
BP-A7
Text Label 3000 1600 2    50   ~ 0
BP-A8
Text Label 3000 1700 2    50   ~ 0
BP-A9
Text Label 3000 1800 2    50   ~ 0
BP-A10
Text Label 3000 1900 2    50   ~ 0
BP-A11
Text Label 3000 2000 2    50   ~ 0
BP-A12
Text Label 3000 2100 2    50   ~ 0
BP-A13
Text Label 3000 2200 2    50   ~ 0
BP-A14
Text Label 3000 2300 2    50   ~ 0
BP-A15
Text Label 3000 2400 2    50   ~ 0
BP-A16
Text Label 3000 2500 2    50   ~ 0
BP-A17
Text Label 3000 2600 2    50   ~ 0
BP-A18
Text Label 3000 2700 2    50   ~ 0
BP-A19
Text Label 3000 2800 2    50   ~ 0
BP-A20
Text Label 3000 2900 2    50   ~ 0
BP-A21
Text Label 3000 3000 2    50   ~ 0
BP-A22
Text Label 3000 3100 2    50   ~ 0
BP-A23
Text Label 3000 3200 2    50   ~ 0
BP-A24
Text Label 3000 3300 2    50   ~ 0
BP-A25
Text Label 3000 3400 2    50   ~ 0
BP-A26
Text Label 3000 3500 2    50   ~ 0
BP-A27
Text Label 3000 3600 2    50   ~ 0
BP-A28
Text Label 3000 3700 2    50   ~ 0
BP-A29
Text Label 3000 3800 2    50   ~ 0
BP-A30
Text Label 3000 3900 2    50   ~ 0
BP-A31
Text Label 3000 4000 2    50   ~ 0
BP-A32
Text Label 3000 4100 2    50   ~ 0
BP-A33
Text Label 3000 4200 2    50   ~ 0
BP-A34
Text Label 3000 4300 2    50   ~ 0
BP-A35
Text Label 3000 4400 2    50   ~ 0
BP-A36
Text Label 3000 4500 2    50   ~ 0
BP-A37
Text Label 3000 4600 2    50   ~ 0
BP-A38
Text Label 3000 4700 2    50   ~ 0
BP-A39
Text Label 3000 4800 2    50   ~ 0
BP-A40
Text Label 3000 4900 2    50   ~ 0
BP-A41
Text Label 3000 5000 2    50   ~ 0
BP-A42
Text Label 3000 5100 2    50   ~ 0
BP-A43
Wire Wire Line
	3000 900  2650 900 
Wire Wire Line
	3000 1000 2650 1000
Wire Wire Line
	3000 4900 2650 4900
Wire Wire Line
	3000 5000 2650 5000
Wire Wire Line
	3000 5100 2650 5100
Wire Wire Line
	3000 4400 2650 4400
Wire Wire Line
	3000 4300 2650 4300
Wire Wire Line
	3000 4200 2650 4200
Wire Wire Line
	3000 4100 2650 4100
Wire Wire Line
	3000 4000 2650 4000
Wire Wire Line
	3000 3900 2650 3900
Wire Wire Line
	3000 3800 2650 3800
Wire Wire Line
	3000 3700 2650 3700
Wire Wire Line
	3000 3600 2650 3600
Wire Wire Line
	3000 3500 2650 3500
Wire Wire Line
	3000 3400 2650 3400
Wire Wire Line
	3000 3300 2650 3300
Wire Wire Line
	3000 3200 2650 3200
Wire Wire Line
	3000 3100 2650 3100
Wire Wire Line
	3000 3000 2650 3000
Wire Wire Line
	3000 2900 2650 2900
Wire Wire Line
	3000 2800 2650 2800
Wire Wire Line
	3000 2700 2650 2700
Wire Wire Line
	3000 2600 2650 2600
Wire Wire Line
	3000 2500 2650 2500
Wire Wire Line
	3000 2400 2650 2400
Wire Wire Line
	3000 2300 2650 2300
Wire Wire Line
	3000 2200 2650 2200
Wire Wire Line
	3000 2100 2650 2100
Wire Wire Line
	3000 2000 2650 2000
Wire Wire Line
	3000 1900 2650 1900
Wire Wire Line
	3000 1800 2650 1800
Wire Wire Line
	3000 1700 2650 1700
Wire Wire Line
	3000 1600 2650 1600
Wire Wire Line
	3000 1500 2650 1500
Wire Wire Line
	3000 1400 2650 1400
Wire Wire Line
	3000 1300 2650 1300
Wire Wire Line
	3000 1200 2650 1200
Wire Wire Line
	3000 1100 2650 1100
Wire Wire Line
	3000 4800 2650 4800
Wire Wire Line
	3000 4700 2650 4700
Wire Wire Line
	3000 4600 2650 4600
Wire Wire Line
	3000 4500 2650 4500
$Comp
L DelightSymbols:Conn_02x43_Row_Letter_First J2
U 1 1 62816B6B
P 3250 3000
F 0 "J2" H 3250 5317 50  0000 C CNN
F 1 "Slot 1" H 3250 5226 50  0000 C CNN
F 2 "DelightFootprints:PinHeader_2x43_P2.54mm_Vertical" H 3200 3200 50  0001 C CNN
F 3 "~" H 3200 3200 50  0001 C CNN
	1    3250 3000
	1    0    0    -1  
$EndComp
Entry Wire Line
	3850 900  3950 1000
Entry Wire Line
	2550 5200 2650 5100
Entry Wire Line
	2550 5100 2650 5000
Entry Wire Line
	2550 5000 2650 4900
Entry Wire Line
	2550 4900 2650 4800
Entry Wire Line
	2550 4800 2650 4700
Entry Wire Line
	2550 4700 2650 4600
Entry Wire Line
	2550 4600 2650 4500
Entry Wire Line
	2550 4500 2650 4400
Entry Wire Line
	2550 4400 2650 4300
Entry Wire Line
	2550 4300 2650 4200
Entry Wire Line
	2550 4200 2650 4100
Entry Wire Line
	2550 4100 2650 4000
Entry Wire Line
	2550 4000 2650 3900
Entry Wire Line
	2550 3900 2650 3800
Entry Wire Line
	2550 3800 2650 3700
Entry Wire Line
	2550 3700 2650 3600
Entry Wire Line
	2550 3600 2650 3500
Entry Wire Line
	2550 3500 2650 3400
Entry Wire Line
	2550 3400 2650 3300
Entry Wire Line
	2550 3300 2650 3200
Entry Wire Line
	2550 3200 2650 3100
Entry Wire Line
	2550 3100 2650 3000
Entry Wire Line
	2550 3000 2650 2900
Entry Wire Line
	2550 2900 2650 2800
Entry Wire Line
	2550 2800 2650 2700
Entry Wire Line
	2550 2700 2650 2600
Entry Wire Line
	2550 2600 2650 2500
Entry Wire Line
	2550 2500 2650 2400
Entry Wire Line
	2550 2400 2650 2300
Entry Wire Line
	2550 2300 2650 2200
Entry Wire Line
	2550 2200 2650 2100
Entry Wire Line
	2550 2100 2650 2000
Entry Wire Line
	2550 2000 2650 1900
Entry Wire Line
	2550 1900 2650 1800
Entry Wire Line
	2550 1800 2650 1700
Entry Wire Line
	2550 1700 2650 1600
Entry Wire Line
	2550 1600 2650 1500
Entry Wire Line
	2550 1500 2650 1400
Entry Wire Line
	2550 1400 2650 1300
Entry Wire Line
	2550 1300 2650 1200
Entry Wire Line
	2550 1200 2650 1100
Entry Wire Line
	2550 1100 2650 1000
Entry Wire Line
	2550 1000 2650 900 
Entry Wire Line
	2550 5200 2650 5100
Text Label 5000 900  0    50   ~ 0
BP-B1
Text Label 5000 1000 0    50   ~ 0
BP-B2
Text Label 5000 1100 0    50   ~ 0
BP-B3
Text Label 5000 1200 0    50   ~ 0
BP-B4
Text Label 5000 1300 0    50   ~ 0
BP-B5
Text Label 5000 1400 0    50   ~ 0
BP-B6
Text Label 5000 1500 0    50   ~ 0
BP-B7
Text Label 5000 1600 0    50   ~ 0
BP-B8
Text Label 5000 1700 0    50   ~ 0
BP-B9
Text Label 5000 1800 0    50   ~ 0
BP-B10
Text Label 5000 1900 0    50   ~ 0
BP-B11
Text Label 5000 2000 0    50   ~ 0
BP-B12
Text Label 5000 2100 0    50   ~ 0
BP-B13
Text Label 5000 2200 0    50   ~ 0
BP-B14
Text Label 5000 2300 0    50   ~ 0
BP-B15
Text Label 5000 2400 0    50   ~ 0
BP-B16
Text Label 5000 2500 0    50   ~ 0
BP-B17
Text Label 5000 2600 0    50   ~ 0
BP-B18
Text Label 5000 2700 0    50   ~ 0
BP-B19
Text Label 5000 2800 0    50   ~ 0
BP-B20
Text Label 5000 2900 0    50   ~ 0
BP-B21
Text Label 5000 3000 0    50   ~ 0
BP-B22
Text Label 5000 3100 0    50   ~ 0
BP-B23
Text Label 5000 3200 0    50   ~ 0
BP-B24
Text Label 5000 3300 0    50   ~ 0
BP-B25
Text Label 5000 3400 0    50   ~ 0
BP-B26
Text Label 5000 3500 0    50   ~ 0
BP-B27
Text Label 5000 3600 0    50   ~ 0
BP-B28
Text Label 5000 3700 0    50   ~ 0
BP-B29
Text Label 5000 3800 0    50   ~ 0
BP-B30
Text Label 5000 3900 0    50   ~ 0
BP-B31
Text Label 5000 4000 0    50   ~ 0
BP-B32
Text Label 5000 4100 0    50   ~ 0
BP-B33
Text Label 5000 4200 0    50   ~ 0
BP-B34
Text Label 5000 4300 0    50   ~ 0
BP-B35
Text Label 5000 4400 0    50   ~ 0
BP-B36
Text Label 5000 4500 0    50   ~ 0
BP-B37
Text Label 5000 4600 0    50   ~ 0
BP-B38
Text Label 5000 4700 0    50   ~ 0
BP-B39
Text Label 5000 4800 0    50   ~ 0
BP-B40
Text Label 5000 4900 0    50   ~ 0
BP-B41
Text Label 5000 5000 0    50   ~ 0
BP-B42
Text Label 5000 5100 0    50   ~ 0
BP-B43
Wire Wire Line
	5000 900  5350 900 
Wire Wire Line
	5000 1000 5350 1000
Wire Wire Line
	5000 1100 5350 1100
Wire Wire Line
	5000 1200 5350 1200
Wire Wire Line
	5000 1300 5350 1300
Wire Wire Line
	5000 1400 5350 1400
Wire Wire Line
	5000 1500 5350 1500
Wire Wire Line
	5000 1600 5350 1600
Wire Wire Line
	5000 1700 5350 1700
Wire Wire Line
	5000 1800 5350 1800
Wire Wire Line
	5000 1900 5350 1900
Wire Wire Line
	5000 2000 5350 2000
Wire Wire Line
	5000 2100 5350 2100
Wire Wire Line
	5000 2200 5350 2200
Wire Wire Line
	5000 2300 5350 2300
Wire Wire Line
	5000 2400 5350 2400
Wire Wire Line
	5000 2500 5350 2500
Wire Wire Line
	5000 2600 5350 2600
Wire Wire Line
	5000 2700 5350 2700
Wire Wire Line
	5000 2800 5350 2800
Wire Wire Line
	5000 2900 5350 2900
Wire Wire Line
	5000 3000 5350 3000
Wire Wire Line
	5000 3100 5350 3100
Wire Wire Line
	5000 3200 5350 3200
Wire Wire Line
	5000 3300 5350 3300
Wire Wire Line
	5000 3400 5350 3400
Wire Wire Line
	5000 3500 5350 3500
Wire Wire Line
	5000 3600 5350 3600
Wire Wire Line
	5000 3700 5350 3700
Wire Wire Line
	5000 3800 5350 3800
Wire Wire Line
	5000 3900 5350 3900
Wire Wire Line
	5000 4000 5350 4000
Wire Wire Line
	5000 4100 5350 4100
Wire Wire Line
	5000 4200 5350 4200
Wire Wire Line
	5000 4300 5350 4300
Wire Wire Line
	5000 4400 5350 4400
Wire Wire Line
	5000 4500 5350 4500
Wire Wire Line
	5000 4600 5350 4600
Wire Wire Line
	5000 4700 5350 4700
Wire Wire Line
	5000 4800 5350 4800
Wire Wire Line
	5000 4900 5350 4900
Wire Wire Line
	5000 5000 5350 5000
Wire Wire Line
	5000 5100 5350 5100
Entry Wire Line
	5350 900  5450 1000
Entry Wire Line
	5350 1000 5450 1100
Entry Wire Line
	5350 1100 5450 1200
Entry Wire Line
	5350 1200 5450 1300
Entry Wire Line
	5350 1300 5450 1400
Entry Wire Line
	5350 1400 5450 1500
Entry Wire Line
	5350 1500 5450 1600
Entry Wire Line
	5350 1600 5450 1700
Entry Wire Line
	5350 1700 5450 1800
Entry Wire Line
	5350 1800 5450 1900
Entry Wire Line
	5350 1900 5450 2000
Entry Wire Line
	5350 2000 5450 2100
Entry Wire Line
	5350 2100 5450 2200
Entry Wire Line
	5350 2200 5450 2300
Entry Wire Line
	5350 2300 5450 2400
Entry Wire Line
	5350 2400 5450 2500
Entry Wire Line
	5350 2500 5450 2600
Entry Wire Line
	5350 2600 5450 2700
Entry Wire Line
	5350 2700 5450 2800
Entry Wire Line
	5350 2800 5450 2900
Entry Wire Line
	5350 2900 5450 3000
Entry Wire Line
	5350 3000 5450 3100
Entry Wire Line
	5350 3100 5450 3200
Entry Wire Line
	5350 3200 5450 3300
Entry Wire Line
	5350 3300 5450 3400
Entry Wire Line
	5350 3400 5450 3500
Entry Wire Line
	5350 3500 5450 3600
Entry Wire Line
	5350 3600 5450 3700
Entry Wire Line
	5350 3700 5450 3800
Entry Wire Line
	5350 3800 5450 3900
Entry Wire Line
	5350 3900 5450 4000
Entry Wire Line
	5350 4000 5450 4100
Entry Wire Line
	5350 4100 5450 4200
Entry Wire Line
	5350 4200 5450 4300
Entry Wire Line
	5350 4300 5450 4400
Entry Wire Line
	5350 4400 5450 4500
Entry Wire Line
	5350 4500 5450 4600
Entry Wire Line
	5350 4600 5450 4700
Entry Wire Line
	5350 4700 5450 4800
Entry Wire Line
	5350 4800 5450 4900
Entry Wire Line
	5350 4900 5450 5000
Entry Wire Line
	5350 5000 5450 5100
Entry Wire Line
	5350 5100 5450 5200
Text Label 4500 900  2    50   ~ 0
BP-A1
Text Label 4500 1000 2    50   ~ 0
BP-A2
Text Label 4500 1100 2    50   ~ 0
BP-A3
Text Label 4500 1200 2    50   ~ 0
BP-A4
Text Label 4500 1300 2    50   ~ 0
BP-A5
Text Label 4500 1400 2    50   ~ 0
BP-A6
Text Label 4500 1500 2    50   ~ 0
BP-A7
Text Label 4500 1600 2    50   ~ 0
BP-A8
Text Label 4500 1700 2    50   ~ 0
BP-A9
Text Label 4500 1800 2    50   ~ 0
BP-A10
Text Label 4500 1900 2    50   ~ 0
BP-A11
Text Label 4500 2000 2    50   ~ 0
BP-A12
Text Label 4500 2100 2    50   ~ 0
BP-A13
Text Label 4500 2200 2    50   ~ 0
BP-A14
Text Label 4500 2300 2    50   ~ 0
BP-A15
Text Label 4500 2400 2    50   ~ 0
BP-A16
Text Label 4500 2500 2    50   ~ 0
BP-A17
Text Label 4500 2600 2    50   ~ 0
BP-A18
Text Label 4500 2700 2    50   ~ 0
BP-A19
Text Label 4500 2800 2    50   ~ 0
BP-A20
Text Label 4500 2900 2    50   ~ 0
BP-A21
Text Label 4500 3000 2    50   ~ 0
BP-A22
Text Label 4500 3100 2    50   ~ 0
BP-A23
Text Label 4500 3200 2    50   ~ 0
BP-A24
Text Label 4500 3300 2    50   ~ 0
BP-A25
Text Label 4500 3400 2    50   ~ 0
BP-A26
Text Label 4500 3500 2    50   ~ 0
BP-A27
Text Label 4500 3600 2    50   ~ 0
BP-A28
Text Label 4500 3700 2    50   ~ 0
BP-A29
Text Label 4500 3800 2    50   ~ 0
BP-A30
Text Label 4500 3900 2    50   ~ 0
BP-A31
Text Label 4500 4000 2    50   ~ 0
BP-A32
Text Label 4500 4100 2    50   ~ 0
BP-A33
Text Label 4500 4200 2    50   ~ 0
BP-A34
Text Label 4500 4300 2    50   ~ 0
BP-A35
Text Label 4500 4400 2    50   ~ 0
BP-A36
Text Label 4500 4500 2    50   ~ 0
BP-A37
Text Label 4500 4600 2    50   ~ 0
BP-A38
Text Label 4500 4700 2    50   ~ 0
BP-A39
Text Label 4500 4800 2    50   ~ 0
BP-A40
Text Label 4500 4900 2    50   ~ 0
BP-A41
Text Label 4500 5000 2    50   ~ 0
BP-A42
Text Label 4500 5100 2    50   ~ 0
BP-A43
Wire Wire Line
	4500 900  4150 900 
Wire Wire Line
	4500 1000 4150 1000
Wire Wire Line
	4500 4900 4150 4900
Wire Wire Line
	4500 5000 4150 5000
Wire Wire Line
	4500 5100 4150 5100
Wire Wire Line
	4500 4400 4150 4400
Wire Wire Line
	4500 4300 4150 4300
Wire Wire Line
	4500 4200 4150 4200
Wire Wire Line
	4500 4100 4150 4100
Wire Wire Line
	4500 4000 4150 4000
Wire Wire Line
	4500 3900 4150 3900
Wire Wire Line
	4500 3800 4150 3800
Wire Wire Line
	4500 3700 4150 3700
Wire Wire Line
	4500 3600 4150 3600
Wire Wire Line
	4500 3500 4150 3500
Wire Wire Line
	4500 3400 4150 3400
Wire Wire Line
	4500 3300 4150 3300
Wire Wire Line
	4500 3200 4150 3200
Wire Wire Line
	4500 3100 4150 3100
Wire Wire Line
	4500 3000 4150 3000
Wire Wire Line
	4500 2900 4150 2900
Wire Wire Line
	4500 2800 4150 2800
Wire Wire Line
	4500 2700 4150 2700
Wire Wire Line
	4500 2600 4150 2600
Wire Wire Line
	4500 2500 4150 2500
Wire Wire Line
	4500 2400 4150 2400
Wire Wire Line
	4500 2300 4150 2300
Wire Wire Line
	4500 2200 4150 2200
Wire Wire Line
	4500 2100 4150 2100
Wire Wire Line
	4500 2000 4150 2000
Wire Wire Line
	4500 1900 4150 1900
Wire Wire Line
	4500 1800 4150 1800
Wire Wire Line
	4500 1700 4150 1700
Wire Wire Line
	4500 1600 4150 1600
Wire Wire Line
	4500 1500 4150 1500
Wire Wire Line
	4500 1400 4150 1400
Wire Wire Line
	4500 1300 4150 1300
Wire Wire Line
	4500 1200 4150 1200
Wire Wire Line
	4500 1100 4150 1100
Wire Wire Line
	4500 4800 4150 4800
Wire Wire Line
	4500 4700 4150 4700
Wire Wire Line
	4500 4600 4150 4600
Wire Wire Line
	4500 4500 4150 4500
$Comp
L DelightSymbols:Conn_02x43_Row_Letter_First J3
U 1 1 62840C7F
P 4750 3000
F 0 "J3" H 4750 5317 50  0000 C CNN
F 1 "Slot 2" H 4750 5226 50  0000 C CNN
F 2 "DelightFootprints:PinHeader_2x43_P2.54mm_Vertical" H 4700 3200 50  0001 C CNN
F 3 "~" H 4700 3200 50  0001 C CNN
	1    4750 3000
	1    0    0    -1  
$EndComp
Entry Wire Line
	5350 900  5450 1000
Entry Wire Line
	4050 5200 4150 5100
Entry Wire Line
	4050 5100 4150 5000
Entry Wire Line
	4050 5000 4150 4900
Entry Wire Line
	4050 4900 4150 4800
Entry Wire Line
	4050 4800 4150 4700
Entry Wire Line
	4050 4700 4150 4600
Entry Wire Line
	4050 4600 4150 4500
Entry Wire Line
	4050 4500 4150 4400
Entry Wire Line
	4050 4400 4150 4300
Entry Wire Line
	4050 4300 4150 4200
Entry Wire Line
	4050 4200 4150 4100
Entry Wire Line
	4050 4100 4150 4000
Entry Wire Line
	4050 4000 4150 3900
Entry Wire Line
	4050 3900 4150 3800
Entry Wire Line
	4050 3800 4150 3700
Entry Wire Line
	4050 3700 4150 3600
Entry Wire Line
	4050 3600 4150 3500
Entry Wire Line
	4050 3500 4150 3400
Entry Wire Line
	4050 3400 4150 3300
Entry Wire Line
	4050 3300 4150 3200
Entry Wire Line
	4050 3200 4150 3100
Entry Wire Line
	4050 3100 4150 3000
Entry Wire Line
	4050 3000 4150 2900
Entry Wire Line
	4050 2900 4150 2800
Entry Wire Line
	4050 2800 4150 2700
Entry Wire Line
	4050 2700 4150 2600
Entry Wire Line
	4050 2600 4150 2500
Entry Wire Line
	4050 2500 4150 2400
Entry Wire Line
	4050 2400 4150 2300
Entry Wire Line
	4050 2300 4150 2200
Entry Wire Line
	4050 2200 4150 2100
Entry Wire Line
	4050 2100 4150 2000
Entry Wire Line
	4050 2000 4150 1900
Entry Wire Line
	4050 1900 4150 1800
Entry Wire Line
	4050 1800 4150 1700
Entry Wire Line
	4050 1700 4150 1600
Entry Wire Line
	4050 1600 4150 1500
Entry Wire Line
	4050 1500 4150 1400
Entry Wire Line
	4050 1400 4150 1300
Entry Wire Line
	4050 1300 4150 1200
Entry Wire Line
	4050 1200 4150 1100
Entry Wire Line
	4050 1100 4150 1000
Entry Wire Line
	4050 1000 4150 900 
Entry Wire Line
	4050 5200 4150 5100
Text Label 6500 900  0    50   ~ 0
BP-B1
Text Label 6500 1000 0    50   ~ 0
BP-B2
Text Label 6500 1100 0    50   ~ 0
BP-B3
Text Label 6500 1200 0    50   ~ 0
BP-B4
Text Label 6500 1300 0    50   ~ 0
BP-B5
Text Label 6500 1400 0    50   ~ 0
BP-B6
Text Label 6500 1500 0    50   ~ 0
BP-B7
Text Label 6500 1600 0    50   ~ 0
BP-B8
Text Label 6500 1700 0    50   ~ 0
BP-B9
Text Label 6500 1800 0    50   ~ 0
BP-B10
Text Label 6500 1900 0    50   ~ 0
BP-B11
Text Label 6500 2000 0    50   ~ 0
BP-B12
Text Label 6500 2100 0    50   ~ 0
BP-B13
Text Label 6500 2200 0    50   ~ 0
BP-B14
Text Label 6500 2300 0    50   ~ 0
BP-B15
Text Label 6500 2400 0    50   ~ 0
BP-B16
Text Label 6500 2500 0    50   ~ 0
BP-B17
Text Label 6500 2600 0    50   ~ 0
BP-B18
Text Label 6500 2700 0    50   ~ 0
BP-B19
Text Label 6500 2800 0    50   ~ 0
BP-B20
Text Label 6500 2900 0    50   ~ 0
BP-B21
Text Label 6500 3000 0    50   ~ 0
BP-B22
Text Label 6500 3100 0    50   ~ 0
BP-B23
Text Label 6500 3200 0    50   ~ 0
BP-B24
Text Label 6500 3300 0    50   ~ 0
BP-B25
Text Label 6500 3400 0    50   ~ 0
BP-B26
Text Label 6500 3500 0    50   ~ 0
BP-B27
Text Label 6500 3600 0    50   ~ 0
BP-B28
Text Label 6500 3700 0    50   ~ 0
BP-B29
Text Label 6500 3800 0    50   ~ 0
BP-B30
Text Label 6500 3900 0    50   ~ 0
BP-B31
Text Label 6500 4000 0    50   ~ 0
BP-B32
Text Label 6500 4100 0    50   ~ 0
BP-B33
Text Label 6500 4200 0    50   ~ 0
BP-B34
Text Label 6500 4300 0    50   ~ 0
BP-B35
Text Label 6500 4400 0    50   ~ 0
BP-B36
Text Label 6500 4500 0    50   ~ 0
BP-B37
Text Label 6500 4600 0    50   ~ 0
BP-B38
Text Label 6500 4700 0    50   ~ 0
BP-B39
Text Label 6500 4800 0    50   ~ 0
BP-B40
Text Label 6500 4900 0    50   ~ 0
BP-B41
Text Label 6500 5000 0    50   ~ 0
BP-B42
Text Label 6500 5100 0    50   ~ 0
BP-B43
Wire Wire Line
	6500 900  6850 900 
Wire Wire Line
	6500 1000 6850 1000
Wire Wire Line
	6500 1100 6850 1100
Wire Wire Line
	6500 1200 6850 1200
Wire Wire Line
	6500 1300 6850 1300
Wire Wire Line
	6500 1400 6850 1400
Wire Wire Line
	6500 1500 6850 1500
Wire Wire Line
	6500 1600 6850 1600
Wire Wire Line
	6500 1700 6850 1700
Wire Wire Line
	6500 1800 6850 1800
Wire Wire Line
	6500 1900 6850 1900
Wire Wire Line
	6500 2000 6850 2000
Wire Wire Line
	6500 2100 6850 2100
Wire Wire Line
	6500 2200 6850 2200
Wire Wire Line
	6500 2300 6850 2300
Wire Wire Line
	6500 2400 6850 2400
Wire Wire Line
	6500 2500 6850 2500
Wire Wire Line
	6500 2600 6850 2600
Wire Wire Line
	6500 2700 6850 2700
Wire Wire Line
	6500 2800 6850 2800
Wire Wire Line
	6500 2900 6850 2900
Wire Wire Line
	6500 3000 6850 3000
Wire Wire Line
	6500 3100 6850 3100
Wire Wire Line
	6500 3200 6850 3200
Wire Wire Line
	6500 3300 6850 3300
Wire Wire Line
	6500 3400 6850 3400
Wire Wire Line
	6500 3500 6850 3500
Wire Wire Line
	6500 3600 6850 3600
Wire Wire Line
	6500 3700 6850 3700
Wire Wire Line
	6500 3800 6850 3800
Wire Wire Line
	6500 3900 6850 3900
Wire Wire Line
	6500 4000 6850 4000
Wire Wire Line
	6500 4100 6850 4100
Wire Wire Line
	6500 4200 6850 4200
Wire Wire Line
	6500 4300 6850 4300
Wire Wire Line
	6500 4400 6850 4400
Wire Wire Line
	6500 4500 6850 4500
Wire Wire Line
	6500 4600 6850 4600
Wire Wire Line
	6500 4700 6850 4700
Wire Wire Line
	6500 4800 6850 4800
Wire Wire Line
	6500 4900 6850 4900
Wire Wire Line
	6500 5000 6850 5000
Wire Wire Line
	6500 5100 6850 5100
Entry Wire Line
	6850 900  6950 1000
Entry Wire Line
	6850 1000 6950 1100
Entry Wire Line
	6850 1100 6950 1200
Entry Wire Line
	6850 1200 6950 1300
Entry Wire Line
	6850 1300 6950 1400
Entry Wire Line
	6850 1400 6950 1500
Entry Wire Line
	6850 1500 6950 1600
Entry Wire Line
	6850 1600 6950 1700
Entry Wire Line
	6850 1700 6950 1800
Entry Wire Line
	6850 1800 6950 1900
Entry Wire Line
	6850 1900 6950 2000
Entry Wire Line
	6850 2000 6950 2100
Entry Wire Line
	6850 2100 6950 2200
Entry Wire Line
	6850 2200 6950 2300
Entry Wire Line
	6850 2300 6950 2400
Entry Wire Line
	6850 2400 6950 2500
Entry Wire Line
	6850 2500 6950 2600
Entry Wire Line
	6850 2600 6950 2700
Entry Wire Line
	6850 2700 6950 2800
Entry Wire Line
	6850 2800 6950 2900
Entry Wire Line
	6850 2900 6950 3000
Entry Wire Line
	6850 3000 6950 3100
Entry Wire Line
	6850 3100 6950 3200
Entry Wire Line
	6850 3200 6950 3300
Entry Wire Line
	6850 3300 6950 3400
Entry Wire Line
	6850 3400 6950 3500
Entry Wire Line
	6850 3500 6950 3600
Entry Wire Line
	6850 3600 6950 3700
Entry Wire Line
	6850 3700 6950 3800
Entry Wire Line
	6850 3800 6950 3900
Entry Wire Line
	6850 3900 6950 4000
Entry Wire Line
	6850 4000 6950 4100
Entry Wire Line
	6850 4100 6950 4200
Entry Wire Line
	6850 4200 6950 4300
Entry Wire Line
	6850 4300 6950 4400
Entry Wire Line
	6850 4400 6950 4500
Entry Wire Line
	6850 4500 6950 4600
Entry Wire Line
	6850 4600 6950 4700
Entry Wire Line
	6850 4700 6950 4800
Entry Wire Line
	6850 4800 6950 4900
Entry Wire Line
	6850 4900 6950 5000
Entry Wire Line
	6850 5000 6950 5100
Entry Wire Line
	6850 5100 6950 5200
Text Label 6000 900  2    50   ~ 0
BP-A1
Text Label 6000 1000 2    50   ~ 0
BP-A2
Text Label 6000 1100 2    50   ~ 0
BP-A3
Text Label 6000 1200 2    50   ~ 0
BP-A4
Text Label 6000 1300 2    50   ~ 0
BP-A5
Text Label 6000 1400 2    50   ~ 0
BP-A6
Text Label 6000 1500 2    50   ~ 0
BP-A7
Text Label 6000 1600 2    50   ~ 0
BP-A8
Text Label 6000 1700 2    50   ~ 0
BP-A9
Text Label 6000 1800 2    50   ~ 0
BP-A10
Text Label 6000 1900 2    50   ~ 0
BP-A11
Text Label 6000 2000 2    50   ~ 0
BP-A12
Text Label 6000 2100 2    50   ~ 0
BP-A13
Text Label 6000 2200 2    50   ~ 0
BP-A14
Text Label 6000 2300 2    50   ~ 0
BP-A15
Text Label 6000 2400 2    50   ~ 0
BP-A16
Text Label 6000 2500 2    50   ~ 0
BP-A17
Text Label 6000 2600 2    50   ~ 0
BP-A18
Text Label 6000 2700 2    50   ~ 0
BP-A19
Text Label 6000 2800 2    50   ~ 0
BP-A20
Text Label 6000 2900 2    50   ~ 0
BP-A21
Text Label 6000 3000 2    50   ~ 0
BP-A22
Text Label 6000 3100 2    50   ~ 0
BP-A23
Text Label 6000 3200 2    50   ~ 0
BP-A24
Text Label 6000 3300 2    50   ~ 0
BP-A25
Text Label 6000 3400 2    50   ~ 0
BP-A26
Text Label 6000 3500 2    50   ~ 0
BP-A27
Text Label 6000 3600 2    50   ~ 0
BP-A28
Text Label 6000 3700 2    50   ~ 0
BP-A29
Text Label 6000 3800 2    50   ~ 0
BP-A30
Text Label 6000 3900 2    50   ~ 0
BP-A31
Text Label 6000 4000 2    50   ~ 0
BP-A32
Text Label 6000 4100 2    50   ~ 0
BP-A33
Text Label 6000 4200 2    50   ~ 0
BP-A34
Text Label 6000 4300 2    50   ~ 0
BP-A35
Text Label 6000 4400 2    50   ~ 0
BP-A36
Text Label 6000 4500 2    50   ~ 0
BP-A37
Text Label 6000 4600 2    50   ~ 0
BP-A38
Text Label 6000 4700 2    50   ~ 0
BP-A39
Text Label 6000 4800 2    50   ~ 0
BP-A40
Text Label 6000 4900 2    50   ~ 0
BP-A41
Text Label 6000 5000 2    50   ~ 0
BP-A42
Text Label 6000 5100 2    50   ~ 0
BP-A43
Wire Wire Line
	6000 900  5650 900 
Wire Wire Line
	6000 1000 5650 1000
Wire Wire Line
	6000 4900 5650 4900
Wire Wire Line
	6000 5000 5650 5000
Wire Wire Line
	6000 5100 5650 5100
Wire Wire Line
	6000 4400 5650 4400
Wire Wire Line
	6000 4300 5650 4300
Wire Wire Line
	6000 4200 5650 4200
Wire Wire Line
	6000 4100 5650 4100
Wire Wire Line
	6000 4000 5650 4000
Wire Wire Line
	6000 3900 5650 3900
Wire Wire Line
	6000 3800 5650 3800
Wire Wire Line
	6000 3700 5650 3700
Wire Wire Line
	6000 3600 5650 3600
Wire Wire Line
	6000 3500 5650 3500
Wire Wire Line
	6000 3400 5650 3400
Wire Wire Line
	6000 3300 5650 3300
Wire Wire Line
	6000 3200 5650 3200
Wire Wire Line
	6000 3100 5650 3100
Wire Wire Line
	6000 3000 5650 3000
Wire Wire Line
	6000 2900 5650 2900
Wire Wire Line
	6000 2800 5650 2800
Wire Wire Line
	6000 2700 5650 2700
Wire Wire Line
	6000 2600 5650 2600
Wire Wire Line
	6000 2500 5650 2500
Wire Wire Line
	6000 2400 5650 2400
Wire Wire Line
	6000 2300 5650 2300
Wire Wire Line
	6000 2200 5650 2200
Wire Wire Line
	6000 2100 5650 2100
Wire Wire Line
	6000 2000 5650 2000
Wire Wire Line
	6000 1900 5650 1900
Wire Wire Line
	6000 1800 5650 1800
Wire Wire Line
	6000 1700 5650 1700
Wire Wire Line
	6000 1600 5650 1600
Wire Wire Line
	6000 1500 5650 1500
Wire Wire Line
	6000 1400 5650 1400
Wire Wire Line
	6000 1300 5650 1300
Wire Wire Line
	6000 1200 5650 1200
Wire Wire Line
	6000 1100 5650 1100
Wire Wire Line
	6000 4800 5650 4800
Wire Wire Line
	6000 4700 5650 4700
Wire Wire Line
	6000 4600 5650 4600
Wire Wire Line
	6000 4500 5650 4500
$Comp
L DelightSymbols:Conn_02x43_Row_Letter_First J4
U 1 1 6286AC89
P 6250 3000
F 0 "J4" H 6250 5317 50  0000 C CNN
F 1 "Slot 3" H 6250 5226 50  0000 C CNN
F 2 "DelightFootprints:PinHeader_2x43_P2.54mm_Vertical" H 6200 3200 50  0001 C CNN
F 3 "~" H 6200 3200 50  0001 C CNN
	1    6250 3000
	1    0    0    -1  
$EndComp
Entry Wire Line
	6850 900  6950 1000
Entry Wire Line
	5550 5200 5650 5100
Entry Wire Line
	5550 5100 5650 5000
Entry Wire Line
	5550 5000 5650 4900
Entry Wire Line
	5550 4900 5650 4800
Entry Wire Line
	5550 4800 5650 4700
Entry Wire Line
	5550 4700 5650 4600
Entry Wire Line
	5550 4600 5650 4500
Entry Wire Line
	5550 4500 5650 4400
Entry Wire Line
	5550 4400 5650 4300
Entry Wire Line
	5550 4300 5650 4200
Entry Wire Line
	5550 4200 5650 4100
Entry Wire Line
	5550 4100 5650 4000
Entry Wire Line
	5550 4000 5650 3900
Entry Wire Line
	5550 3900 5650 3800
Entry Wire Line
	5550 3800 5650 3700
Entry Wire Line
	5550 3700 5650 3600
Entry Wire Line
	5550 3600 5650 3500
Entry Wire Line
	5550 3500 5650 3400
Entry Wire Line
	5550 3400 5650 3300
Entry Wire Line
	5550 3300 5650 3200
Entry Wire Line
	5550 3200 5650 3100
Entry Wire Line
	5550 3100 5650 3000
Entry Wire Line
	5550 3000 5650 2900
Entry Wire Line
	5550 2900 5650 2800
Entry Wire Line
	5550 2800 5650 2700
Entry Wire Line
	5550 2700 5650 2600
Entry Wire Line
	5550 2600 5650 2500
Entry Wire Line
	5550 2500 5650 2400
Entry Wire Line
	5550 2400 5650 2300
Entry Wire Line
	5550 2300 5650 2200
Entry Wire Line
	5550 2200 5650 2100
Entry Wire Line
	5550 2100 5650 2000
Entry Wire Line
	5550 2000 5650 1900
Entry Wire Line
	5550 1900 5650 1800
Entry Wire Line
	5550 1800 5650 1700
Entry Wire Line
	5550 1700 5650 1600
Entry Wire Line
	5550 1600 5650 1500
Entry Wire Line
	5550 1500 5650 1400
Entry Wire Line
	5550 1400 5650 1300
Entry Wire Line
	5550 1300 5650 1200
Entry Wire Line
	5550 1200 5650 1100
Entry Wire Line
	5550 1100 5650 1000
Entry Wire Line
	5550 1000 5650 900 
Entry Wire Line
	5550 5200 5650 5100
Text Label 8000 900  0    50   ~ 0
BP-B1
Text Label 8000 1000 0    50   ~ 0
BP-B2
Text Label 8000 1100 0    50   ~ 0
BP-B3
Text Label 8000 1200 0    50   ~ 0
BP-B4
Text Label 8000 1300 0    50   ~ 0
BP-B5
Text Label 8000 1400 0    50   ~ 0
BP-B6
Text Label 8000 1500 0    50   ~ 0
BP-B7
Text Label 8000 1600 0    50   ~ 0
BP-B8
Text Label 8000 1700 0    50   ~ 0
BP-B9
Text Label 8000 1800 0    50   ~ 0
BP-B10
Text Label 8000 1900 0    50   ~ 0
BP-B11
Text Label 8000 2000 0    50   ~ 0
BP-B12
Text Label 8000 2100 0    50   ~ 0
BP-B13
Text Label 8000 2200 0    50   ~ 0
BP-B14
Text Label 8000 2300 0    50   ~ 0
BP-B15
Text Label 8000 2400 0    50   ~ 0
BP-B16
Text Label 8000 2500 0    50   ~ 0
BP-B17
Text Label 8000 2600 0    50   ~ 0
BP-B18
Text Label 8000 2700 0    50   ~ 0
BP-B19
Text Label 8000 2800 0    50   ~ 0
BP-B20
Text Label 8000 2900 0    50   ~ 0
BP-B21
Text Label 8000 3000 0    50   ~ 0
BP-B22
Text Label 8000 3100 0    50   ~ 0
BP-B23
Text Label 8000 3200 0    50   ~ 0
BP-B24
Text Label 8000 3300 0    50   ~ 0
BP-B25
Text Label 8000 3400 0    50   ~ 0
BP-B26
Text Label 8000 3500 0    50   ~ 0
BP-B27
Text Label 8000 3600 0    50   ~ 0
BP-B28
Text Label 8000 3700 0    50   ~ 0
BP-B29
Text Label 8000 3800 0    50   ~ 0
BP-B30
Text Label 8000 3900 0    50   ~ 0
BP-B31
Text Label 8000 4000 0    50   ~ 0
BP-B32
Text Label 8000 4100 0    50   ~ 0
BP-B33
Text Label 8000 4200 0    50   ~ 0
BP-B34
Text Label 8000 4300 0    50   ~ 0
BP-B35
Text Label 8000 4400 0    50   ~ 0
BP-B36
Text Label 8000 4500 0    50   ~ 0
BP-B37
Text Label 8000 4600 0    50   ~ 0
BP-B38
Text Label 8000 4700 0    50   ~ 0
BP-B39
Text Label 8000 4800 0    50   ~ 0
BP-B40
Text Label 8000 4900 0    50   ~ 0
BP-B41
Text Label 8000 5000 0    50   ~ 0
BP-B42
Text Label 8000 5100 0    50   ~ 0
BP-B43
Wire Wire Line
	8000 900  8350 900 
Wire Wire Line
	8000 1000 8350 1000
Wire Wire Line
	8000 1100 8350 1100
Wire Wire Line
	8000 1200 8350 1200
Wire Wire Line
	8000 1300 8350 1300
Wire Wire Line
	8000 1400 8350 1400
Wire Wire Line
	8000 1500 8350 1500
Wire Wire Line
	8000 1600 8350 1600
Wire Wire Line
	8000 1700 8350 1700
Wire Wire Line
	8000 1800 8350 1800
Wire Wire Line
	8000 1900 8350 1900
Wire Wire Line
	8000 2000 8350 2000
Wire Wire Line
	8000 2100 8350 2100
Wire Wire Line
	8000 2200 8350 2200
Wire Wire Line
	8000 2300 8350 2300
Wire Wire Line
	8000 2400 8350 2400
Wire Wire Line
	8000 2500 8350 2500
Wire Wire Line
	8000 2600 8350 2600
Wire Wire Line
	8000 2700 8350 2700
Wire Wire Line
	8000 2800 8350 2800
Wire Wire Line
	8000 2900 8350 2900
Wire Wire Line
	8000 3000 8350 3000
Wire Wire Line
	8000 3100 8350 3100
Wire Wire Line
	8000 3200 8350 3200
Wire Wire Line
	8000 3300 8350 3300
Wire Wire Line
	8000 3400 8350 3400
Wire Wire Line
	8000 3500 8350 3500
Wire Wire Line
	8000 3600 8350 3600
Wire Wire Line
	8000 3700 8350 3700
Wire Wire Line
	8000 3800 8350 3800
Wire Wire Line
	8000 3900 8350 3900
Wire Wire Line
	8000 4000 8350 4000
Wire Wire Line
	8000 4100 8350 4100
Wire Wire Line
	8000 4200 8350 4200
Wire Wire Line
	8000 4300 8350 4300
Wire Wire Line
	8000 4400 8350 4400
Wire Wire Line
	8000 4500 8350 4500
Wire Wire Line
	8000 4600 8350 4600
Wire Wire Line
	8000 4700 8350 4700
Wire Wire Line
	8000 4800 8350 4800
Wire Wire Line
	8000 4900 8350 4900
Wire Wire Line
	8000 5000 8350 5000
Wire Wire Line
	8000 5100 8350 5100
Entry Wire Line
	8350 900  8450 1000
Entry Wire Line
	8350 1000 8450 1100
Entry Wire Line
	8350 1100 8450 1200
Entry Wire Line
	8350 1200 8450 1300
Entry Wire Line
	8350 1300 8450 1400
Entry Wire Line
	8350 1400 8450 1500
Entry Wire Line
	8350 1500 8450 1600
Entry Wire Line
	8350 1600 8450 1700
Entry Wire Line
	8350 1700 8450 1800
Entry Wire Line
	8350 1800 8450 1900
Entry Wire Line
	8350 1900 8450 2000
Entry Wire Line
	8350 2000 8450 2100
Entry Wire Line
	8350 2100 8450 2200
Entry Wire Line
	8350 2200 8450 2300
Entry Wire Line
	8350 2300 8450 2400
Entry Wire Line
	8350 2400 8450 2500
Entry Wire Line
	8350 2500 8450 2600
Entry Wire Line
	8350 2600 8450 2700
Entry Wire Line
	8350 2700 8450 2800
Entry Wire Line
	8350 2800 8450 2900
Entry Wire Line
	8350 2900 8450 3000
Entry Wire Line
	8350 3000 8450 3100
Entry Wire Line
	8350 3100 8450 3200
Entry Wire Line
	8350 3200 8450 3300
Entry Wire Line
	8350 3300 8450 3400
Entry Wire Line
	8350 3400 8450 3500
Entry Wire Line
	8350 3500 8450 3600
Entry Wire Line
	8350 3600 8450 3700
Entry Wire Line
	8350 3700 8450 3800
Entry Wire Line
	8350 3800 8450 3900
Entry Wire Line
	8350 3900 8450 4000
Entry Wire Line
	8350 4000 8450 4100
Entry Wire Line
	8350 4100 8450 4200
Entry Wire Line
	8350 4200 8450 4300
Entry Wire Line
	8350 4300 8450 4400
Entry Wire Line
	8350 4400 8450 4500
Entry Wire Line
	8350 4500 8450 4600
Entry Wire Line
	8350 4600 8450 4700
Entry Wire Line
	8350 4700 8450 4800
Entry Wire Line
	8350 4800 8450 4900
Entry Wire Line
	8350 4900 8450 5000
Entry Wire Line
	8350 5000 8450 5100
Entry Wire Line
	8350 5100 8450 5200
Text Label 7500 900  2    50   ~ 0
BP-A1
Text Label 7500 1000 2    50   ~ 0
BP-A2
Text Label 7500 1100 2    50   ~ 0
BP-A3
Text Label 7500 1200 2    50   ~ 0
BP-A4
Text Label 7500 1300 2    50   ~ 0
BP-A5
Text Label 7500 1400 2    50   ~ 0
BP-A6
Text Label 7500 1500 2    50   ~ 0
BP-A7
Text Label 7500 1600 2    50   ~ 0
BP-A8
Text Label 7500 1700 2    50   ~ 0
BP-A9
Text Label 7500 1800 2    50   ~ 0
BP-A10
Text Label 7500 1900 2    50   ~ 0
BP-A11
Text Label 7500 2000 2    50   ~ 0
BP-A12
Text Label 7500 2100 2    50   ~ 0
BP-A13
Text Label 7500 2200 2    50   ~ 0
BP-A14
Text Label 7500 2300 2    50   ~ 0
BP-A15
Text Label 7500 2400 2    50   ~ 0
BP-A16
Text Label 7500 2500 2    50   ~ 0
BP-A17
Text Label 7500 2600 2    50   ~ 0
BP-A18
Text Label 7500 2700 2    50   ~ 0
BP-A19
Text Label 7500 2800 2    50   ~ 0
BP-A20
Text Label 7500 2900 2    50   ~ 0
BP-A21
Text Label 7500 3000 2    50   ~ 0
BP-A22
Text Label 7500 3100 2    50   ~ 0
BP-A23
Text Label 7500 3200 2    50   ~ 0
BP-A24
Text Label 7500 3300 2    50   ~ 0
BP-A25
Text Label 7500 3400 2    50   ~ 0
BP-A26
Text Label 7500 3500 2    50   ~ 0
BP-A27
Text Label 7500 3600 2    50   ~ 0
BP-A28
Text Label 7500 3700 2    50   ~ 0
BP-A29
Text Label 7500 3800 2    50   ~ 0
BP-A30
Text Label 7500 3900 2    50   ~ 0
BP-A31
Text Label 7500 4000 2    50   ~ 0
BP-A32
Text Label 7500 4100 2    50   ~ 0
BP-A33
Text Label 7500 4200 2    50   ~ 0
BP-A34
Text Label 7500 4300 2    50   ~ 0
BP-A35
Text Label 7500 4400 2    50   ~ 0
BP-A36
Text Label 7500 4500 2    50   ~ 0
BP-A37
Text Label 7500 4600 2    50   ~ 0
BP-A38
Text Label 7500 4700 2    50   ~ 0
BP-A39
Text Label 7500 4800 2    50   ~ 0
BP-A40
Text Label 7500 4900 2    50   ~ 0
BP-A41
Text Label 7500 5000 2    50   ~ 0
BP-A42
Text Label 7500 5100 2    50   ~ 0
BP-A43
Wire Wire Line
	7500 900  7150 900 
Wire Wire Line
	7500 1000 7150 1000
Wire Wire Line
	7500 4900 7150 4900
Wire Wire Line
	7500 5000 7150 5000
Wire Wire Line
	7500 5100 7150 5100
Wire Wire Line
	7500 4400 7150 4400
Wire Wire Line
	7500 4300 7150 4300
Wire Wire Line
	7500 4200 7150 4200
Wire Wire Line
	7500 4100 7150 4100
Wire Wire Line
	7500 4000 7150 4000
Wire Wire Line
	7500 3900 7150 3900
Wire Wire Line
	7500 3800 7150 3800
Wire Wire Line
	7500 3700 7150 3700
Wire Wire Line
	7500 3600 7150 3600
Wire Wire Line
	7500 3500 7150 3500
Wire Wire Line
	7500 3400 7150 3400
Wire Wire Line
	7500 3300 7150 3300
Wire Wire Line
	7500 3200 7150 3200
Wire Wire Line
	7500 3100 7150 3100
Wire Wire Line
	7500 3000 7150 3000
Wire Wire Line
	7500 2900 7150 2900
Wire Wire Line
	7500 2800 7150 2800
Wire Wire Line
	7500 2700 7150 2700
Wire Wire Line
	7500 2600 7150 2600
Wire Wire Line
	7500 2500 7150 2500
Wire Wire Line
	7500 2400 7150 2400
Wire Wire Line
	7500 2300 7150 2300
Wire Wire Line
	7500 2200 7150 2200
Wire Wire Line
	7500 2100 7150 2100
Wire Wire Line
	7500 2000 7150 2000
Wire Wire Line
	7500 1900 7150 1900
Wire Wire Line
	7500 1800 7150 1800
Wire Wire Line
	7500 1700 7150 1700
Wire Wire Line
	7500 1600 7150 1600
Wire Wire Line
	7500 1500 7150 1500
Wire Wire Line
	7500 1400 7150 1400
Wire Wire Line
	7500 1300 7150 1300
Wire Wire Line
	7500 1200 7150 1200
Wire Wire Line
	7500 1100 7150 1100
Wire Wire Line
	7500 4800 7150 4800
Wire Wire Line
	7500 4700 7150 4700
Wire Wire Line
	7500 4600 7150 4600
Wire Wire Line
	7500 4500 7150 4500
$Comp
L DelightSymbols:Conn_02x43_Row_Letter_First J5
U 1 1 628B3F0F
P 7750 3000
F 0 "J5" H 7750 5317 50  0000 C CNN
F 1 "Slot 4" H 7750 5226 50  0000 C CNN
F 2 "DelightFootprints:PinHeader_2x43_P2.54mm_Vertical" H 7700 3200 50  0001 C CNN
F 3 "~" H 7700 3200 50  0001 C CNN
	1    7750 3000
	1    0    0    -1  
$EndComp
Entry Wire Line
	8350 900  8450 1000
Entry Wire Line
	7050 5200 7150 5100
Entry Wire Line
	7050 5100 7150 5000
Entry Wire Line
	7050 5000 7150 4900
Entry Wire Line
	7050 4900 7150 4800
Entry Wire Line
	7050 4800 7150 4700
Entry Wire Line
	7050 4700 7150 4600
Entry Wire Line
	7050 4600 7150 4500
Entry Wire Line
	7050 4500 7150 4400
Entry Wire Line
	7050 4400 7150 4300
Entry Wire Line
	7050 4300 7150 4200
Entry Wire Line
	7050 4200 7150 4100
Entry Wire Line
	7050 4100 7150 4000
Entry Wire Line
	7050 4000 7150 3900
Entry Wire Line
	7050 3900 7150 3800
Entry Wire Line
	7050 3800 7150 3700
Entry Wire Line
	7050 3700 7150 3600
Entry Wire Line
	7050 3600 7150 3500
Entry Wire Line
	7050 3500 7150 3400
Entry Wire Line
	7050 3400 7150 3300
Entry Wire Line
	7050 3300 7150 3200
Entry Wire Line
	7050 3200 7150 3100
Entry Wire Line
	7050 3100 7150 3000
Entry Wire Line
	7050 3000 7150 2900
Entry Wire Line
	7050 2900 7150 2800
Entry Wire Line
	7050 2800 7150 2700
Entry Wire Line
	7050 2700 7150 2600
Entry Wire Line
	7050 2600 7150 2500
Entry Wire Line
	7050 2500 7150 2400
Entry Wire Line
	7050 2400 7150 2300
Entry Wire Line
	7050 2300 7150 2200
Entry Wire Line
	7050 2200 7150 2100
Entry Wire Line
	7050 2100 7150 2000
Entry Wire Line
	7050 2000 7150 1900
Entry Wire Line
	7050 1900 7150 1800
Entry Wire Line
	7050 1800 7150 1700
Entry Wire Line
	7050 1700 7150 1600
Entry Wire Line
	7050 1600 7150 1500
Entry Wire Line
	7050 1500 7150 1400
Entry Wire Line
	7050 1400 7150 1300
Entry Wire Line
	7050 1300 7150 1200
Entry Wire Line
	7050 1200 7150 1100
Entry Wire Line
	7050 1100 7150 1000
Entry Wire Line
	7050 1000 7150 900 
Entry Wire Line
	7050 5200 7150 5100
Wire Bus Line
	2450 5400 2550 5400
Connection ~ 2450 5400
Connection ~ 2550 5400
Wire Bus Line
	2550 5400 3950 5400
Connection ~ 3950 5400
Wire Bus Line
	3950 5400 4050 5400
Connection ~ 4050 5400
Wire Bus Line
	4050 5400 5450 5400
Connection ~ 5450 5400
Wire Bus Line
	5450 5400 5550 5400
Connection ~ 5550 5400
Wire Bus Line
	5550 5400 6950 5400
Connection ~ 6950 5400
Wire Bus Line
	6950 5400 7050 5400
Connection ~ 7050 5400
Wire Bus Line
	7050 5400 8450 5400
$Comp
L Mechanical:MountingHole H1
U 1 1 62E5E8DD
P 850 650
F 0 "H1" H 950 696 50  0000 L CNN
F 1 "MountingHole" H 950 605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 850 650 50  0001 C CNN
F 3 "~" H 850 650 50  0001 C CNN
	1    850  650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 62E5F55E
P 4750 550
F 0 "H3" H 4850 596 50  0000 L CNN
F 1 "MountingHole" H 4850 505 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 4750 550 50  0001 C CNN
F 3 "~" H 4750 550 50  0001 C CNN
	1    4750 550 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 62EB541E
P 8500 550
F 0 "H5" H 8600 596 50  0000 L CNN
F 1 "MountingHole" H 8600 505 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 8500 550 50  0001 C CNN
F 3 "~" H 8500 550 50  0001 C CNN
	1    8500 550 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 62F0B18A
P 850 5600
F 0 "H2" H 950 5646 50  0000 L CNN
F 1 "MountingHole" H 950 5555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 850 5600 50  0001 C CNN
F 3 "~" H 850 5600 50  0001 C CNN
	1    850  5600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 62F60B8D
P 4750 5650
F 0 "H4" H 4850 5696 50  0000 L CNN
F 1 "MountingHole" H 4850 5605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 4750 5650 50  0001 C CNN
F 3 "~" H 4750 5650 50  0001 C CNN
	1    4750 5650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 62FB6577
P 8600 5650
F 0 "H6" H 8700 5696 50  0000 L CNN
F 1 "MountingHole" H 8700 5605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 8600 5650 50  0001 C CNN
F 3 "~" H 8600 5650 50  0001 C CNN
	1    8600 5650
	1    0    0    -1  
$EndComp
Wire Bus Line
	2450 1000 2450 5400
Wire Bus Line
	3950 1000 3950 5400
Wire Bus Line
	5450 1000 5450 5400
Wire Bus Line
	6950 1000 6950 5400
Wire Bus Line
	8450 1000 8450 5400
Wire Bus Line
	1050 1000 1050 5400
Wire Bus Line
	2550 1000 2550 5400
Wire Bus Line
	4050 1000 4050 5400
Wire Bus Line
	5550 1000 5550 5400
Wire Bus Line
	7050 1000 7050 5400
$EndSCHEMATC
