# 8 Bit Delight

This repository is my personal stack of tools for working on 8 bit computers of various sorts. It is also a [mdbook](https://rust-lang.github.io/mdBook/) project that is [built and hosted](https://wraithan.gitlab.io/8-bit-delight/). The book serves a number of purposes, mostly it is a way for me to remember why I built things one way, or plans I had at one point. But also allows me to share the state of the build with friends and folks who find retro computing interesting.

## Working with the book

Install `mdbook` ((link)[https://rust-lang.github.io/mdBook/guide/installation.html]):

`cargo install mdbook`

Continously rebuild:

`mdbook serve`

And open:

`mdbook serve --open`

